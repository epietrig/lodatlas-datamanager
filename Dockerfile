FROM maven:3.5.4-jdk-8-slim as builder

LABEL maintainer="hande.gozukan@inria.fr"

ENV LANG en_US.utf8

# Install dependencies to build the project
RUN apt-get update && apt-get install -y --allow-unauthenticated \
    git \
    librdf0-dev \
    python-dev \
    python-librdf \
    python-pip \
    raptor2-utils && \
    pip install \
    bitarray \
    future \
    requests && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /opt

# Clone repo from GitLab repository with submodules
RUN git clone --recurse-submodules https://gitlab.inria.fr/epietrig/LODAtlas.git

# Checkout version xxx

WORKDIR /root/.m2
# Create settings.xml file for Maven
RUN touch settings.xml

# Add CEDAR teams private maven repository password to settings file
RUN echo '<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0" \
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" \
	     xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 \
	     https://maven.apache.org/xsd/settings-1.0.0.xsd"> \
  <servers> \
    <server> \
      <id>rdfsummary-releases</id> \
      <username>rdfsummary-private-reader</username> \
      <password>rfdsummary4maven6525766</password> \
    </server> \
    <server> \
      <id>oakcommons-releases</id> \
      <username>oak-private-reader</username> \
      <password>oakreader!</password> \
    </server> \
  </servers> \
</settings>' >  /root/.m2/settings.xml


WORKDIR /opt/LODAtlas

# Package 
RUN mvn clean package -Pprod -Dmaven.test.skip=true

#######################################################
# Start new image
#######################################################
FROM openjdk:8-jre-slim-stretch

RUN groupadd -r lodatlas --gid=999 && useradd -r -g lodatlas --uid=999 lodatlas

RUN apt-get update && apt-get install -y --allow-unauthenticated \
    librdf0-dev \
    python-dev \
    python-librdf \
    python-pip \
    raptor2-utils && \
    pip install \
    bitarray \
    future \
    requests && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /

# Copy lodatlas-datamanager from previous build
COPY --from=builder /opt/LODAtlas/lodatlas-datamanager/target/lodatlas-datamanager-project.tar.gz ./

RUN tar -zxf lodatlas-datamanager-project.tar.gz

RUN ls lodatlas-datamanager

RUN pwd

WORKDIR /lodatlas-datamanager

RUN ls

ENV LODATLAS_HOME=/lodatlas-datamanager

# the directory to save dump files temporarily
ENV LODATLAS_TMP "$LODATLAS_HOME/tmp"

RUN mkdir -p "$LODATLAS_TMP/vocabularies" && \
    chown -R lodatlas:lodatlas "$LODATLAS_TMP" && \
    chmod 777 "$LODATLAS_TMP"

# the directory to save logs
ENV LODATLAS_LOGS "$LODATLAS_HOME/logs"

RUN mkdir -p "$LODATLAS_LOGS" && \
    chown -R lodatlas:lodatlas "$LODATLAS_LOGS" && \
    chmod 777 "$LODATLAS_LOGS"

# the directory to share other files with the container
ENV LODATLAS_SHARE "$LODATLAS_HOME/share"

RUN mkdir -p "$LODATLAS_SHARE" && \
    chown -R lodatlas:lodatlas "$LODATLAS_SHARE" && \
    chmod 777 "$LODATLAS_SHARE"

ENV PATH $LODATLAS_HOME:$PATH

VOLUME /lodatlas-datamanager/tmp
VOLUME /lodatlas-datamanager/logs
VOLUME /lodatlas-datamanager/share

# This is exposed if a profiler is to be used
EXPOSE 9010