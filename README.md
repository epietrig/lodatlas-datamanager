# LODAtlas Datamanager
LODAtlas datamanager is a tool to download, manipulate and persist the metadata for linked data datasets. It also downloads the RDF dump files for dataset resources specified by the repository, processes it with [LODStats](https://github.com/AKSW/LODStats) to extract used classes, properties and vocabularies. Finally it processes dump files together with ontology files using [RDF Summary API](https://gitlab.inria.fr/scebiric/rdfsummary/tree/master) to get RDF graph summary. 

# Dependencies
* [lodatlas-commons](https://gitlab.inria.fr/epietrig/lodatlas-commons)
* [lodatlas-summarygenerator](https://gitlab.inria.fr/epietrig/lodatlas-summarygenerator)
* [lodatlas-lodsummaries](https://gitlab.inria.fr/epietrig/lodatlas-lodsummaries)
* MongoDB 3.2.2
* Elasticsearch Server 6.2.2
* Java 1.8

## LODAtlas Summary Generator Dependencies
* PostgreSQL 9.4 and up

## LODStats Dependencies
* Pyton 2.7
* librdf0-dev 
* python-librdf 
* raptor2-utils
* bitarray 
* requests 
* future

# Usage

Command line options:

```
 -h,--help                                  print this message
 -d,--download repository data <filePath>   download data provided by
                                            repositories listed in the
                                            file specified by <filePath>.
 -s,--extract summaries <directoryPath>     convert dumps to nt format,
                                            extract summaries,
                                            download/merge/save
                                            vocabularies using temporary
                                            <directoryPath>
 -e,--elasticsearch                         create/update elasticsearch
                                            indexes for datasets and
                                            vocabularies
 -l,--update_links                          update incoming links for
                                            datasets
```

# repoList.csv format
Each line in this file specifies a repository which provides a RESTful API to fetch metadata for datasets. The values are as follows:

```
   APIversion, Repo name, Repo URL, query to select relevant datasets, start index, number of datasets
```

The default repoList.csv contains the following lines:
   
```
ckanv3,datahub,https://old.datahub.io/,q=tags:lod,0,-1
ckanv3,datagov,https://catalog.data.gov/,q=rdf,0,-1
ckanv3,europeandata,https://www.europeandataportal.eu/data/,q=rdf,0,-1
```
Currently only **CKAN API v3** is supported. To download data from other repositories, [IRepoApiConnector](https://gitlab.inria.fr/epietrig/lodatlas-datamanager/blob/master/src/main/java/fr/inria/ilda/lodatlas/datamanager/repository/IRepoApiConnector.java) interface should be implemented.

# Performance Tuning
RDF summary computation requires extensive memory usage. The following Postgresql setting should be set depending on the machine that runs the datamanager program. 

* shared_buffers
* effective_cache_size
* work_mem
* maintenance_work_mem
* checkpoint_completion_target
* checkpoint_segments (replaced in 9.6 with max_wal_size)
* wal_buffers
* default_statistics_target
* checkpoint_timeout

[pgtune](https://github.com/gregs1104/pgtune) can be used to determine the best values for the parameters.

It is also adviced to set the JVM parameters `-Xms` and `-Xmx` depnding on your machine configuration and turn on string deduplication for G1 garbage collector using `-XX:+UseG1GC -XX:+UseStringDeduplication` JVM arguments to reduce memory consumption

To give an idea, with 128GB of RAM, it is possible to process an RDF dump file of size ~20GB. This size might vary depending on the size of the RDF summary. 

# Docker 

# License
LODAtlas Datamanager is licensed under [GNU General Public License version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)