#!/bin/sh

###############################################
# Generates docker image for provided version 
# and sets it as the latest version. Pushes the 
# generated docker images to GitLab repository
###############################################

set -e

[ "$DEBUG" = true ] && set -x

# check if version argument is provided, exit if not provided
if [ $# -eq 0 ]
  then
    echo "Please specify version x.x"
    exit 1
fi


VERSION=$1

# login to gitlab registry
docker login registry.gitlab.inria.fr

# build docker image with version
docker build -t registry.gitlab.inria.fr/epietrig/lodatlas:datamanager-${VERSION} .

# push image to registry
docker push registry.gitlab.inria.fr/epietrig/lodatlas

# build docker image as latest
docker build -t registry.gitlab.inria.fr/epietrig/lodatlas:datamanager-latest .

# push image to registry
docker push registry.gitlab.inria.fr/epietrig/lodatlas