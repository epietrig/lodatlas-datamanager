# coding=utf-8
"""
 Copyright (C) 2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 
 This file contains a modified version of LodStats code.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from builtins import object

virtenv_path = '/home/hande/.virtualenvs/lodstats/src/LODStats/test/resources/'

rdf_test_file_uri = 'file://' + virtenv_path + 'heb-original.rdf'
rdf_test_file_uri_head = 'file://' + virtenv_path + 'heb-head-original.nt'
rdf_test_file_uri_tail = 'file://' + virtenv_path + 'heb-tail-original.nt'
rdf_test_file_name = virtenv_path + 'heb-original.rdf'
rdf_test_file_name_head = virtenv_path + 'heb-head-original.nt'
rdf_test_file_name_head_ttl = virtenv_path + 'heb-head-original.ttl'
rdf_test_file_name_tail = virtenv_path + 'heb-tail-original.nt'

def callback_function_archive_extraction(object):
    print object.bytes_extracted

def callback_function_download(object):
    print object.bytes_downloaded

def callback_function_statistics(object):
    print object.get_no_of_triples()

def callback_function_conversion(object):
    print object.bytes_converted

import logging
# import sys
# reload(sys)
# sys.setdefaultencoding('utf-8')

# root logger config
logging.basicConfig(format = u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', filename = 'logs/lodstats.log', level=logging.CRITICAL)

def enable_debug():
    logging.disable(logging.NOTSET)

def disable_debug():
    logging.disable(logging.CRITICAL)
