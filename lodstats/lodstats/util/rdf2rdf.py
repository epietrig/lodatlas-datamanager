# coding=utf-8
"""
 Copyright (C) 2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 
 This file contains a modified version of LodStats code.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import subprocess
import tempfile
import os

import lodstats.config

from lodstats.util.interfaces import UriParserInterface
from lodstats.util.interfaces import CallbackInterface

import logging

logger = logging.getLogger("lodstats")

class RDF2RDF(CallbackInterface, UriParserInterface):
    def __init__(self, uri, callback_function=None):
        super(RDF2RDF, self).__init__()
        self.uri = uri
        self.filename = self.identify_filename(self.uri)
        self.bytes_converted = 0
        self.callback_function = callback_function
        self.rdf2rdf_path = ""
        if(self.is_remote()):
            logger.error("Can not process URI, is remote: %s" % self.uri)

    def convert_to_nt(self, type= type, uri=None):
        if(uri is None):
            uri = self.uri

        input_file_path = uri[7:]
        output_file_path = input_file_path+".nt"

        logger.info("Converting: %s %s" % (input_file_path, output_file_path))

        command = "any23 rover -e %s -f ntriples -o %s %s" % (type, output_file_path, input_file_path)
        logger.info(command)
        print command

        pipe = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (stdout, stderr) = pipe.stdout, pipe.stderr

        for data in stderr:
            logger.debug(str(data))

        for data in stdout:
            logger.debug(str(data))
		
        return "file://%s" % output_file_path


    def convert_n3_to_nt(self, uri=None):
        logger.error("Cannot process n3 format {}".format(uri))
        pass  