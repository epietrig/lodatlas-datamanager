# coding=utf-8
"""
 Copyright (C) 2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 
 This file contains a modified version of LodStats code.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import lodstats.util.rdffile
import lodstats.config
import logging

logger = logging.getLogger("lodstats")

class RdfParser(object):
    def __init__(self, uri_list, callback_function=None, stats=None, rdf_format=None):
        self.rdf_format=rdf_format
        self.callback_function = callback_function
        self.uri_list = uri_list
        self.stats = stats
        self.stream = None
        self.stats_results = None # shared object between several rdf_file
        self.init_stats(stats)
        self.rdf_file_list = self.process_uri_list()

    def init_stats(self, stats):
        if stats is None:
            self.stats_results = lodstats.stats.init_stats()
        else:
            self.stats_results = lodstats.stats.init_stats(stats)

    def get_rdf_file_list(self):
        return self.rdf_file_list

    def get_no_of_statements(self):
        result = 0
        for rdf_file in self.get_rdf_file_list():
            result += rdf_file.get_no_of_statements()
        return result

    def get_no_of_triples(self):
        return self.get_no_of_statements()

    def get_no_of_warnings(self):
        no_of_warnings = 0
        for rdf_file in self.get_rdf_file_list():
            no_of_warnings += rdf_file.warnings
        return no_of_warnings

    def get_last_warning(self):
        last_warning = None
        for rdf_file in self.get_rdf_file_list():
            last_warning = rdf_file.last_warning
        return last_warning

    def get_stats_results(self):
        return self.stats_results

    def process_uri_list(self):
        rdf_files = []
        for uri in self.uri_list:
            logger.info("Processing rdf file: %s" % uri)
            logger.info('Format:   %s ' % self.rdf_format)
#            print "Processing rdf file: %s" % uri
            rdf_file = lodstats.util.rdffile.RdfFile(uri,
                                                     stats=self.stats,
                                                     callback_function=self.callback_function,
                                                     rdf_format=self.rdf_format)
            logger.info("Processed rdf file: %s" % uri)
            rdf_files.append(rdf_file)
        lodstats.stats.postproc()
        return rdf_files
