#!/usr/bin/env python
# coding=utf-8
"""
 Copyright (C) 2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 
 This file contains a modified version of LodStats code.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
Copyright 2013 AKSW Research Group http://aksw.org/

This file is part of LODStats.

LODStats is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LODStats is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LODStats.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys
import datetime
from optparse import OptionParser

import lodstats.RDFStats
import lodstats.config
import lodstats.stats

#from pymongo import MongoClient

import RDF

import traceback
import logging

# get logger
logger = logging.getLogger("lodstats")
# set level for this logger
logger.setLevel(logging.INFO)

parser = OptionParser(usage="usage: %prog [options] [-m model.{rdf,nt,...}] file/URI [file/URI ...]")
parser.add_option('-f', '--format', dest='format', help='specify RDF format', metavar="FORMAT")
parser.add_option('-r', '--resource-id', dest='resourceId', help='specify resource ID', metavar="RESOURCE_ID")
parser.add_option('-n', '--file-name', dest='fileName', help='file name', metavar='FILE_NAME')
parser.add_option('-p', '--progress', action='store_true', help='show progress')
parser.add_option('-d', '--debug', action='store_true', help='print debugging output')

(options, args) = parser.parse_args()

if len(args) == 0:
    parser.print_help()
    exit(2)

lodstats.config.disable_debug()
if options.debug:
    lodstats.config.enable_debug()

# prepare only void stats which contains vocabularies
stats = lodstats.stats.void_stats
new_stats = []

######################
# Callback functions #
######################
def callback_function_download(remote_file):
    now = datetime.datetime.now()
    time_delta = (now-start_time).seconds
    if time_delta > 0:
        print("\r%d of %d KB loaded, %d KB/s." % (remote_file.bytes_downloaded/1024,
        remote_file.content_length/1024, (remote_file.bytes_downloaded/1024)/time_delta)),
        sys.stdout.flush()
    else:
        print("\r%d of %d KB loaded." % (remote_file.bytes_downloaded/1024,
        remote_file.content_length/1024)),
        sys.stdout.flush()

def callback_function_extraction(archive_extractor):
    manyspaces = ' '*72
    print("\r%s" % manyspaces),
    sys.stdout.flush()
    now = datetime.datetime.now()
    time_delta = (now-start_time).seconds
    if time_delta > 0:
        print("\rFetch done %d KB, %d KB uncomp., %d KB/s." % (archive_extractor.original_file_size/1024, archive_extractor.bytes_extracted/1024, (archive_extractor.bytes_extracted/1024)/time_delta))
        sys.stdout.flush()
    else:
        print("\rFetch done %d KB, %d KB uncomp." % (archive_extractor.original_file_size/1024, archive_extractor.bytes_extracted/1024))
        sys.stdout.flush()

def callback_function_statistics(rdf_file):
    if rdf_file.get_no_of_triples() > 0:
        now = datetime.datetime.now()
        time_delta = (now-start_time).seconds
        if time_delta > 0:
            print("\r%d triples done, %d/s, %d warnings" % (rdf_file.get_no_of_triples(),
            rdf_file.get_no_of_triples()/time_delta, rdf_file.warnings)),
        else:
            print("\r%d triples done, %d warnings" % (rdf_file.get_no_of_triples(), rdf_file.warnings)),
        sys.stdout.flush()
        if not options.count and options.intermediate_stats and rdf_file.no_of_statements % 100000 == 0:
            print("Results (from custom code):")
            for stat_name,stat_dict in rdf_file.stats_results.iteritems():
                print("\t%s" % stat_name)
                for subname, result in stat_dict.iteritems():
                    if type(result) == dict or type(result) == list:
                        print("\t\tlen(%s): %d" % (subname, len(result)))
                    else:
                        print("\t\t%s: %s" % (subname, result))

# *************************************************************
# MongoDB connection 
# *************************************************************

#dbName = 'ckan_04_11_2017'
#repoNames = ['datahub']

#mongoClient = MongoClient('localhost', 27017)
#dbDatahub = mongoClient[dbName]
#collection = dbDatahub[repoNames[0]]

# *************************************************************
# Processing logic 
# *************************************************************

resourceUri = args[0]
resourceId = options.resourceId.strip()
fileName = options.fileName.strip()
format = options.format.strip()
ntFileName = fileName

logger.info('################################')
logger.info('Resource URI:   %s ' % resourceUri)
logger.info('Format:   %s ' % format)
logger.info('Resource id:  {}'.format(resourceId))
logger.info('File name:  {}\n'.format(fileName))

#if not os.access(resource_uri, os.R_OK):
     #print("File not found/unreadable")
     #exit(3)

rdf_stats = lodstats.RDFStats(resourceUri, stats=stats, new_stats=new_stats, format=format)

if options.progress:
    rdf_stats.set_callback_function_download(callback_function_download)
    rdf_stats.set_callback_function_extraction(callback_function_extraction)
    rdf_stats.set_callback_function_statistics(callback_function_statistics)
else:
    rdf_stats.disable_callback_function_download()
    rdf_stats.disable_callback_function_extraction()
    rdf_stats.disable_callback_function_statistics()

# generate statistics
processed = True

try:
    logger.info('Starting statistics')
    rdf_stats.start_statistics()
    logger.info('Ran statistics without problems')
except NameError as ex:
    processed = False
    logger.exception('NameError for resource URL {}\n'.format(resourceUri))

    entry = {"fileName": fileName,
             "hasError": True,
             "error": repr(ex)}


except RDF.RedlandError as ex:
    processed = False
    logger.exception('RedlandError for resource URL {}\n'.format(resourceUri))

    entry = {"fileName": fileName,
             "hasError": True,
             "error" : repr(ex)}


except Exception as ex:
    processed = False
    logger.exception('Exception for resource URL {}\n'.format(resourceUri))

    entry = {"fileName": fileName,
             "hasError": True,
             "error" : repr(ex)}


logger.info('Resource processed= {}'.format(processed))

# if statistics are generated without problems, extract vocabularies
if processed:

# *************************************************************
# triple count
# *************************************************************
    tripleCount = rdf_stats.get_no_of_triples()
    
    stat_results = rdf_stats.get_stats_results()
   
# *************************************************************
# vocabularies
# *************************************************************
    vocabularies = stat_results['vocabularies']
    vocabularyList = []

    logger.info('Vocabularies:')
    for vocabulary in vocabularies:
        logger.info(vocabulary)
        vocabularyList.append(vocabulary)

# *************************************************************
# used classes
# *************************************************************
    logger.info("Used classes:")
    classes = stat_results['usedclasses']['usage_count']
    classList = []

    for classN in classes:
        logger.info(classN)
        classList.append(classN)


# *************************************************************
# property usage
# *************************************************************
    logger.info("Property usage:")
    propertyUsage = stat_results['propertyusage']['usage_count']
    propertyUsageList = []

    for property in propertyUsage:
        logger.info(property)
        propertyUsageList.append(property)


    entry = {"fileName": fileName,
             "tripleCount": tripleCount,
             "hasError": False,
             "error": "",
             "usedClasses": classList,
             "usedProperties": propertyUsageList,
             "vocabularies" : vocabularyList
             }
    
else:
    logger.warning('Did not process dump {}\n'.format(resourceUri))
 
# *****************************************************************************************************************
# Instead of writing results to MongoDB, now it is sent to standard output and the result is retrieved by java app. 
# *****************************************************************************************************************
    
# Write results to MongoDB    
#try:
#    logger.info("Writing to MongoDB")
#    collection.update_one({"resources.id": resourceId}, {"$push": {"resources.$.lodstats": entry}})
#    logger.info('Updated MongoDB for {}\n'.format(resourceUri))
#
#except Exception as ex:
#    logger.exception('Exception for resource URL {}\n'.format(resourceUri))
#      
#	entry = {"fileName": fileName,
#        "hasError": True,
#        "error" : repr(ex)}


# This is necessary to get the result from Java code
sys.stdout.write(str(entry))
