/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Main entry point of the application. Executes depending on the selected
 * command line options. Usage and possible command line options are as follows.
 * 
 * <pre>
 * 
 * usage: DataManager [-h] [-d <filePath>] [-s <directoryPath>] [-e] [-l]
 *  -h,--help                                  print this message
 *  -d,--download repository data <filePath>   download data provided by
 *                                             repositories listed in the
 *                                             file specified by <filePath>.
 *  -s,--extract summaries <directoryPath>     convert dumps to nt format,
 *                                             extract summaries,
 *                                             download/merge/save
 *                                             vocabularies using temporary
 *                                             <directoryPath>
 *  -e,--elasticsearch                         create/update elasticsearch
 *                                             indexes for datasets and
 *                                             vocabularies
 *  -l,--update_links                          update incoming links for
 *                                             datasets 
 *  -r, --restrict <filePath>                  restrict the datasets to process
 *                                             to the ones listed in the CSV 
 *                                             file specified by <filePath>
 * </pre>
 * 
 * @author Hande Gözükan
 *
 */
public class DataManagerMain {

	public static void main(String[] args) {
		Options options = createOptions();

		CommandLineParser cliParser = new DefaultParser();

		try {
			CommandLine cli = cliParser.parse(options, args);

			if (((Option[]) cli.getOptions()).length == 0) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.setOptionComparator(null);
				formatter.printHelp("DataManager", options, true);
				System.exit(0);
			}

			ProcessManager.Builder builder = ProcessManager.builder();

			// // dataset list file specified
			// if (cli.hasOption("p")) {
			// builder = builder.setDatasetList(cli.getOptionValue("p"));
			// }
			//

			try {
				// download JSON
				if (cli.hasOption("d")) {
					builder = builder.downloadJson(cli.getOptionValue("d"));
				}

				// extract summaries
				if (cli.hasOption("s")) {
					builder = builder.extractSummaries(cli.getOptionValue("s"));
				}

				// restrict list of datasets
				if (cli.hasOption("r")) {
					builder = builder.restrict(cli.getOptionValue("r"));
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
				System.exit(1);
			}

			// create elasticsearch indexes
			if (cli.hasOption("e")) {
				builder = builder.createElasticIndexes();
			}

			// update links
			if (cli.hasOption("l")) {
				builder = builder.updateLinks();
			}

			ProcessManager processManager = builder.build();
			processManager.run();

		} catch (ParseException e) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.setOptionComparator(null);
			formatter.printHelp("DataManager", options, true);
		}
	}

	/**
	 * Creates command line options.
	 * 
	 * @return option list for available command line options.
	 */
	private static Options createOptions() {

		Options options = new Options();

		// display help
		Option help = new Option("h", "help", false, "print this message");
		options.addOption(help);

		// download JSON data from dataset repository for each dataset
		Option downloadRepoData = Option.builder("d").longOpt("download repository data").hasArg(true)
				.argName("filePath")
				.desc("download data provided by repositories listed in the file specified by <filePath>.").build();
		options.addOption(downloadRepoData);

		// extract rdfsums/lodsums for each dataset
		Option extractSummaries = Option.builder("s").longOpt("extract summaries").hasArg(true).argName("directoryPath")
				.desc("convert dumps to nt format, extract summaries, download/merge/save vocabularies using temporary <directoryPath>")
				.build();
		options.addOption(extractSummaries);

		// create update elasticsearch indexes for each datasets and vocabularies
		Option elastic = new Option("e", "elasticsearch", false,
				"create/update elasticsearch indexes for datasets and vocabularies");
		options.addOption(elastic);

		// extract rdfsums/lodsums for each dataset
		Option restrict = Option.builder("r").longOpt("restrict datasets").hasArg(true).argName("filePath")
				.desc("restrict the datasets to process to the ones listed in the CSV file specified by <filePath>")
				.build();
		options.addOption(restrict);

		// update incoming links of datasets
		Option updateLinks = new Option("l", "update_links", false, "update incoming links for datasets");
		options.addOption(updateLinks);

		return options;
	}

}
