/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.commons.es.EsStrings;
import fr.inria.ilda.lodatlas.commons.es.EsUtil;
import fr.inria.ilda.lodatlas.datamanager.dataset.DatasetIterator;
import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;
import fr.inria.ilda.lodatlas.datamanager.dataset.processor.DatasetIndexDecorator;
import fr.inria.ilda.lodatlas.datamanager.dataset.processor.DatasetProcessor;
import fr.inria.ilda.lodatlas.datamanager.dataset.processor.DbReadDecorator;
import fr.inria.ilda.lodatlas.datamanager.dataset.processor.DownloadDecorator;
import fr.inria.ilda.lodatlas.datamanager.dataset.processor.IDatasetProcessor;
import fr.inria.ilda.lodatlas.datamanager.dataset.processor.InsertDecorator;
import fr.inria.ilda.lodatlas.datamanager.dataset.processor.LinkUpdateDecorator;
import fr.inria.ilda.lodatlas.datamanager.dataset.processor.ResourceIndexDecorator;
import fr.inria.ilda.lodatlas.datamanager.dataset.processor.SummaryDecorator;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.Config;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.MongoUtil;
import fr.inria.ilda.lodatlas.datamanager.exception.NoRepoExistsException;
import fr.inria.ilda.lodatlas.datamanager.util.Util;
import fr.inria.ilda.lodatlas.datamanager.util.VocabularyIndexer;

/**
 * A class to manage data processing. Depending on the selected command line
 * options, initiates the necessary decorators to download/read from database
 * and process contents and metadata for datasets.
 * 
 * @author Hande Gözükan
 *
 */
public class ProcessManager {

	private static final Logger logger = LoggerFactory.getLogger(ProcessManager.class);

	/**
	 * The file that contains runtime configuration parameters.
	 */
	public static final String CONFIG_FILE = "./config.properties";

	public static final Properties prop = Util.loadProperties(CONFIG_FILE);

	/**
	 * Dataset processor that will process each dataset depending on the selected
	 * options specified by {@link Builder}.
	 */
	private IDatasetProcessor datasetProcessor = null;

	/**
	 * Iterator used to iterate over datasets from a specified repository or a list
	 * of repositories.
	 */
	private final DatasetIterator datasetIterator;

	/**
	 * Vocabulary indexer to index vocabulary data.
	 */
	private final VocabularyIndexer vocabularyIndexer;

	/**
	 * Boolean to indicate whether incoming links should be updated after all
	 * processing finishes.
	 */
	private final boolean updateLinks;

	/**
	 * Boolean to indicate whether Elasticsearch indexes should be generated.
	 */
	private final boolean generateEsIndexes;

	private final MongoUtil mongoUtil;

	private final EsUtil esUtil;

	private Map<String, List<String>> restrictMap;

	/**
	 * Private constructor used by the static nested class {@link Builder}.
	 * 
	 * @param builder
	 *            used to create this ProcessManager.
	 */
	private ProcessManager(final Builder builder) {

		this.updateLinks = builder.updateLinks;
		this.generateEsIndexes = builder.generateEsIndexes;

		this.mongoUtil = new MongoUtil(prop.getProperty(Config.MG_HOST),
				Integer.parseInt(prop.getProperty(Config.MG_PORT)), prop.getProperty(Config.MG_DB_NAME));

		this.datasetIterator = getDatasetIterator(builder.downloadJSON, builder.repoListPath, this.mongoUtil);

		// If no iterator could be initialized, exits the system.
		if (this.datasetIterator == null) {
			System.out.println("No repository connector could be initialized. Please check if the file "
					+ builder.repoListPath.toAbsolutePath() + " is well formed.");
			System.exit(1);
		}

		if (builder.restrict) {
			try {
				restrictMap = loadRestrict(builder.restrictPath);
			} catch (IOException e) {
				System.out.println("Problem loading " + builder.restrictPath.toAbsolutePath() + " file");
			}
		}

		// load data from datasource (download from repository or read from db)
		if (builder.downloadJSON) {
			datasetProcessor = new InsertDecorator(new DownloadDecorator(new DatasetProcessor(),
					this.datasetIterator.getRepositoryMap(), this.mongoUtil), this.mongoUtil);
		} else if (builder.extractSummaries) {
			datasetProcessor = new DbReadDecorator(new DatasetProcessor(), this.mongoUtil);
		}

		// extract summaries
		if (builder.extractSummaries) {
			datasetProcessor = new SummaryDecorator(datasetProcessor, builder.tempProcessPath, this.mongoUtil);
		}

		this.esUtil = new EsUtil(prop.getProperty(EsStrings.CLUSTER),
				Integer.parseInt(prop.getProperty(EsStrings.PORT)), prop.getProperty(EsStrings.HOST));

		this.vocabularyIndexer = new VocabularyIndexer(this.esUtil, mongoUtil);

		// TODO make this optional, to index while processing
		// if a dataset processor is initialized, the database
		// must be updated with the results of the process
		if (datasetProcessor != null && !this.generateEsIndexes) {
			// TODO decide the value of deleteIfExists depending on the options
			datasetProcessor = new ResourceIndexDecorator(
					new DatasetIndexDecorator(new DbReadDecorator(datasetProcessor, this.mongoUtil), this.esUtil,
							false),
					this.mongoUtil, this.esUtil, false);
		}

	}

	private Map<String, List<String>> loadRestrict(Path restrictPath) throws IOException {
		Map<String, List<String>> datasetMap = new HashMap<>();

		Scanner scanner = new Scanner(restrictPath);

		while (scanner.hasNextLine()) {
			// I did not put a check for first line which contains field names, as there
			// will be no dataset with reponame and name
			String line = scanner.nextLine();
			String[] lineArray = line.split(",");

			if (lineArray.length == 2) {
				String repoName = lineArray[0];
				String datasetName = lineArray[1];

				List<String> datasetList = datasetMap.get(repoName);
				if (datasetList == null) {
					datasetList = new ArrayList<>();
					datasetList.add(datasetName);
					datasetMap.put(repoName, datasetList);
				} else {
					datasetList.add(datasetName);
				}

			}
		}
		scanner.close();
		return datasetMap;
	}

	/**
	 * Initializes a {@link DatasetIterator} instance depending on the
	 * builder.downloadJSON value.
	 * 
	 * 
	 * @param downloadJSON
	 *            indicates if JSON document will be downloaded from repository.
	 * @param repoListPath
	 *            the path of the repository list file, if the JSON will be
	 *            downloaded from repository.
	 * @param monguUtil
	 *            mongoDB connection utility to be used if dataset JSON will not be
	 *            downloaded.
	 * 
	 * @return a dataset iterator.
	 */
	private DatasetIterator getDatasetIterator(boolean downloadJSON, Path repoListPath, MongoUtil mongoUtil) {
		DatasetIterator tempIterator = null;

		if (downloadJSON) {
			try {
				tempIterator = new DatasetIterator(repoListPath);
			} catch (IOException e) {
				logger.error("Problem with the specified repository list file.", e);
			} catch (NoRepoExistsException e) {
				logger.error("Problem with the specified repository list file. No repo exists.", e);
			}
		} else {
			tempIterator = new DatasetIterator(mongoUtil);
		}
		return tempIterator;
	}

	/**
	 * TODO
	 */
	public void run() {

		if (logger.isInfoEnabled())
			logger.info("Starting processing datasets.");

		long start = System.currentTimeMillis();

		if (datasetProcessor != null) {
			IDataset dataset = null;
			int index = 1;
			boolean process = true;
			while (datasetIterator.hasNext()) {

				if (logger.isInfoEnabled())
					logger.info("Index : " + index++);

				dataset = datasetIterator.next();
				try {
					// this check can be put inside DatasetProcessor
					if (restrictMap != null) {
						List<String> datasetList = restrictMap.get(dataset.getRepoName());
						if (datasetList == null || !datasetList.contains(dataset.getName())) {
							process = false;
						} else {
							process = true;
						}
					} else {
						process = true;
					}
					if (process) {
						datasetProcessor.processDataset(dataset);
					}
				} catch (Exception ex) {
					logger.error("Exception while processing dataset " + dataset.getName(), ex);
				}
			}
		}

		// After processing all datasets, update the links for all datasets
		if (updateLinks) {
			updateLinks();
		}
		if (!updateLinks && generateEsIndexes) {
			regenerateEsIndexes();
		}

		dispose();

		long end = System.currentTimeMillis();

		if (logger.isInfoEnabled())
			logger.info("Finished processing datasets in " + ((end - start) / 60000)
					+ " min.\n****************************************************************************\n\n\n");
	}

	/**
	 * TODO
	 */
	private void updateLinks() {
		if (logger.isInfoEnabled())
			logger.info("Updating links for all datasets");

		IDatasetProcessor datasetProcessor = new LinkUpdateDecorator(
				new DbReadDecorator(new DatasetProcessor(), this.mongoUtil), this.mongoUtil);

		datasetIterator.reset();

		while (datasetIterator.hasNext()) {
			datasetProcessor.processDataset(datasetIterator.next());
		}
		if (this.generateEsIndexes) {
			regenerateEsIndexes();
		}
	}

	/**
	 * TODO
	 */
	private void regenerateEsIndexes() {
		if (logger.isInfoEnabled())
			logger.info("Updating links for all datasets");

		IDatasetProcessor datasetProcessor = new ResourceIndexDecorator(
				new DatasetIndexDecorator(new DbReadDecorator(new DatasetProcessor(), this.mongoUtil), this.esUtil,
						true),
				this.mongoUtil, this.esUtil, true);

		datasetIterator.reset();

		while (datasetIterator.hasNext()) {
			datasetProcessor.processDataset(datasetIterator.next());
		}

		this.vocabularyIndexer.indexAll();

	}

	/**
	 * TODO
	 */
	private void dispose() {
		this.mongoUtil.dispose();

		if (this.esUtil != null) {
			this.esUtil.dispose();
		}
	}

	/**
	 * Returns a {@link Builder} to create a {@link ProcessManager} instance.
	 * 
	 * @return a {@link Builder} instance.
	 */
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * A nested builder class to create <code>ProcessManager</code> instances
	 * depending on the selected options.
	 * <p>
	 * Example usage:
	 * 
	 * <pre>
	 * ProcessManager pManager = ProcessManager.builder().downloadJson("filePath").createElasticIndexes().build();
	 * </pre>
	 * 
	 * @author Hande Gözükan
	 *
	 */
	public static final class Builder {

		// // absolute path for the dataset list file.
		// private String datasetFilePath;

		// specifies whether JSON file from data repository will be downloaded
		private boolean downloadJSON;

		// specifies whether summaries will be extracted.
		private boolean extractSummaries;

		// the absolute path to the temporary directory to download and process resource
		// dump files.
		private Path tempProcessPath;

		// the absolute path to the CSV file that lists the repositories to download
		// data.
		private Path repoListPath;

		// specifies whether elasticsearch indexes will be generated
		private boolean generateEsIndexes;

		// specifies whether incoming links will be updated after download.
		private boolean updateLinks;

		private boolean restrict;

		// the absolute path to the CSV file that lists the repoName, dataset name pairs
		// to be processed
		private Path restrictPath;

		/**
		 * Constructs a {@link Builder}.
		 */
		private Builder() {
		}

		/**
		 * Sets whether this ProcessManager will download repository JSON for specified
		 * dataset list.
		 * 
		 * @param filePath
		 *            CSV file that lists repository name, repository URL, and query to
		 *            get relevant datasets.
		 * 
		 * @return this builder
		 * 
		 * @throws IllegalArgumentException
		 *             if the specified <code>filePath</code> is <code>null</code>,
		 *             empty string, or does not point to a file on the host machine.
		 */
		public Builder downloadJson(String filePath) {
			if (filePath == null || "".equals(filePath))
				throw new IllegalArgumentException(
						"Repository list file must be a valid file. Specified path : " + filePath);

			this.repoListPath = Paths.get(filePath);

			if (!this.repoListPath.toFile().exists()) {
				throw new IllegalArgumentException(
						"Repository list file must be a valid file. Specified path : " + filePath);
			}
			this.downloadJSON = true;
			return this;
		}

		/**
		 * Sets whether this ProcessManager will extract summaries for specified dataset
		 * list and also sets the temporary path where the dump files will be downloaded
		 * and processed.
		 * 
		 * @param tempPath
		 *            the absolute path to the temporary directory to download and
		 *            process resource dump files.
		 * @return this builder
		 * 
		 * @throws IllegalArgumentException
		 *             if the specified <code>tempPath</code> is <code>null</code>,
		 *             empty string, or does not point to a directory on the host
		 *             machine.
		 */
		public Builder extractSummaries(String tempPath) {
			if (tempPath == null || "".equals(tempPath))
				throw new IllegalArgumentException(
						"Temporary processing directory must be a valid directory. Specified path : " + tempPath);

			this.tempProcessPath = Paths.get(tempPath);

			if (!this.tempProcessPath.toFile().exists() || !this.tempProcessPath.toFile().isDirectory()) {
				throw new IllegalArgumentException(
						"Temporary processing directory must be a valid directory. Specified path : " + tempPath);
			}
			this.extractSummaries = true;
			return this;
		}

		/**
		 * Sets whether this ProcessManager will restrict dataset processing to the
		 * datasets listed in the file specified by <code>restPath</code>.
		 * 
		 * @param restPath
		 *            the absolute path to the file that lists the datasets to be
		 *            processed. This file must be a CSV file.
		 * @return this builder
		 * 
		 * @throws IllegalArgumentException
		 *             if the specified <code>restPath</code> is <code>null</code>,
		 *             empty string, or does not point to a valid file.
		 */
		public Builder restrict(String restPath) {
			if (restPath == null || "".equals(restPath))
				throw new IllegalArgumentException(
						"Restrict path must be a valid directory. Specified path : " + restPath);

			this.restrictPath = Paths.get(restPath);

			if (!this.restrictPath.toFile().exists()) {
				throw new IllegalArgumentException(
						"Restrict path must point to a valid file. Specified path : " + restPath);
			}
			this.restrict = true;
			return this;
		}

		/**
		 * Sets whether this ProcessManager will create Elasticsearch indexes for
		 * specified datasets.
		 * 
		 * @return this builder
		 */
		public Builder createElasticIndexes() {
			this.generateEsIndexes = true;
			return this;
		}

		/**
		 * Sets whether this ProcessManager will update incoming links for all datasets
		 * in the database.
		 * 
		 * @return this builder
		 */
		public Builder updateLinks() {
			this.updateLinks = true;
			return this;
		}

		/**
		 * Constructs a new {@link ProcessManager} with the properties specified by this
		 * builder.
		 * 
		 * @return a new {@link ProcessManager}
		 */
		public ProcessManager build() {
			// TODO check for illegal state
			return new ProcessManager(this);
		}
	}
}
