package fr.inria.ilda.lodatlas.datamanager;

import java.util.function.Consumer;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

public class TestModel {

	public static void main(String[] args) {
		OntModel model = ModelFactory.createOntologyModel();

//		model.read("http://xmlns.com/foaf/0.1/");
//		 model.read("http://purl.org/ontology/bibo/");
		 model.read("http://bibliographic-ontology.org/bibo");

		System.out.println("listclasses()");
		model.listClasses().forEachRemaining(new Consumer<OntClass>() {

			@Override
			public void accept(OntClass ontClass) {
				String label = ontClass.getLabel("en");
				if (label != null) {
					System.out.println(label);
				}

			}
		});

		System.out.println("\n\nlistComplementClasses()");
		model.listComplementClasses().forEachRemaining(new Consumer<OntClass>() {

			@Override
			public void accept(OntClass ontClass) {
				String label = ontClass.getLabel("en");
				if (label != null) {
					System.out.println(label);
				}

			}
		});

		System.out.println("\n\nlistOntProps()");
		model.listAllOntProperties().forEachRemaining(new Consumer<OntProperty>() {

			@Override
			public void accept(OntProperty ontProperty) {
				String label = ontProperty.getLabel("en");
				if (label != null) {
					System.out.println(label);
				}

			}
		});

		System.out.println("\n\nclass iterator");
		StmtIterator iterator = model.listStatements(null, RDF.type, RDFS.Class.asResource());

		while (iterator.hasNext()) {
			org.apache.jena.rdf.model.Resource res = ((Statement) iterator.next()).getSubject();
			Statement label = res.getProperty(RDFS.label);
			if (label != null) {
				System.out.println(label.getString());
			}
			// System.out.println(res.getURI() + " --- " +
			// res.getProperty(RDFS.label).getString());
		}

		System.out.println("\n\nproperty iterator");
		iterator = model.listStatements(null, RDF.type, RDF.Property.asResource());

		while (iterator.hasNext()) {
			org.apache.jena.rdf.model.Resource res = ((Statement) iterator.next()).getSubject();
			Statement label = res.getProperty(RDFS.label);
			if (label != null) {
				System.out.println(label.getString());
			}
			// System.out.println(res.getURI() + "---" +
			// res.getProperty(RDFS.label).getString());
		}

	}
}
