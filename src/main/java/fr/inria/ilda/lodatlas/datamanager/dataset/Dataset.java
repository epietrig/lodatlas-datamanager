/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset;

import org.bson.Document;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;

/**
 * A class that implements {@link IDataset} interface.
 * 
 * @author Hande Gözükan
 *
 */
public class Dataset implements IDataset {

	/**
	 * Unique name of the dataset in its repository.
	 */
	private final String name;

	/**
	 * Dataset BSON.
	 */
	private Document bson;

	/**
	 * The source name to get JSON document for this dataset.
	 */
	private String jsonSourceName;

	/**
	 * The repository name for this dataset.
	 */
	private String repoName;

	/**
	 * Constructs a dataset with specified parameters.
	 * 
	 * @param datasetName
	 *            unique name of the dataset in its repository.
	 * @param jsonSourceName
	 *            the name of the JSON source for this dataset.
	 * 
	 * @throws IllegalArgumentException
	 *             if <code>datasetName</code> is <code>null</code> or empty
	 *             {@link String}.
	 */
	public Dataset(String datasetName, String jsonSourceName) {
		if (datasetName == null || "".equals(datasetName))
			throw new IllegalArgumentException("Dataset name cannot be null or empty string.");
		if (jsonSourceName == null || "".equals(jsonSourceName))
			throw new IllegalArgumentException("Repository name cannot be null or empty string.");

		this.name = datasetName;
		this.jsonSourceName = jsonSourceName;
	}

	/**
	 * Constructs a dataset with specified parameters.
	 * 
	 * @param datasetName
	 *            unique name of the dataset in its repository.
	 * @param jsonSourceName
	 *            the name of the JSON source for this dataset.
	 * @param repoName
	 *            name of the repository where JSON of this dataset can be
	 *            downloaded.
	 * 
	 * @throws IllegalArgumentException
	 *             if <code>datasetName</code> is <code>null</code> or empty
	 *             {@link String}.
	 */
	public Dataset(String datasetName, String jsonSourceName, String repoName) {
		this(datasetName, jsonSourceName);
		if (repoName != null || !"".equals(repoName)) {
			this.repoName = repoName;
		}
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getJsonSourceName() {
		return this.jsonSourceName;
	}

	@Override
	public String getRepoName() {
		return this.repoName;
	}

	@Override
	public void setBson(Document bson) {
		if (bson == null) {
			return;
		}

		if (bson.containsKey(DatasetStrings.NAME) && this.name.equals(bson.getString(DatasetStrings.NAME))) {
			this.bson = bson;
		}
		if (this.repoName == null && bson.containsKey(DatasetStrings.REPO_NAME)) {
			this.repoName = bson.getString(DatasetStrings.REPO_NAME);
		}
	}

	@Override
	public Document getBson() {
		return bson;
	}

}
