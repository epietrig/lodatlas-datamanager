/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.datamanager.dataset.util.MongoUtil;
import fr.inria.ilda.lodatlas.datamanager.exception.NoRepoExistsException;
import fr.inria.ilda.lodatlas.datamanager.repository.IRepoApiConnector;
import fr.inria.ilda.lodatlas.datamanager.repository.MongoRepoConnector;
import fr.inria.ilda.lodatlas.datamanager.repository.RepositoryFactory;

/**
 * An iterator used to load dataset list from multiple repositories and iterate
 * over them.
 * 
 * Repository list should be specified by a file path.
 * 
 * @author Hande Gözükan
 *
 */
public class DatasetIterator implements Iterator<IDataset> {

	private final static Logger logger = LoggerFactory.getLogger(DatasetIterator.class);

	/**
	 * The list of repositories to connect and fetch datasets from.
	 */
	private final Map<String, IRepoApiConnector> repoConnectorList = new HashMap<>();

	/**
	 * Repository connector that is being processed currently.
	 */
	private IRepoApiConnector currentConnector;

	/**
	 * Iterator that iterates on repository connectors.
	 */
	private Iterator<IRepoApiConnector> topIterator;

	/**
	 * Iterator that iterates on the dataset names for a specific repository
	 * connector.
	 */
	private Iterator<IDataset> nestedIterator;

	// TODO can add reading dataset list from file with repo info

	/**
	 * Constructs a dataset iterator which gets dataset list from the repositories
	 * listed in the specified <code>repoListPath</code> CSV file.
	 *
	 * @param repoListPath
	 *            the file that contains list of repositories and their
	 *            corresponding URLs to download dataset.
	 * @throws IOException
	 *             if there are problems with the file specified by path
	 *             <code>repoListPath</code>.
	 * @throws NoRepoExistsException
	 *             if no repository could be created with the list of the
	 *             repositories in the file specified by <code>repoListPath</code>.
	 */
	public DatasetIterator(Path repoListPath) throws IOException, NoRepoExistsException {
		assert (repoListPath != null);
		readFromCsv(repoListPath);

		if (repoConnectorList.size() > 0) {
			setInnerIterators();
		} else {
			throw new NoRepoExistsException();
		}
	}

	/**
	 * Constructs a dataset iterator which gets dataset list from the repositories
	 * listed in the specified <code>repoListPath</code> CSV file.
	 *
	 * @param repoListPath
	 *            the file that contains list of repositories and their
	 *            corresponding URLs to download dataset.
	 * @throws IOException
	 *             if there are problems with the file specified by path
	 *             <code>repoListPath</code>.
	 * @throws NoRepoExistsException
	 *             if no repository could be created with the list of the
	 *             repositories in the file specified by <code>repoListPath</code>.
	 */
	public DatasetIterator(MongoUtil mongoUtil) {
		repoConnectorList.put(MongoRepoConnector.JSON_SOURCE, new MongoRepoConnector(mongoUtil));
		if (logger.isInfoEnabled()) {
			logger.info("No repository list is specified, will get dataset list from MongoDB");
		}
		setInnerIterators();
	}

	private void setInnerIterators() {
		topIterator = repoConnectorList.values().iterator();
		currentConnector = topIterator.next();
		nestedIterator = currentConnector.getNextChunkList().iterator();
	}

	/**
	 * Reads the CSV file with specified path <code>filePath</code> and creates the
	 * repository connectors and puts them in the list of repository connectors.
	 * <br>
	 * The file should have api, repository name, repository URL and query string
	 * values.
	 * 
	 * @param filePath
	 *            path of the CSV file that contains repository data.
	 * @throws IOException
	 */
	private void readFromCsv(Path filePath) throws IOException {
		Scanner scanner = new Scanner(filePath);
		IRepoApiConnector repoConnector = null;
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			String[] lineArray = line.split(",");

			String api = lineArray[0];
			String repoName = lineArray[1];
			String repoUrl = lineArray[2];
			String query = lineArray.length >= 4 ? lineArray[3] : "";
			int startIndex = lineArray.length >= 5 ? Integer.parseInt(lineArray[4]) : 0;
			int totalCount = lineArray.length >= 6 ? Integer.parseInt(lineArray[5]) : -1;

			// getRepoConnector method will check if the values read from file are
			// convenient, and return null if not.
			repoConnector = RepositoryFactory.getRepoConnector(api, repoName, repoUrl, query, startIndex, totalCount);

			// No need to check if repoName is null, if it is null, repoConnector will
			// return null
			if (repoConnector != null) {
				repoConnectorList.put(repoName, repoConnector);
			}
		}
		scanner.close();
	}

	@Override
	public boolean hasNext() {
		if (!nestedIterator.hasNext()) {
			if (currentConnector.hasNextChunk()) {
				nestedIterator = currentConnector.getNextChunkList().iterator();
			} else {
				if (topIterator.hasNext()) {
					currentConnector = topIterator.next();
					nestedIterator = currentConnector.getNextChunkList().iterator();
				}
			}
		}
		return nestedIterator.hasNext();
	}

	@Override
	public IDataset next() {
		if (nestedIterator.hasNext()) {
			return nestedIterator.next();
		} else {
			return null;
		}
	}

	/**
	 * TODO
	 */
	public void reset() {
		topIterator = repoConnectorList.values().iterator();
		while (topIterator.hasNext()) {
			topIterator.next().reset();
		}
		topIterator = repoConnectorList.values().iterator();
		nestedIterator = topIterator.next().getNextChunkList().iterator();
	}

	public Map<String, IRepoApiConnector> getRepositoryMap() {
		return repoConnectorList;
	}

}
