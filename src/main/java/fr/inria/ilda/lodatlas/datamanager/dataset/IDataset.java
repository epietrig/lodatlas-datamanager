/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset;

import org.bson.Document;

/**
 * An interface that represents a dataset from a dataset repository.<br>
 *
 * Name of a dataset and its repository name, uniquely identifies a dataset. A
 * dataset can have many resources defined. A resource can be a file, or a
 * compressed or/and archived file.
 * 
 * @author Hande Gözükan
 *
 */
public interface IDataset {

	/**
	 * Gets unique name of the dataset in a dataset repository.
	 * 
	 * @return unique name of the dataset in a dataset repostitory.
	 */
	public String getName();
	
	/**
	 * Returns the source name to get JSON document for this dataset.
	 * 
	 * @return the source name to get JSON document for this dataset.
	 */
	public String getJsonSourceName();
	
	/**
	 * Returns repository name for this dataset.
	 * 
	 * @return repository name for this dataset.
	 */
	public String getRepoName();

	/**
	 * Sets BSON document for the dataset if name field exists in the
	 * <code>bson</code> document and its value is the same as the name of this
	 * dataset.
	 * 
	 * @param bson
	 *            BSON document for the dataset.
	 */
	public void setBson(Document bson);

	/**
	 * Returns BSON document for the dataset.
	 * 
	 * @return BSON document for the dataset.
	 */
	public Document getBson();

}
