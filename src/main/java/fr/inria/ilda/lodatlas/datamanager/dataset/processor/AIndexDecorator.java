/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import java.util.Properties;

import fr.inria.ilda.lodatlas.commons.es.EsUtil;
import fr.inria.ilda.lodatlas.datamanager.ProcessManager;

/**
 * An abstract class for a decorator which creates elasticsearch indexes.
 * 
 * @author Hande Gözükan
 *
 */
public abstract class AIndexDecorator extends AProcessorDecorator {

	protected final static Properties prop = ProcessManager.prop;

	protected final EsUtil esUtil;

	protected String indexName;

	protected String mappingType;

	/**
	 * 
	 * @param datasetProcessor
	 */
	public AIndexDecorator(IDatasetProcessor datasetProcessor, EsUtil esUtil) {
		super(datasetProcessor);

		assert (esUtil != null);
		this.esUtil = esUtil;
	}

}
