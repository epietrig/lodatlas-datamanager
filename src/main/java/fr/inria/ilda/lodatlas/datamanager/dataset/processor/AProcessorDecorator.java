/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;

/**
 * An abstract decorator class that implements {@link IDatasetProcessor}
 * interface and has an instance of {@link IDatasetProcessor}.
 * 
 * @author Hande Gözükan
 *
 */
public abstract class AProcessorDecorator implements IDatasetProcessor {

	/**
	 * {@link IDatasetProcessor} instance
	 */
	protected final IDatasetProcessor datasetProcessor;

	/**
	 * Constructs a decorator which has an {@link IDatasetProcessor} instance.
	 * 
	 * @param datasetProcessor
	 *            the dataset processor which will process the dataset before this
	 *            class processes the dataset.
	 */
	public AProcessorDecorator(IDatasetProcessor datasetProcessor) {
		assert (datasetProcessor != null);
		this.datasetProcessor = datasetProcessor;
	}

	@Override
	public void processDataset(IDataset dataset) {
		this.datasetProcessor.processDataset(dataset);
		process(dataset);
	}

	/**
	 * To be implemented by each processor for its specific process.
	 * 
	 * @param dataset
	 *            dataset to process.
	 */
	protected abstract void process(IDataset dataset);
}
