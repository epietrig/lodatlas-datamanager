/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import java.io.FileNotFoundException;

import org.elasticsearch.action.index.IndexResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.commons.es.EsStrings;
import fr.inria.ilda.lodatlas.commons.es.EsUtil;
import fr.inria.ilda.lodatlas.commons.es.exceptions.MappingFileException;
import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;

/**
 * A decorator which generates Elasticsearch index for datasets.
 * 
 * @author Hande Gözükan
 *
 */
public class DatasetIndexDecorator extends AIndexDecorator {

	private static final Logger logger = LoggerFactory.getLogger(DatasetIndexDecorator.class);

	private boolean canIndex = false;

	/**
	 * Constructs a decorator to generate Elasticsearch indices for datasets.
	 * 
	 * @param datasetProcessor
	 * @param esUtil
	 * @param deleteIndexIfExists
	 */
	public DatasetIndexDecorator(IDatasetProcessor datasetProcessor, EsUtil esUtil, boolean deleteIndexIfExists) {

		super(datasetProcessor, esUtil);

		this.indexName = prop.getProperty(EsStrings.DATASET_INDEX);
		this.mappingType = prop.getProperty(EsStrings.DATASET_TYPE);

		try {
			canIndex = this.esUtil.createIndex(indexName, mappingType, prop.getProperty(EsStrings.DATASET_MAPPING),
					prop.getProperty(EsStrings.ANALYZERS_FILE), deleteIndexIfExists);
		} catch (FileNotFoundException e) {
			logger.error("The specified file does not exist on classpath.", e);
		} catch (MappingFileException e) {
			logger.error("Specified mapping file has problems.", e);
		}
	}

	@Override
	protected void process(IDataset dataset) {
		if (logger.isInfoEnabled())
			logger.info("\tIndexing dataset " + dataset.getName());

		if (!canIndex) {
			if (logger.isWarnEnabled()) {
				logger.warn("Index is not created, cannot index.");
				return;
			}
		}

		if (dataset.getBson() != null) {

			dataset.getBson().remove("_id");
			try {
				// System.out.println(dataset.getBson().get(DatasetStrings.TRIPLE_COUNT).toString());
				// Elasticsearch does not index when long values are returned as
				// NumberLong(...), however couldn't find a way to get JSON from mongo BSon to
				// remove NumberLong when converting to json string
				String tripleCount = dataset.getBson().get(DatasetStrings.TRIPLE_COUNT).toString();
				// System.out.println(tripleCount);
				dataset.getBson().put(DatasetStrings.TRIPLE_COUNT, tripleCount);
				IndexResponse response = this.esUtil.indexDocument(indexName, mappingType, dataset.getName(),
						dataset.getBson().toJson());

				if (logger.isDebugEnabled())
					logger.debug("Index name " + response.getIndex() + " type: " + response.getType() + " id: "
							+ response.getId() + " version " + response.getVersion());
			} catch (Exception e) {
				logger.error("Exception  " + e.getMessage() + " while parsing " + dataset.getName(), e);
			}
		} else {
			if (logger.isWarnEnabled())
				logger.warn("Dataset JSON is null for dataset " + dataset.getName()
						+ " and no elasticsearch index is created.");
		}
	}

}
