/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;

/**
 * A concrete implementation of {@link IDatasetProcessor} to log the information on
 * the dataset currently being processed.
 * 
 * @author Hande Gözükan
 *
 */
public class DatasetProcessor implements IDatasetProcessor {

	private static final Logger logger = LoggerFactory.getLogger(DatasetProcessor.class);

	@Override
	public void processDataset(IDataset dataset) {
		if (dataset == null)
			throw new IllegalArgumentException("The dataset must not be null.");

		if (logger.isInfoEnabled()) {
			logger.info("");
			logger.info("\t#############################################");
			logger.info("\tProcessing dataset: " + dataset.getName());
			logger.info("\t#############################################");
			logger.info("");
		}
	}
}
