/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.MongoUtil;

/**
 * A decorator which reads dataset JSON from database.
 * 
 * @author Hande Gözükan
 *
 */
public class DbReadDecorator extends AProcessorDecorator {

	private static final Logger logger = LoggerFactory.getLogger(DbReadDecorator.class);

	private final MongoUtil mongoUtil;

	/**
	 * Constructs a decorator to read dataset JSON from database.
	 * 
	 * @param datasetProcessor
	 * @param collection
	 *            TODO
	 */
	public DbReadDecorator(IDatasetProcessor datasetProcessor, MongoUtil mongoUtil) {
		super(datasetProcessor);

		assert (mongoUtil != null);
		this.mongoUtil = mongoUtil;
	}

	@Override
	protected void process(IDataset dataset) {
		if (logger.isInfoEnabled())
			logger.info("\tLoading dataset JSON for " + dataset.getName() + " from mongo database");

		Document result = mongoUtil.findDataset(dataset.getName());
		if (result != null) {
			dataset.setBson(result);
			if (logger.isDebugEnabled())
				logger.debug("Retrieved document from mongodb for dataset " + dataset.getName());
		} else {
			if (logger.isWarnEnabled())
				logger.warn("No document exists in mongodb for dataset " + dataset.getName());
		}
	}

}
