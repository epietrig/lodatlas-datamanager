/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import java.util.Map;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.MongoUtil;
import fr.inria.ilda.lodatlas.datamanager.repository.IRepoApiConnector;

/**
 * A concrete dataset processing decorator which downloads dataset JSON from
 * dataset repository.
 * 
 * @author Hande Gözükan
 *
 */
public class DownloadDecorator extends AProcessorDecorator {

	private static final Logger logger = LoggerFactory.getLogger(DownloadDecorator.class);

	private final Map<String, IRepoApiConnector> repoList;

	private final MongoUtil mongoUtil;

	/**
	 * Constructs a decorator to download package JSON from dataset repository.
	 * 
	 * @param datasetProcessor
	 */
	public DownloadDecorator(IDatasetProcessor datasetProcessor, Map<String, IRepoApiConnector> repositoryList,
			MongoUtil mongoUtil) {
		super(datasetProcessor);

		assert (repositoryList != null);
		this.repoList = repositoryList;

		assert (mongoUtil != null);
		this.mongoUtil = mongoUtil;
	}

	@Override
	protected void process(IDataset dataset) {
		if (logger.isInfoEnabled())
			logger.info("\tDownloading dataset JSON");

		// Do not download datasets that already exists in the database
		if (this.mongoUtil.findDataset(dataset.getName()) == null) {

			IRepoApiConnector repo = repoList.get(dataset.getJsonSourceName());
			if (repo == null) {
				if (logger.isWarnEnabled())
					logger.warn("\tNo repo exists for dataset.");
				return;
			}

			String datasetJson = repo.getDatasetJson(dataset.getName());
			if (datasetJson == null) {
				if (logger.isWarnEnabled())
					logger.warn("\tDataset JSON could not be downloaded.");
				return;
			}
			dataset.setBson(Document.parse(datasetJson));
		} else {
			if (logger.isInfoEnabled())
				logger.info("\tThe dataset already exists in the database, will not download.");
		}
	}

}
