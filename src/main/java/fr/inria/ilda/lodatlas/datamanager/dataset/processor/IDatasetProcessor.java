/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;

/**
 * Interface for a dataset processor. <br>
 * 
 * Decorator pattern is used for dataset processing. This interface should be
 * implemented by any decorator to be used as a dataset processor.
 * 
 * @author Hande Gözükan
 *
 */
public interface IDatasetProcessor {

	/**
	 * Processes the specified dataset <code>dataset</code>.
	 * 
	 * @param dataset
	 *            dataset to process.
	 */
	public void processDataset(IDataset dataset);

}
