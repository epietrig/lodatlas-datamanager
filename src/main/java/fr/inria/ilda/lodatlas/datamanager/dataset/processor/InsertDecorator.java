/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.MongoUtil;

/**
 * A decorator which inserts dataset JSON to Mongo database.
 * 
 * @author Hande Gözükan
 *
 */
public class InsertDecorator extends AProcessorDecorator {

	private static final Logger logger = LoggerFactory.getLogger(InsertDecorator.class);

	private final MongoUtil mongoUtil;

	/**
	 * Constructs a decorator to insert dataset JSON to Mongo database.
	 * 
	 * @param datasetProcessor
	 *            TODO
	 * @param mongoUtil
	 *            TODO
	 */
	public InsertDecorator(IDatasetProcessor datasetProcessor, MongoUtil mongoUtil) {
		super(datasetProcessor);

		assert (mongoUtil != null);
		this.mongoUtil = mongoUtil;
	}

	@Override
	protected void process(IDataset dataset) {
		if (logger.isInfoEnabled())
			logger.info("\tInserting dataset JSON for dataset " + dataset.getName() + " to mongodb.");

		if (dataset.getBson() != null) {
			this.mongoUtil.insertDataset(dataset.getBson());
		} else {
			if (logger.isWarnEnabled())
				logger.warn("Dataset JSON is null for dataset " + dataset.getName()
						+ " and is not inserted to mongodb.");
		}
	}

}
