/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import java.util.List;
import java.util.function.Consumer;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.MongoUtil;

/**
 * A decorator which updates the incoming link list of the packages whose name
 * is listed in the outgoing link list of a specific package.
 * 
 * @author Hande Gözükan
 *
 */
public class LinkUpdateDecorator extends AProcessorDecorator {

	private static final Logger logger = LoggerFactory.getLogger(LinkUpdateDecorator.class);

	private final MongoUtil mongoUtil;

	/**
	 * Constructs a decorator to update incoming links of packages listed in the
	 * outgoing links of a package.
	 * 
	 * @param pkgProcessor
	 * @param collection
	 */
	public LinkUpdateDecorator(IDatasetProcessor pkgProcessor, MongoUtil mongoUtil) {
		super(pkgProcessor);

		assert (mongoUtil != null);
		this.mongoUtil = mongoUtil;
	}

	/**
	 * If there is no outgoing links defined for the dataset specified, does
	 * nothing.
	 * 
	 * If there exists an outgoing link, and the dataset for that link does not
	 * exist in the database, does nothing for that link.
	 * 
	 * If the dataset for the outgoing link exists, increments the count value for
	 * incoming links node and adds the name of dataset specified as a link to the
	 * incoming links node. It does not check if a link with the same dataset name
	 * already exists.
	 */
	@Override
	protected void process(IDataset dataset) {
		if (logger.isInfoEnabled())
			logger.info("\tUpdating links for package " + dataset.getName());

		Document pkgDoc = mongoUtil.findDataset(dataset.getName());

		if (pkgDoc != null) {
			Document outgoingLinksNode = (Document) pkgDoc.get(DatasetStrings.OUTGOING_LINKS);
			if (outgoingLinksNode != null) {
				List<Document> linkList = (List<Document>) outgoingLinksNode.get(DatasetStrings.LINKS);

				if (linkList != null) {

					linkList.forEach(new Consumer<Document>() {

						@Override
						public void accept(Document doc) {
							// outgoing link name will be the dataset name to
							// update
							String outgoinglinkName = doc.getString(DatasetStrings.NAME);
							int linkCount = doc.getInteger(DatasetStrings.COUNT);

							Document linkObject = new Document();
							linkObject.append(DatasetStrings.NAME, dataset.getName()).append(DatasetStrings.COUNT,
									linkCount);
							Document linkAddObj = new Document("$push", new Document(
									DatasetStrings.INCOMING_LINKS + "." + DatasetStrings.LINKS, linkObject));
							linkAddObj.append("$inc",
									new Document(DatasetStrings.INCOMING_LINKS + "." + DatasetStrings.COUNT, 1));

							Document result = mongoUtil
									.updateDataset(new Document(DatasetStrings.NAME, outgoinglinkName)
											.append(DatasetStrings.REPO_NAME, dataset.getRepoName()), linkAddObj);

							if (result == null && logger.isWarnEnabled()) {
								logger.warn("No document exist for the outgoing link " + outgoinglinkName
										+ "of package " + dataset.getName());
							}
						}

					});
				}
			}

		} else {
			if (logger.isWarnEnabled())
				logger.warn("No JSON exists in mongo db for package " + dataset.getName());
		}
	}

}
