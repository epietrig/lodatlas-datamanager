/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import java.io.FileNotFoundException;

import org.bson.Document;
import org.elasticsearch.action.index.IndexResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.Block;
import com.mongodb.client.FindIterable;

import fr.inria.ilda.lodatlas.commons.es.EsStrings;
import fr.inria.ilda.lodatlas.commons.es.EsUtil;
import fr.inria.ilda.lodatlas.commons.es.exceptions.MappingFileException;
import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.MongoUtil;

/**
 * 
 * @author Hande Gözükan
 *
 */
public class ResourceIndexDecorator extends AIndexDecorator {

	private static final Logger logger = LoggerFactory.getLogger(ResourceIndexDecorator.class);

	private boolean canIndex = false;

	private final MongoUtil mongoUtil;

	/**
	 * Constructs a decorator to generate elastic search index for processed files
	 * of resources.
	 * 
	 * It uses already existing elasticsearch index, or creates one if it does not
	 * exist.
	 * 
	 * @param datasetProcessor
	 */
	public ResourceIndexDecorator(IDatasetProcessor datasetProcessor, MongoUtil mongoUtil, EsUtil esUtil,
			boolean deleteIfExists) {

		super(datasetProcessor, esUtil);

		assert (mongoUtil != null);
		this.mongoUtil = mongoUtil;

		this.indexName = prop.getProperty(EsStrings.PROCESSED_FILE_INDEX);
		this.mappingType = prop.getProperty(EsStrings.PROCESSED_FILE_TYPE);

		try {
			canIndex = esUtil.createIndex(indexName, mappingType, prop.getProperty(EsStrings.PROCESSED_FILE_MAPPING),
					prop.getProperty(EsStrings.ANALYZERS_FILE), deleteIfExists);
		} catch (FileNotFoundException e) {
			logger.error("The specified file does not exist on classpath.", e);
		} catch (MappingFileException e) {
			logger.error("Specified mapping file has problems.", e);
		}
	}

	@Override
	protected void process(IDataset dataset) {
		if (logger.isInfoEnabled())
			logger.info("\tIndexing dataset " + dataset.getName() + " for processed files collection");

		if (!canIndex) {
			if (logger.isWarnEnabled()) {
				logger.warn("Index is not created, cannot index.");
				return;
			}
		}

		if (dataset.getBson() != null) {

			FindIterable<Document> result = mongoUtil.getProcessedFiles(dataset.getRepoName(), dataset.getName());

			logger.info(dataset.getRepoName());
			logger.info(dataset.getName());
			logger.info("Result is null " + (result.first() == null));

			result.forEach(new Block<Document>() {

				@Override
				public void apply(Document processedFile) {
					processedFile.remove("_id");

					try {
						IndexResponse response = esUtil.indexDocument(indexName, mappingType, null,
								processedFile.toJson());

						if (logger.isDebugEnabled())
							logger.debug("Index name " + response.getIndex() + " type: " + response.getType() + " id: "
									+ response.getId() + " version " + response.getVersion());
					} catch (Exception e) {
						logger.error("Exception  " + e.getMessage() + " while parsing " + dataset.getName(), e);
					}
				}
			});
		} else {
			if (logger.isWarnEnabled())
				logger.warn("Package JSON is null for package " + dataset.getName()
						+ " and no elasticsearch index is created.");
		}
	}

}
