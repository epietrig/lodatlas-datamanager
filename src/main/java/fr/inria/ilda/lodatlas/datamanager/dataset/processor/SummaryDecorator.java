/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.io.FileUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;
import fr.inria.ilda.lodatlas.datamanager.dataset.resource.IResource;
import fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource;
import fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump.ResourceProcessor;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.MongoUtil;

/**
 * A decorator which downloads dataset resource files and generates RDF
 * summaries.
 * 
 * @author Hande Gözükan
 *
 */
public class SummaryDecorator extends AProcessorDecorator {

	private static final Logger logger = LoggerFactory.getLogger(SummaryDecorator.class);

	private final Path tempProcessPath;

	private final ResourceProcessor resourceProcessor;

	/**
	 * Constructs a decorator to generate summaries for dataset resources.
	 * 
	 * @param datasetProcessor
	 */
	public SummaryDecorator(IDatasetProcessor datasetProcessor, Path tempProcessPath, MongoUtil mongoUtil) {
		super(datasetProcessor);

		assert (tempProcessPath != null);
		assert (mongoUtil != null);

		this.tempProcessPath = tempProcessPath;
		this.resourceProcessor = new ResourceProcessor(mongoUtil, this.tempProcessPath);
	}

	@Override
	protected void process(IDataset dataset) {
		if (logger.isInfoEnabled())
			logger.info("\tGenerating summaries for dataset " + dataset.getName());

		if (dataset.getBson() != null) {

			Path datasetPath = Paths.get(this.tempProcessPath.toString(), dataset.getName());

			List<Document> resources = (List<Document>) dataset.getBson().get(DatasetStrings.RESOURCES);

			if (logger.isDebugEnabled())
				logger.debug("Dataset has " + resources.size() + " resources");

			resources.forEach(new Consumer<Document>() {

				@Override
				public void accept(Document resourceDoc) {
					IResource resource = new Resource(dataset.getRepoName(), dataset.getName(), resourceDoc);
					resourceProcessor.processResource(datasetPath, resource);
				}
			});

			try {
				if (datasetPath.toFile().exists()) {
					FileUtils.cleanDirectory(datasetPath.toFile());
				} else {
					if (logger.isWarnEnabled())
						logger.warn("The path to dataset does not exist. Cannot delete.");
				}
			} catch (IOException e) {
				if (logger.isWarnEnabled())
					logger.warn("Exception while trying to delete dataset path " + datasetPath.toString(), e);
			}

		} else {
			if (logger.isWarnEnabled())
				logger.warn(
						"Dataset JSON is null for dataset " + dataset.getName() + " and cannot generate RDF summary.");
		}
	}

}
