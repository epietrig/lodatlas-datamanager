/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.resource;

import java.net.URL;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.bson.Document;

import fr.inria.ilda.lodatlas.commons.model.FormatType;

/**
 * An interface that represents a resource of a dataset from a dataset
 * repository.
 * 
 * @author Hande Gözükan
 *
 */
public interface IResource {

	/**
	 * Gets the id of the resource.
	 * 
	 * @return id of the resource.
	 */
	public String getResourceId();

	/**
	 * Gets the repository name of this resource.
	 * 
	 * @return repository name for this resource.
	 */
	public String getRepoName();

	/**
	 * Gets the dataset name of this resource.
	 * 
	 * @return dataset name for this resource.
	 */
	public String getDatasetName();

	/**
	 * Gets the verified format of this resource.
	 * 
	 * @return verified format of this resource.
	 */
	public FormatType getVerifiedFormat();

	/**
	 * Checks if the specified resource is a linked dataset verifying its format
	 * field.
	 * 
	 * @return <code>true</code> if the resource is verified to to a linked dataset;
	 *         <code>false</code> otherwise
	 */
	public boolean isLinkedData();

	/**
	 * Gets the error encountered while downloading resource dump.
	 * 
	 * @return the error encountered while downloading resource dump. Empty string
	 *         if the dump is downloaded withour any problems.
	 */
	public String getDownloadError();

	/**
	 * Sets the error encountered for downloading the dump file.
	 * 
	 * @param error
	 *            error for downloading the dump file.
	 * @throws IllegalArgumentException
	 *             if the specified <code>error</code> is <code>null</code> or empty
	 *             string.
	 */
	public void setDownloadError(String error);

	/**
	 * Gets the content type specified by HTTP response while downloading.
	 * 
	 * @return the content type specified by HTTP response while downloading.
	 */
	public String getContentType();

	/**
	 * Sets the size of the dump file for this resource.
	 * 
	 * @param contentLength
	 *            the size of the dump file for this resource.
	 * @throws IllegalArgumentException
	 *             if the specified <code>contentLength</code> is less than 0.
	 */
	public void setContentLength(long contentLength);

	/**
	 * Gets the content length specified by HTTP response while downloading.
	 * 
	 * @return the content length specified by HTTP response while downloading.
	 */
	public long getContentLength();

	/**
	 * Checks whether this resource has a dump as linked data that can be
	 * downloaded.
	 * 
	 * This is decided depending on the verified format of the resource. If it is
	 * not a linked data resource, or it is a sparql endpoint or if it is a meta
	 * file for a linked data set is is not downloaded.
	 * 
	 * @return <code>true</code> if the dump of this resource can be downloaded;
	 *         <code>false</code> otherwise.
	 */
	public boolean canDownloadDump();

	/**
	 * Gets the absolute path the dump file.
	 * 
	 * @return absolute path of the dump file; <code>null</code> if no dump exists.
	 */
	public Path getDumpPath();

	/**
	 * Checks whether the dump file exists on local disk for this resource.
	 * 
	 * @return <code>true</code> if dump for the resource is downloaded;
	 *         <code>false</code> otherwise.
	 */
	public boolean hasDump();

	/**
	 * Checks whether the dump file is compressed for this resource.
	 * 
	 * @return <code>true</code> if dump for the resource is compressed;
	 *         <code>false</code> otherwise.
	 */
	public boolean isCompressed();

	/**
	 * Sets the name of the dump file for this resource.
	 * 
	 * @param fileName
	 *            name of the dump file for this resource.
	 */
	public void setDumpFileName(String fileName);

	/**
	 * Sets the path of the dump file for this resource.
	 * 
	 * @param filePath
	 *            path of the dump file for this resource.
	 */
	public void setDumpFilePath(Path filePath);

	/**
	 * Gets the complete file name of the resource.
	 * 
	 * @return the complete file name of the resource.
	 */
	public String getDumpFileName();

	/**
	 * Gets the file name without extensions.
	 * 
	 * @return the file name without any file extensions
	 */
	public String getDumpFileNameNoX();

	/**
	 * Gets the URL of the resource dump.
	 * 
	 * @return the URL of the resource dump.
	 */
	public URL getUrl();

	/**
	 * Gets BSON document for this resource.
	 * 
	 * @return BSON document for this resource.
	 */
	public Document getDoc();

	/**
	 * Sets the specified <code>contentType</code> for the resource and updates the
	 * verified format accordingly.
	 * 
	 * @param contentType
	 *            mime type of the resource.
	 */
	public void setContentType(String contentType);

	/**
	 * Creates a {@link ProcessedFile} instance for the dump file currently being
	 * processed for this resource.
	 * 
	 * @param originalFileName
	 *            original name of the dump file.
	 * @return {@link ProcessedFile} instance for the dump file that is currently
	 *         being processed.
	 */
	public ProcessedFile createCurrentProcessedFile(String originalFileName);

	/**
	 * Getter method for the currentProcessedFile.
	 * 
	 * @return currently being processed resource dump file.
	 */
	public ProcessedFile getCurrentProcessedFile();

	/**
	 * Resets the currently being processed file to <code>null</code>.
	 */
	public void resetProcessedFile();

	/**
	 * Returns the list of processed files in short form for the resource.
	 * 
	 * @return returns the list of processed files in short form for the resource.
	 */
	public List<ShortProcessedFile> getProcessedFiles();

	public Set<String> getClassSet();

	public void addClasses(Collection<String> classes);

	public Set<String> getPropertySet();

	public void addProperties(Collection<String> properties);

	public Set<String> getVocabularySet();

	public void addVocabularies(Collection<String> vocabularies);

	public Set<String> getClassLabelSet();

	public void addClassLabels(Collection<String> classLabels);

	public Set<String> getPropertyLabelSet();

	public void addPropertyLabels(Collection<String> propertyLabels);

}
