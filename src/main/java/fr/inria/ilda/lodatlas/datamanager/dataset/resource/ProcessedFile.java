/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.resource;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

public class ProcessedFile {

	private final String repoName;

	private final String datasetName;

	private final String resourceId;

	private String fileName;

	private final String originalFileName;

	private long tripleCount;

	private String error;

	private boolean hasError;

	private List<String> usedClasses = new ArrayList<>();

	private List<String> usedProperties = new ArrayList<>();

	private List<String> classLabels = new ArrayList<>();

	private List<String> propertyLabels = new ArrayList<>();

	private List<String> vocabularies = new ArrayList<>();

	private Document heb;

	private Document fd;

	public ProcessedFile(String repoName, String datasetName, String resourceId, String originalFileName) {
		this.repoName = repoName;
		this.datasetName = datasetName;
		this.resourceId = resourceId;
		this.originalFileName = originalFileName;
	}

	public String getRepoName() {
		return repoName;
	}

	public String getDatasetName() {
		return datasetName;
	}

	public String getResourceId() {
		return resourceId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public long getTripleCount() {
		return tripleCount;
	}

	public void setTripleCount(long tripleCount) {
		this.tripleCount = tripleCount;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public boolean isHasError() {
		return hasError;
	}

	public void setHasError(boolean hasError) {
		this.hasError = hasError;
	}

	public List<String> getUsedClasses() {
		return usedClasses;
	}

	public void setUsedClasses(List<String> usedClasses) {
		this.usedClasses = usedClasses;
	}

	public List<String> getUsedProperties() {
		return usedProperties;
	}

	public void setUsedProperties(List<String> usedProperties) {
		this.usedProperties = usedProperties;
	}

	public List<String> getClassLabels() {
		return classLabels;
	}

	public void setClassLabels(List<String> classLabels) {
		this.classLabels = classLabels;
	}

	public List<String> getPropertyLabels() {
		return propertyLabels;
	}

	public void setPropertyLabels(List<String> propertyLabels) {
		this.propertyLabels = propertyLabels;
	}

	public List<String> getVocabularies() {
		return vocabularies;
	}

	public void setVocabularies(List<String> vocabularies) {
		this.vocabularies = vocabularies;
	}

	public Document getHeb() {
		return heb;
	}

	public void setHeb(Document heb) {
		this.heb = heb;
	}

	public Document getFd() {
		return fd;
	}

	public void setFd(Document fd) {
		this.fd = fd;
	}

	public ShortProcessedFile getShort() {
		return new ShortProcessedFile(fileName, originalFileName, error, hasError, tripleCount);
	}
}
