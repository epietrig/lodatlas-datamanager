/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.resource;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.commons.model.FormatType;
import fr.inria.ilda.lodatlas.commons.strings.ResourceStrings;

/**
 * A class that implements {@link IResource} interface. This class represents an
 * RDF resource of a dataset. A resource defines a dump file, metadata file, or
 * a sparql endpoint for a dataset.<br>
 * 
 * The resource file for a {@link IResource} should be processed, hence
 * downloaded, only if it is a dump file in one of the RDF formats.
 * 
 * @author Hande Gözükan
 *
 */
public class Resource implements IResource {

	private static final Logger logger = LoggerFactory.getLogger(Resource.class);

	/**
	 * The Document object that contains all data of this resource.
	 */
	private final Document resourceDoc;

	/**
	 * The name of the repository that this resource's dataset belongs to.
	 */
	private final String repoName;

	/**
	 * The name of the dataset that this resource belongs to.
	 */
	private final String datasetName;

	/**
	 * The URL where the dump file for the resource is hosted.
	 */
	private URL url;

	/**
	 * ID of this resource.
	 */
	private String id;

	/**
	 * The verified RDF format of the resource dump file.
	 */
	private FormatType verifiedFormat;

	/**
	 * The content type received from the URL of the dump file of the resource.
	 */
	private String contentType = "";

	/**
	 * The content length in bytes received from the URL of the dump file of the
	 * resource.
	 */
	private long contentLength = 0;

	/**
	 * Indicates if the dump file is compressed.
	 */
	private boolean isCompressed = false;

	/**
	 * The path of the dump file on the disk.
	 */
	private Path dumpPath = null;

	/**
	 * Error encountered while downloading, if any.
	 */
	private String downloadError = "";

	/**
	 * The name of the dump file for this resource.
	 */
	private String dumpFileName = "";

	private Set<String> classSet = new HashSet<>();

	private Set<String> propertySet = new HashSet<>();

	private Set<String> vocabularySet = new HashSet<>();

	private Set<String> classLabelSet = new HashSet<>();

	private Set<String> propertyLabelSet = new HashSet<>();

	/**
	 * The original file name for the current file being processed. This is the dump
	 * file of the resource if the file is not an archive file. If the dump file is
	 * an archive file, this is the file in the archive which is currently being
	 * processed.
	 */
	private String originalFileName = null;

	private List<ShortProcessedFile> processedFileList = new LinkedList<ShortProcessedFile>();

	private ProcessedFile currentProcessedFile = null;

	/**
	 * Construct a Resource instance which belongs to the specified
	 * <code>repoName</code> <code>datasetName</code> and has the specified
	 * <code>resourceDoc</code> JSON. <br>
	 * Sets the id and URL of the resources reading from <code>resourceDoc</code>
	 * and identifies the format depending on the format field value in
	 * <code>resourceDoc</code>.
	 * 
	 * @param repoName
	 *            name of the repository that this resource's dataset belongs to.
	 * @param datasetName
	 *            name of the dataset that this resource belongs to.
	 * @param resourceDoc
	 *            the document of this resource.
	 * @throws IllegalArgumentException
	 *             if
	 *             <ul>
	 *             <li>the specified <code>repoName</code> is <code>null</code> or
	 *             empty string.</li>
	 *             <li>the specified <code>datasetName</code> is <code>null</code>
	 *             or empty string.</li>
	 *             <li>the specified <code>resourceDoc</code> is <code>null</code>.
	 *             </li>
	 *             </ul>
	 */
	public Resource(String repoName, String datasetName, Document resourceDoc) {
		if (repoName == null || "".equals(repoName))
			throw new IllegalArgumentException("The repository name cannot not be null or empty string.");
		if (datasetName == null || "".equals(datasetName))
			throw new IllegalArgumentException("The dataset name cannot not be null or empty string.");
		if (resourceDoc == null) {
			throw new IllegalArgumentException("Resource doc cannot be null.");
		}

		this.repoName = repoName;
		this.datasetName = datasetName;
		this.resourceDoc = resourceDoc;

		// TODO check if the id and url fields exist in document, write tests as well.
		this.id = resourceDoc.getString(ResourceStrings.ID);
		this.url = getURL(resourceDoc.getString(ResourceStrings.URL));

		setFormat();
	}

	/**
	 * Verifies and sets the RDF format of this resource depending on the value
	 * specified in format field of the resource Document.
	 */
	private void setFormat() {
		if (logger.isInfoEnabled())
			logger.info("---- Resource Id " + this.id + "----------------");
		String format = resourceDoc.getString(ResourceStrings.FORMAT);

		if (logger.isDebugEnabled())
			logger.debug("Format is " + format);

		if (this.url != null) {
			this.verifiedFormat = this.url.toString().matches("(?i:.*sparql.*)") ? FormatType.SPARQL
					: FormatType.getFormat(format);
		} else {
			this.verifiedFormat = FormatType.getFormat(format);
		}

		if (!this.verifiedFormat.isLinkedDataFormat()) {
			setDownloadError("Not a linked data format: " + this.verifiedFormat);
		} else if (!this.verifiedFormat.canDownload()) {
			setDownloadError("Unsupported format for download: " + this.verifiedFormat);
		}

		if (logger.isDebugEnabled())
			logger.debug("Verified format is " + this.verifiedFormat);
	}

	/**
	 * Gets the {@link URL} instance for the specified <code>urlString</code>. <br>
	 * 
	 * @param urlString
	 *            URL string.
	 * @return the {@link URL} instance for the specified <code>urlString</code>. If
	 *         the <code>urlString</code> is malformed, sets download error and
	 *         returns <code>null</code>.
	 */
	private URL getURL(String urlString) {
		URL url = null;
		try {
			url = new URL(urlString);
		} catch (MalformedURLException e1) {
			logger.error("MalformedURLException for " + urlString);
			setDownloadError("MalformedURLException for " + urlString);
		}
		return url;
	}

	@Override
	public String getRepoName() {
		return this.repoName;
	}

	@Override
	public String getDatasetName() {
		return this.datasetName;
	}

	@Override
	public Document getDoc() {
		return this.resourceDoc;
	}

	@Override
	public String getResourceId() {
		return this.id;
	}

	@Override
	public URL getUrl() {
		return this.url;
	}

	@Override
	public FormatType getVerifiedFormat() {
		return this.verifiedFormat;
	}

	@Override
	public boolean isLinkedData() {
		return this.verifiedFormat.isLinkedDataFormat();
	}

	@Override
	public boolean canDownloadDump() {
		return this.verifiedFormat.canDownload();
	}

	@Override
	public String getDownloadError() {
		return this.downloadError;
	}

	@Override
	public void setDownloadError(String error) {
		if (error == null || "".equals(error)) {
			throw new IllegalArgumentException("Error cannot be null or empty string.");
		}
		if ("".equals(this.downloadError)) {
			this.downloadError = error;
		} else {
			this.downloadError += "\n" + error;
		}
	}

	@Override
	public String getContentType() {
		return this.contentType;
	}

	@Override
	public void setContentLength(long contentLength) {
		if (contentLength < 0) {
			throw new IllegalArgumentException("Content length cannot be less than 0.");
		}
		this.contentLength = contentLength;
	}

	@Override
	public long getContentLength() {
		return this.contentLength;
	}

	@Override
	public Path getDumpPath() {
		return this.dumpPath;
	}

	@Override
	public boolean hasDump() {
		if (this.dumpPath == null || "".equals(this.dumpPath.toAbsolutePath()))
			return false;
		return Files.exists(this.dumpPath);
	}

	public void setDumpFilePath(Path filePath) {
		this.dumpPath = filePath;
	}

	@Override
	public void setDumpFileName(String fileName) {
		this.dumpFileName = fileName;
		this.isCompressed = ((dumpFileName.endsWith(".tar")) || (dumpFileName.endsWith(".zip"))
				|| (dumpFileName.endsWith(".gz")) || (dumpFileName.endsWith(".gzip")) || (dumpFileName.endsWith(".tgz"))
				|| (dumpFileName.endsWith(".bz2")) || (dumpFileName.endsWith(".bzip2"))
				|| (dumpFileName.endsWith(".7z")) || (dumpFileName.endsWith(".7zip"))
				|| (dumpFileName.endsWith(".rar")));

		// Rar is an archive file format which also supports compression. Proprietary
		// format. No Java SE or apache commons API provided.
		if (dumpFileName.endsWith(".rar")) {
			setDownloadError("Cannot download dump: RAR format");
		}
	}

	@Override
	public String getDumpFileName() {
		return this.dumpFileName;
	}

	@Override
	public String getDumpFileNameNoX() {
		int index = dumpFileName.indexOf(".");
		if (index > -1) {
			return this.dumpFileName.substring(0, index);
		}
		return this.dumpFileName;
	}

	@Override
	public boolean isCompressed() {
		return this.isCompressed;
	}

	@Override
	public void setContentType(String contentType) {

		FormatType format = FormatType.getFormatByMimeType(contentType);

		if (format != this.verifiedFormat && format != FormatType.NONE) {
			this.verifiedFormat = format;

			if (!this.verifiedFormat.canDownload()) {
				setDownloadError("Unsupported content-type for download: " + contentType);
			}

			if (!this.verifiedFormat.isLinkedDataFormat()) {
				setDownloadError("Not a linked data content-type: " + contentType);
			}
		}

		this.contentType = contentType;
	}

	@Override
	public ProcessedFile createCurrentProcessedFile(String originalFileName) {
		if (this.currentProcessedFile == null) {
			this.currentProcessedFile = new ProcessedFile(repoName, datasetName, id, originalFileName);
		}
		return currentProcessedFile;
	}

	@Override
	public ProcessedFile getCurrentProcessedFile() {
		return this.currentProcessedFile;
	}

	@Override
	public void resetProcessedFile() {
		this.processedFileList.add(this.currentProcessedFile.getShort());
		this.currentProcessedFile = null;
	}

	@Override
	public List<ShortProcessedFile> getProcessedFiles() {
		return this.processedFileList;
	}

	@Override
	public Set<String> getClassSet() {
		return classSet;
	}

	@Override
	public void addClasses(Collection<String> classes) {
		this.classSet.addAll(classes);
	}

	@Override
	public Set<String> getPropertySet() {
		return propertySet;
	}

	@Override
	public void addProperties(Collection<String> properties) {
		this.propertySet.addAll(properties);
	}

	@Override
	public Set<String> getVocabularySet() {
		return vocabularySet;
	}

	@Override
	public void addVocabularies(Collection<String> vocabularies) {
		this.vocabularySet.addAll(vocabularies);
	}

	@Override
	public Set<String> getClassLabelSet() {
		return classLabelSet;
	}

	@Override
	public void addClassLabels(Collection<String> classLabels) {
		this.classLabelSet.addAll(classLabels);
	}

	@Override
	public Set<String> getPropertyLabelSet() {
		return propertyLabelSet;
	}

	@Override
	public void addPropertyLabels(Collection<String> propertyLabels) {
		this.propertyLabelSet.addAll(propertyLabels);
	}

	@Override
	public String toString() {
		return "Resource [repoName=" + repoName + ", datasetName=" + datasetName + ", url=" + url + ", id=" + id
				+ ", verifiedFormat=" + verifiedFormat + ", contentType=" + contentType + ", contentLength="
				+ contentLength + ", isCompressed=" + isCompressed + ", dumpPath=" + dumpPath + ", downloadError="
				+ downloadError + ", dumpFileName=" + dumpFileName + ", originalFileName=" + originalFileName
				+ ", processedFileList=" + processedFileList + "]";
	}

}
