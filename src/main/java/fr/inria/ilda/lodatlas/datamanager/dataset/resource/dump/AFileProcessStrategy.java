/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.datamanager.dataset.resource.IResource;

/**
 * An abstract class that implements IFileProcessStrategy interface.
 * 
 * @author Hande Gözükan
 *
 */
public abstract class AFileProcessStrategy implements IFileProcessStrategy {

	protected static final Logger logger = LoggerFactory.getLogger(AFileProcessStrategy.class);

	protected static final int BIG_SIZE = 2000000000;

	protected final ResourceProcessor resourceProcessor;

	protected IResource resource;

	public AFileProcessStrategy(ResourceProcessor dumpProcessor) {
		assert (dumpProcessor != null);
		this.resourceProcessor = dumpProcessor;
	}

	@Override
	public void processFile(IResource resource, File file) {
		this.resource = resource;
		try {
			if (logger.isInfoEnabled())
				logger.info(getStrategyName() + " to process file " + file);

			process(resource, file);

			if (logger.isDebugEnabled())
				logger.debug("Finished processing with " + getStrategyName());

		} catch (FileNotFoundException e) {
			setCurrentError(file, "FileNotFoundException while loading file " + file.getName(), e);
		} catch (IOException e) {
			setCurrentError(file, "IOException while loading file " + file.getName(), e);
		} catch (Exception e) {
			setCurrentError(file, "Exception while loading file " + file.getName(), e);
		}

		// delete dump file after processing is finished.
		if (file.exists())
			file.delete();
	}

	/**
	 * Processes the specified <code>file</code> for the specified
	 * <code>resource</code> with this IFileProcessStrategy.
	 * 
	 * @param resource
	 * @param file
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	protected abstract void process(IResource resource, File file) throws IOException, FileNotFoundException;

	/**
	 * The name of this file processing strategy.
	 * 
	 * @return
	 */
	protected abstract String getStrategyName();

	/**
	 * Saves the specified <code>stream</code> to specified <code>file</code>.
	 * 
	 * @param stream
	 *            the stream to be saved.
	 * @param file
	 *            the file to save the stream.
	 * @param size
	 *            the size of the file. The saving method for files smaller and
	 *            larger than {@link AFileProcessStrategy#BIG_SIZE} differs.
	 * @return
	 */
	protected File saveStream(InputStream stream, File file, long size) {
		try {
			FileOutputStream outFile = new FileOutputStream(file);

			// size in bytes
			if (size > BIG_SIZE) {
				IOUtils.copyLarge(stream, outFile);
			} else {
				IOUtils.copy(stream, outFile);
			}
			if (logger.isDebugEnabled())
				logger.debug("Saved file " + file);
			outFile.close();
		} catch (IOException ex) {
			setCurrentError(file, "IOException while trying to write extracted file " + file.getName(), ex);
			logger.error("Received IOException while trying to write extracted file " + file, ex);
			file.delete();
			file = null;
		}
		return file;
	}

	/**
	 * Persists the error/exception occurred during processing of this file to
	 * database.
	 * 
	 * @param resource
	 * @param currentFile
	 * @param error
	 * @param e
	 */
	protected void setCurrentError(File currentFile, String error, Exception e) {
		this.resourceProcessor.setCurrentError(resource, currentFile, error, e);
	}
}
