/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;

import fr.inria.ilda.lodatlas.datamanager.dataset.resource.IResource;

/**
 * A {@link IFileProcessStrategy} for .bz2 files.
 * 
 * Bz2 file contains a single compressed file and has extension .bz2 or bzip2.
 * 
 * @author Hande Gözükan
 *
 */
public class Bz2Strategy extends AFileProcessStrategy {

	public Bz2Strategy(ResourceProcessor dumpProcessor) {
		super(dumpProcessor);
	}

	@Override
	protected void process(IResource resource, File file) throws IOException, FileNotFoundException {
		InputStream in = new FileInputStream(file);
		BZip2CompressorInputStream bzipStream = new BZip2CompressorInputStream(in);

		String gzipFileName = file.getName().replace(".bz2", "").replace(".bzip2", "");
		File extractedFile = new File(file.getParentFile(), gzipFileName);

		File savedFile = saveStream(bzipStream, extractedFile, BIG_SIZE);

		bzipStream.close();
		in.close();

		if (savedFile != null) {
			resourceProcessor.processFile(resource, savedFile);

			// delete the file after being processed.
			savedFile.delete();
		}
	}

	@Override
	protected String getStrategyName() {
		return "Bz2Strategy";
	}

}
