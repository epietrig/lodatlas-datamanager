/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump;

import java.io.File;

import fr.inria.ilda.lodatlas.datamanager.dataset.resource.IResource;

/**
 * An interface for a file processor.
 * 
 * Implements Strategy design pattern to process files in different formats.
 * 
 * The files can be a directory, a hidden file, a file in compressed/archieved
 * format (zip, gzip, gz, 7zip, 7z tar, tgz, tar.gz, rar, bz2) or one of the rdf
 * format (rdf, nt, nq, ttl, n3, trix).
 * 
 * @author Hande Gözükan
 *
 */
public interface IFileProcessStrategy {

	/**
	 * Processes the specified <code>file</code> that belongs to the specified
	 * <code>resource</code>.
	 * 
	 * A resource can have many files compressed/archived into one dump file. Each
	 * must be processed separately.
	 * 
	 * @param resource
	 *            resource that the specified <code>file</code> belongs to.
	 * @param file
	 *            the file to be processed.
	 */
	public void processFile(IResource resource, File file);

}
