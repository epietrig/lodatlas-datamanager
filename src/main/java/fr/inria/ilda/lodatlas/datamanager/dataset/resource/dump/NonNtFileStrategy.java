/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.any23.Any23;
import org.apache.any23.ExtractionReport;
import org.apache.any23.extractor.ExtractionException;
import org.apache.any23.source.DocumentSource;
import org.apache.any23.source.FileDocumentSource;
import org.apache.any23.writer.NTriplesWriter;
import org.apache.any23.writer.TripleHandler;
import org.apache.any23.writer.TripleHandlerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.commons.model.FormatType;
import fr.inria.ilda.lodatlas.datamanager.dataset.resource.IResource;

/**
 * 
 * @author Hande Gözükan
 *
 */
public class NonNtFileStrategy extends AFileProcessStrategy {

	private static final Logger logger = LoggerFactory.getLogger(NonNtFileStrategy.class);

	public NonNtFileStrategy(ResourceProcessor dumpProcessor) {
		super(dumpProcessor);
	}

	@Override
	protected void process(IResource resource, File file) throws IOException, FileNotFoundException {

		int dotIndex = file.getName().lastIndexOf(".");
		// check file extension to see if the file is an RDF format file.
		if (dotIndex < 0 || FormatType.OTHER != FormatType.getFormat(file.getName().substring(dotIndex))) {

			Any23 any23 = new Any23();

			DocumentSource source = new FileDocumentSource(file);

			int index = file.getAbsolutePath().lastIndexOf(".");
			File outputFile = null;
			if (index == -1) {
				outputFile = new File(file.getAbsolutePath() + ".nt");
			} else {
				outputFile = new File(file.getAbsolutePath().substring(0, index) + ".nt");
			}
			boolean hasError = false;
			if (!outputFile.exists()) {
				FileOutputStream out = new FileOutputStream(outputFile);

				if (out != null) {
					TripleHandler handler = new NTriplesWriter(out);
					long start = System.currentTimeMillis();
					try {

						if (logger.isDebugEnabled())
							logger.debug("Starting extraction.");

						ExtractionReport report = any23.extract(source, handler);

						if (!report.hasMatchingExtractors()) {
							setCurrentError(file, "Any23 could not find any matching extractors to convert the file "
									+ file.getName() + " to nt format", null);
							hasError = true;
						}
					} catch (IOException e) {
						setCurrentError(file,
								"IOException while converting the file " + file.getName() + " to nt format using Any23",
								e);
						hasError = true;
					} catch (ExtractionException e) {
						setCurrentError(file, "ExtractionException while converting the file " + file.getName()
								+ " to nt format using Any23", e);
						hasError = true;
					} finally {
						try {
							handler.close();
						} catch (TripleHandlerException e) {
							logger.error("TripleHandlerException closing TripleHandler for file " + file.getName(), e);
						}
						out.close();

					}
					long end = System.currentTimeMillis();

					if (logger.isInfoEnabled())
						logger.info("Conversion took " + ((end - start) / 60000) + "mins");
					source = null;
					any23 = null;

				}
			}
			if (!hasError) {
				resource.createCurrentProcessedFile(file.getName());
				if (logger.isDebugEnabled())
					logger.debug("Converted file " + file.getName() + " to nt format.");
				this.resourceProcessor.processFile(resource, outputFile);
				// outputFile.delete();
			} else {
				outputFile.delete();
			}
		} else {
			setCurrentError(file, "File " + file.getName() + " is not RDF format and will not be processed.", null);
		}

	}

	@Override
	protected String getStrategyName() {
		return "NonNtFileStrategy";
	}

}
