/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump;

import java.io.File;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.commons.strings.ProcessedFileStrings;
import fr.inria.ilda.lodatlas.datamanager.dataset.resource.IResource;
import fr.inria.ilda.lodatlas.datamanager.dataset.resource.ProcessedFile;
import fr.inria.ilda.lodatlas.lodsummaries.D3JSONGeneratorFD;
import fr.inria.ilda.lodatlas.lodsummaries.D3JSONGeneratorHEB;

/**
 * An dump processing strategy to process N-triples files.
 * 
 * Note: This might not have been part of dump processing strategy if we did not
 * process and delete each file of the resource consecutively. We process each
 * file and delete it afterwards to save disk space in case of big dumps.
 * 
 * @author Hande Gözükan
 *
 */
public class NtFileStrategy extends AFileProcessStrategy {

	private static final Logger logger = LoggerFactory.getLogger(NtFileStrategy.class);

	/**
	 * Constructs a dump processor strategy to process N-triples files.
	 * 
	 * @param dumpProcessor
	 */
	public NtFileStrategy(ResourceProcessor dumpProcessor) {
		super(dumpProcessor);
	}

	@Override
	protected void process(IResource resource, File ntFile) {
		ProcessedFile processedFile = resource.createCurrentProcessedFile(ntFile.getName());

		// TODO the file name must end with nt

		long start = System.currentTimeMillis();
		String lodEntry = processFileWithLodstats(resource, ntFile, "nt");

		System.out.println(lodEntry);

		long end = System.currentTimeMillis();

		if (logger.isInfoEnabled())
			logger.info("Lodstats processing took " + ((end - start) / 60000) + "mins");

		if (lodEntry != null) {
			setLodStatEntries(processedFile, lodEntry);

			if (!processedFile.isHasError()) {

				if (logger.isDebugEnabled())
					logger.debug("No error returned from lodstats processing.");

				List<String> vocabularies = processedFile.getVocabularies();

				if (vocabularies.size() > 0) {
					if (logger.isDebugEnabled())
						logger.debug("Starting vocabulary file generation.");

					File vocabularyFile = resourceProcessor.downloadVocabulariesForFile(vocabularies, ntFile);
					// set class and property labels for the processed file
					resourceProcessor.setLabels(processedFile);
					// add class, property, vocabulary, class label, property label lists to resource
					resourceProcessor.updateResourceLists(resource, processedFile);

					if (logger.isDebugEnabled())
						logger.debug("Concatenated vocabulary generation finished. File name " + vocabularyFile);

					if (vocabularyFile != null) {

						// process for RDF sums
						String rdfSumEntry = processFileWithRdfSums(ntFile);

						String rdfFileName = setRDFSumEntries(processedFile, rdfSumEntry);

						// add values to current
						if (!processedFile.isHasError()) {

							if (logger.isDebugEnabled())
								logger.debug("RDFsum processing finished without errors.");

							File rdfsumFile = Paths.get(ntFile.getParentFile().getAbsolutePath(), rdfFileName).toFile();

							if (rdfsumFile.exists()) {

								if (logger.isInfoEnabled())
									logger.info("Generating FD");

								String fd = generateFD(rdfsumFile.getAbsolutePath());

								if (fd != null) {
									processedFile.setFd(Document.parse(fd));
									if (logger.isDebugEnabled())
										logger.debug("Added FD");
								} else {
									if (logger.isDebugEnabled())
										logger.debug("No FD generated.");
								}

								if (logger.isInfoEnabled())
									logger.info("Generating HEB");
								String heb = generateHEB(rdfsumFile.getAbsolutePath());
								if (heb != null) {
									processedFile.setHeb(Document.parse(heb));
									if (logger.isDebugEnabled())
										logger.debug("Added HEB");
								} else {
									if (logger.isDebugEnabled())
										logger.debug("No HEB generated.");
								}

								if (logger.isDebugEnabled())
									logger.debug("Finished lodsum. Fd is " + (fd == null ? "" : " not ")
											+ "null heb is " + (heb == null ? "" : " not ") + "null");

								fd = null;
								heb = null;
							} else {
								logger.error(
										"The RDF Sum file does not exist although there were no problems processing RDFSums.");
								processedFile.setHasError(true);
								processedFile.setError(
										"The RDF Sum file does not exist although there were no problems processing RDFSums.");
							}

						}
					} else {
						if (logger.isWarnEnabled())
							logger.warn("Merged vocabulary file could not be generated for file "
									+ ntFile.getAbsolutePath());
						processedFile.setHasError(true);
						processedFile.setError(
								"Merged vocabulary file could not be generated for file " + ntFile.getAbsolutePath());
					}

				} else {
					if (logger.isWarnEnabled())
						logger.warn("Lodstats could not extract any vocabularies for file " + ntFile.getAbsolutePath());
					processedFile.setHasError(true);
					processedFile.setError(
							"Lodstats could not extract any vocabularies for file " + ntFile.getAbsolutePath());
				}
			} else {
				if (logger.isWarnEnabled())
					logger.warn("Lodstats processing has returned error for file " + ntFile.getAbsolutePath());
				// we do not update current with error, it is already contained in current.
			}

		} else {
			if (logger.isWarnEnabled())
				logger.warn("Lodstats processing failed for file " + ntFile.getAbsolutePath());
			processedFile.setHasError(true);
			processedFile.setError("Lodstats processing failed for file due to unknown error.");
		}

		// persist the results of process to the database
		resourceProcessor.persistCurrent(resource);
		if (logger.isDebugEnabled())
			logger.debug("Wrote resource processing result to DB");
	}

	/**
	 * Sets the values returned from LodStats to processedFile.
	 * 
	 * <pre>
	 * Lodstats returns JSON in the following format when there is no error:
	 * 
	 *  entry = {
	 *    "fileName": fileName,
	 *    "tripleCount": tripleCount,
	 *    "hasError": False,
	 *    "error": ""
	 *    "usedClasses": classList,
	 *    "usedProperties": propertyUsageList,
	 *    "vocabularies" : vocabularyList
	 *  }
	 *    
	 * In case of error it returns:
	 *    
	 *  entry = {
	 *    "fileName": fileName,
	 *    "hasError": True,
	 *    "error" : repr(ex)
	 *  }
	 * </pre>
	 * 
	 * @param processedFile
	 * @param lodEntry
	 */
	private void setLodStatEntries(ProcessedFile processedFile, String lodEntry) {
		// This is necessary for parser to parse boolean values which come with capital
		// letters from python code.
		lodEntry = lodEntry.replaceAll("False", "false");
		lodEntry = lodEntry.replaceAll("True", "true");

		Document current = Document.parse(lodEntry);
		processedFile.setFileName(current.getString(ProcessedFileStrings.FILE_NAME));
		if (current.getBoolean(ProcessedFileStrings.HAS_ERROR)) {
			processedFile.setHasError(true);
			processedFile.setError(current.getString(ProcessedFileStrings.ERROR));
		} else {
			processedFile.setTripleCount(current.getInteger(ProcessedFileStrings.TRIPLE_COUNT));
			processedFile.setUsedClasses((List<String>) current.get(ProcessedFileStrings.USED_CLASSES));
			processedFile.setUsedProperties((List<String>) current.get(ProcessedFileStrings.USED_PROPERTIES));
			processedFile.setVocabularies((List<String>) current.get(ProcessedFileStrings.VOCABULARIES));
		}
	}

	private String setRDFSumEntries(ProcessedFile processedFile, String rdfSumEntry) {
		String fileName = null;
		Document rdfSumDoc = Document.parse(rdfSumEntry);
		if (rdfSumDoc.getBoolean(ProcessedFileStrings.HAS_ERROR)) {
			processedFile.setHasError(true);
			processedFile.setError(rdfSumDoc.getString(ProcessedFileStrings.ERROR));
			if (logger.isWarnEnabled())
				logger.warn("RDFSum processing has returned error for file " + processedFile.getFileName());
		} else {
			fileName = rdfSumDoc.getString(ProcessedFileStrings.FILE_NAME);
		}

		return fileName;
	}

	/**
	 * 
	 * @param resource
	 * @param resourceFile
	 */
	public String processFileWithLodstats(IResource resource, File resourceFile, String format) {

		// Note:
		// I tried this part with jython, but jython cannot load c extensions and
		// redland rdf api has c extensions
		// Using jython it is possible to get "entry" value from python script

		ProcessBuilder processBuilder = new ProcessBuilder("python", "./lodstats/lodstats_script.py",
				resourceFile.getAbsolutePath(), "-r", resource.getResourceId(), "-f", format, "-n",
				resourceFile.getName(), "-d");

		if (logger.isInfoEnabled()) {
			logger.info("Process command ------");
			logger.info(processBuilder.command().toString());
			logger.info("----------------------");
		}

		processBuilder.directory(new File(System.getProperty("user.dir")));
		processBuilder.redirectErrorStream(true);

		// This part can be used to direct the output from python code to
		// process_lodstats.log file. The output coming from python code is retrieved by
		// p.getInputStream() method. If directed to file, it returns null, if directed
		// to Redirect.PIPE, it can be read from file.
		// File log = new File("logs/process_lodstats.log");
		// processBuilder.redirectOutput(Redirect.appendTo(log));

		processBuilder.redirectOutput(Redirect.PIPE);

		Process p = null;
		String entry = null;

		try {
			p = processBuilder.start();

			entry = IOUtils.toString(p.getInputStream(), StandardCharsets.UTF_8.name());

			p.getInputStream().close();
			p.getOutputStream().close();
			p.getErrorStream().close();

			int result = p.waitFor();

			if (result == 1) {
				if (logger.isWarnEnabled())
					logger.warn("Lodstats execution could not be completed, the subprocess exited upon exception.");
			} else {
				if (logger.isDebugEnabled())
					logger.debug("Lodstats processing finished.");
			}
		} catch (IOException e) {
			logger.error("Received IOException while running python process.", e);
		} catch (InterruptedException e) {
			logger.error("InterruptedException while waiting lodstats process to finish", e);
		} finally {
			p.destroy();
		}

		return entry;
	}

	public String processFileWithRdfSums(File ntFile) {
		// process for RDF sums
		long start = System.currentTimeMillis();

		// Sejla's (CEDAR) rdf summary code is called as process because the jar file
		// from Sejla exits
		// in case of an exception which terminates CkanDataManager program too.
		// Sejla said that this is due to another library that she used in her code
		ProcessBuilder processBuilder = new ProcessBuilder("java", "-jar", "lodatlas-summarygenerator-prod.jar", ntFile.getAbsolutePath(), 
				ntFile.getParent());

		if (logger.isInfoEnabled()) {
			logger.info("Process command ------");
			logger.info(processBuilder.command().toString());
			logger.info("----------------------");
		}

		processBuilder.directory(new File(System.getProperty("user.dir")));
		File log = new File("logs/rdfsum.log");
		processBuilder.redirectErrorStream(true);
		processBuilder.redirectOutput(Redirect.appendTo(log));
		Process p = null;
		try {
			p = processBuilder.start();
			try {
				p.getInputStream().close();
				p.getOutputStream().close();
				p.getErrorStream().close();

				int result = p.waitFor();

				if (result == 1) {
					if (logger.isWarnEnabled())
						logger.warn("RDF summary generation encountered errors.");
					if (new File(ntFile.getName() + "_rdfsum.nt").exists()) {
						if (logger.isWarnEnabled())
							logger.warn("In spite of the error, summary file is generated.");
						return "{'fileName': '" + (ntFile.getName() + "_rdfsum.nt")
								+ "','hasError': false, 'error': ''}";
					} else {
						if (logger.isWarnEnabled())
							logger.warn("Summary generation could not be completed.");
					}
				} else {
					if (logger.isDebugEnabled())
						logger.debug("RDF summary generation finished without problems.");
					return "{'fileName': '" + (ntFile.getName() + "_rdfsum.nt") + "','hasError': false, 'error': ''}";
				}
			} catch (InterruptedException e) {
				logger.error("InterruptedException while processing RDF summary", e);
				return "{'fileName': '" + (ntFile.getName() + "_rdfsum.nt")
						+ "','hasError': true, 'error': 'InterruptedException while processing RDF summary'}";
			}

		} catch (IOException e) {
			logger.error("IOException while processing RDF summary", e);
			return "{'fileName': '" + (ntFile.getName() + "_rdfsum.nt")
					+ "','hasError': true, 'error': 'IOException while processing RDF summary'}";
		} finally {
			p.destroy();
		}

		long end = System.currentTimeMillis();

		if (logger.isInfoEnabled())
			logger.info("RDFsum processing took " + ((end - start) / 60000) + "mins");

		return "{'fileName': '" + (ntFile.getName() + "_rdfsum.nt")
				+ "','hasError': true, 'error': 'RDF summary generation finished with problems'}";

	}

	/**
	 * 
	 * @param rdfSumPath
	 * @return
	 */
	private String generateFD(String rdfSumPath) {
		D3JSONGeneratorFD fdGenerator = new D3JSONGeneratorFD();
		fdGenerator.loadRDFSummary(rdfSumPath);
		fdGenerator.buildViz();
		// System.out.println(fdGenerator.generateJSON().toString());
		return fdGenerator.generateJSON().toString();
	}

	/**
	 * 
	 * @param rdfSumPath
	 * @return
	 */
	private String generateHEB(String rdfSumPath) {
		D3JSONGeneratorHEB hebGenerator = new D3JSONGeneratorHEB();
		hebGenerator.reset();
		hebGenerator.loadRDFSummary(rdfSumPath);
		hebGenerator.buildViz();
		// System.out.println(hebGenerator.generateJSON().toString());

		return hebGenerator.generateJSON().toString();
	}

	@Override
	protected String getStrategyName() {
		return "NtFileStrategy";
	}
}
