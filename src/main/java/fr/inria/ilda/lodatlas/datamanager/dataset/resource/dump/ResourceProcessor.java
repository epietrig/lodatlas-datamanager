/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URLConnection;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;

import org.apache.commons.io.FileUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.commons.model.Label;
import fr.inria.ilda.lodatlas.commons.model.Vocabulary;
import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.commons.strings.ProcessedFileStrings;
import fr.inria.ilda.lodatlas.commons.strings.ResourceStrings;
import fr.inria.ilda.lodatlas.commons.strings.VocabularyStrings;
import fr.inria.ilda.lodatlas.datamanager.dataset.resource.IResource;
import fr.inria.ilda.lodatlas.datamanager.dataset.resource.ProcessedFile;
import fr.inria.ilda.lodatlas.datamanager.dataset.resource.ShortProcessedFile;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.LovConnector;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.MongoUtil;
import fr.inria.ilda.lodatlas.datamanager.util.HttpUtil;

/**
 * 
 * @author Hande Gözükan
 *
 */
public class ResourceProcessor implements IFileProcessStrategy {

	private static final Logger logger = LoggerFactory.getLogger(ResourceProcessor.class);

	private static final long COMPRESSED_MAX_SIZE = 3221225472l;

	private static final long UNCOMPRESSED_MAX_SIZE = 10737418240l;

	private static final String CONTENT_TYPE = "Content-Type";

	private static final String CONTENT_LENGTH = "Content-Length";

	private static final String CONTENT_DISPOSITION = "Content-Disposition";

	/**
	 * Strategy to be used to process files.
	 */
	private IFileProcessStrategy processStrategy;

	/**
	 * Mongo utility to interact with MOngoDB.
	 */
	private final MongoUtil mongoUtil;

	/**
	 * Directory to save vocabulary files.
	 */
	private final File vocabularyDir;

	/**
	 * Constructs a processor for dataset resources.
	 * 
	 * @param mongoUtil
	 *            mongo utility to access, query and manipulate MongoDB.
	 * @param tempProcessPath
	 *            path for the directory to temporarily process dataset resource
	 *            files.
	 */
	public ResourceProcessor(MongoUtil mongoUtil, Path tempProcessPath) {
		this.mongoUtil = mongoUtil;

		this.vocabularyDir = Paths.get(tempProcessPath.toString(), ProcessedFileStrings.VOCABULARIES).toFile();
		if (!this.vocabularyDir.exists()) {
			this.vocabularyDir.mkdir();
		}
	}

	/**
	 * Processes the specified <code>resource</code>. Creates resource directory for
	 * resource dump file under the specified <code>datasetPath</code>.
	 * 
	 * @param datasetPath
	 *            path for the directory of temporary processing for the dataset of
	 *            the specified <code>resource</code>.
	 * @param resource
	 *            resource to process.
	 */
	public void processResource(Path datasetPath, IResource resource) {
		try {
			// download resource dump, if it does not exist in local
			if (!resource.hasDump() || (resource.hasDump() && !resource.getDumpPath().toFile().exists())) {
				downloadDump(datasetPath, resource);
			}

			// if resource has its dump file in local, process it
			// it is assumed that if the file exists in local, basic values are persisted
			if (resource.hasDump()) {
				processFile(resource, resource.getDumpPath().toFile());
			}
			persistResource(resource);
		} catch (Exception e) {
			logger.error("Exception while processing resource " + resource.getResourceId(), e);
		}
	}

	/**
	 * Downloads the dump file of the specified dataset resource
	 * <code>resource</code> to a directory created with the ID of the
	 * <code>resource</code> under the specified <code>datasetPath</code>.
	 * 
	 * @param datasetPath
	 *            path of the directory for temporary processing for the dataset of
	 *            the <code>resource</code> specified.
	 * @param resource
	 *            resource for which the dump file is to be downloaded.
	 */
	public void downloadDump(Path datasetPath, IResource resource) {
		Path resourcePath = Paths.get(datasetPath.toString(), resource.getResourceId());

		if (resource.canDownloadDump()) {
			if (logger.isInfoEnabled()) {
				logger.info("Downloading dump from " + resource.getUrl().toString());
			}

			if (resource.getUrl().getProtocol().startsWith("http")) {
				if (logger.isInfoEnabled())
					logger.info("Downloading dump with HTTP protocol.");
				downloadHttp(resource, resourcePath);
			} else {
				if (logger.isInfoEnabled())
					logger.info("Downloading dump with other protocol.");
				downloadOther(resource, resourcePath);
			}
		} else if (resource.isLinkedData()) {
			if (logger.isWarnEnabled()) {
				logger.debug("Will not download dump for verified format " + resource.getVerifiedFormat());
			}
		} else {
			if (logger.isWarnEnabled()) {
				logger.debug("Not a linked data source and will not download dump. Verified format is "
						+ resource.getVerifiedFormat());
			}
		}
	}

	/**
	 * Downloads the dump file of the resource using HTTP client.
	 * 
	 * @param resource
	 *            resource for which the dump file is to be downloaded.
	 * @param resourcePath
	 *            the directory path to download the dump file of the resource.
	 */
	private void downloadHttp(IResource resource, Path resourcePath) {
		String urlString = resource.getUrl().toString();
		HttpGet httpGet = new HttpGet(urlString);
		httpGet.addHeader("Accept",
				"application/n-triples;q=0.9,text/turtle;q=0.8,application/rdf+xml;q=0.7,application/n-quads;q=0.6,text/n3;q=0.5,application/ld+json;q=0.4,application/trig;q=0.3,*/*;q=0.2");

		if (logger.isInfoEnabled()) {
			logger.info("Executing request " + httpGet.getRequestLine());
		}
		try {
			CloseableHttpClient httpClient = HttpClients.custom().setRetryHandler(HttpUtil.retryHandler).build();
			CloseableHttpResponse response = httpClient.execute(httpGet);

			int status = response.getStatusLine().getStatusCode();
			if (logger.isDebugEnabled()) {
				logger.debug("Status code is " + status);
			}
			if ((status >= 200) && (status < 400)) {

				Header header = response.getFirstHeader(CONTENT_TYPE);
				if (header != null) {
					String contentType = "" + header.getValue().trim();
					if (logger.isDebugEnabled())
						logger.debug("Got http response. Content type: " + contentType);
					resource.setContentType(contentType);
				}

				// check if the resource is linked data considering the content type set
				if (resource.isLinkedData()) {
					header = response.getFirstHeader(CONTENT_LENGTH);
					if (header != null) {
						resource.setContentLength(Long.parseLong(header.getValue()));
					}

					String dumpFileName = "";

					header = response.getFirstHeader(CONTENT_DISPOSITION);
					if (header != null) {
						dumpFileName = header.getValue().replaceFirst("(?i)^.*filename=\"([^\"]+)\".*$", "$1");
					} else {
						dumpFileName = urlString.substring(urlString.lastIndexOf("/") + 1, urlString.length());
					}
					if (logger.isDebugEnabled()) {
						logger.debug("File name is " + dumpFileName);
					}
					resource.setDumpFileName(dumpFileName);

					if (resource.canDownloadDump()) {
						if (resourcePath != null) {
							if ((resource.isCompressed() && resource.getContentLength() <= COMPRESSED_MAX_SIZE)
									|| (!resource.isCompressed()
											&& resource.getContentLength() <= UNCOMPRESSED_MAX_SIZE)) {
								File fileToDownload = new File(resourcePath.toFile(), dumpFileName);
								resource.setDumpFilePath(fileToDownload.toPath());

								if (!fileToDownload.exists()) {
									HttpEntity httpEntity = response.getEntity();

									if (httpEntity == null) {
										resource.setDownloadError("Cannot download dump: HttpEntity returned null.");
									} else {
										FileUtils.copyInputStreamToFile(httpEntity.getContent(), fileToDownload);
										if (logger.isDebugEnabled())
											logger.debug("Downloaded resource");
									}
								} else {
									if (logger.isInfoEnabled()) {
										logger.info("The file already exists, will not download.");
									}
								}
							} else {
								resource.setDownloadError("Cannot download dump: File is too large to process. "
										+ (resource.isCompressed() ? "Compressed size:" : "Size:")
										+ resource.getContentLength() / 1073741824L + "GB\n");
							}
						} else {
							resource.setDownloadError(
									"Cannot download dump, no location is specified to save the dump file.");
						}
					}
				}
			} else {
				resource.setDownloadError("Cannot download dump: Status code is " + status);
			}
			response.close();
			httpClient.close();
		} catch (ClientProtocolException e) {
			resource.setDownloadError("Cannot download dump: ClientProtocolException");
			logger.error("Cannot download dump: ClientProtocolException", e);
		} catch (ConnectTimeoutException e) {
			resource.setDownloadError("Cannot download dump: ConnectTimeoutException");
			logger.error("Cannot download dump: ConnectTimeoutException", e);
		} catch (SocketTimeoutException e) {
			resource.setDownloadError("Cannot download dump: SocketTimeoutException");
			logger.error("Cannot download dump: SocketTimeoutException", e);
		} catch (IOException e) {
			resource.setDownloadError("Cannot download dump: IOException");
			logger.error("Cannot download dump: IOException", e);
		}
	}

	private void downloadOther(IResource resource, Path resourcePath) {
		String urlString = resource.getUrl().toString();

		try {
			resource.setContentType(URLConnection.getFileNameMap().getContentTypeFor(urlString));
		} catch (Exception e) {
			if (logger.isWarnEnabled()) {
				logger.warn("Got Exception trying to get content type from URL", e);
			}
		}

		if (resource.isLinkedData()) {
			String dumpFileName = urlString.substring(urlString.lastIndexOf("/") + 1, urlString.length());
			if (logger.isDebugEnabled()) {
				logger.debug("File name is " + dumpFileName);
			}

			resource.setDumpFileName(dumpFileName);

			if (resource.canDownloadDump()) {
				if (resourcePath != null) {
					if ((resource.isCompressed() && resource.getContentLength() <= COMPRESSED_MAX_SIZE)
							|| (!resource.isCompressed() && resource.getContentLength() <= UNCOMPRESSED_MAX_SIZE)) {
						File fileToDownload = new File(resourcePath.toFile(), dumpFileName);
						resource.setDumpFilePath(fileToDownload.toPath());

						if (!fileToDownload.exists()) {
							try {
								FileUtils.copyInputStreamToFile(resource.getUrl().openStream(), fileToDownload);
								if (logger.isDebugEnabled())
									logger.debug("Downloaded resource");
							} catch (IOException e) {
								resource.setDownloadError("IOException while downloading file");
								logger.error("IOException while downloading file " + resource.getUrl());
							}
						} else {
							if (logger.isInfoEnabled()) {
								logger.info("The file already exists, will not download.");
							}
						}
					} else {
						resource.setDownloadError("Cannot download dump: File is too large to process. "
								+ (resource.isCompressed() ? "Compressed size:" : "Size:")
								+ resource.getContentLength() / 1073741824L + "GB\n");
					}

				} else {
					resource.setDownloadError("Cannot download dump, no location is specified to save the dump file.");
				}
			}
		}

	}

	/**
	 * Dump processing strategy must be set before calling this method using
	 * {@link ResourceProcessor#setResourceStrategy(IResource)} or
	 * {@link ResourceProcessor#setStrategyForFile(IResource, String)} methods.
	 */
	@Override
	public void processFile(IResource resource, File file) {
		// if the file is null, nothing to process.
		if (file == null) {
			if (logger.isWarnEnabled())
				logger.warn("the file is null, nothing to process.");
			return;
		}

		assert (resource != null);

		setStrategy(file);

		// a strategy must be set to any type of file specified.
		assert (this.processStrategy != null);
		this.processStrategy.processFile(resource, file);
	}

	/**
	 * Sets {@link IDumpProcessStrategy} using the file name extension for the file
	 * specified by <code>filePath</code>.
	 * 
	 * @param resourceDoc
	 *            TODO
	 * @param file
	 *            file to process.
	 */
	private void setStrategy(File file) {
		if (file.isHidden()) {
			this.processStrategy = new HiddenFileStrategy(this);
		} else if (file.getName().endsWith(".rar")) {
			this.processStrategy = new RarStrategy(this);
		} else if (file.getName().endsWith(".tar")) {
			this.processStrategy = new TarStrategy(this);
		} else if (file.getName().endsWith(".zip")) {
			this.processStrategy = new ZipStrategy(this);
		} else if (file.getName().endsWith(".gz") || file.getName().endsWith(".gzip")
				|| file.getName().endsWith(".tgz")) {
			this.processStrategy = new GzStrategy(this);
		} else if (file.getName().endsWith(".bz2") || file.getName().endsWith(".bzip2")) {
			this.processStrategy = new Bz2Strategy(this);
		} else if (file.getName().endsWith(".7z") || file.getName().endsWith(".7zip")) {
			this.processStrategy = new SevenZStrategy(this);
		} else if (file.getName().endsWith(".trig")) {
			this.processStrategy = new TrigStrategy(this);
		} else if (file.getName().endsWith(".nt")) {
			this.processStrategy = new NtFileStrategy(this);
		} else {
			this.processStrategy = new NonNtFileStrategy(this);
		}
	}

	/**
	 * 
	 * @param currentFile
	 * @param error
	 * @param e
	 */
	public void setCurrentError(IResource resource, File currentFile, String error, Exception e) {
		if (e != null)
			logger.error(error, e);
		else
			logger.error(error);

		// set the original file name as currentFile. If it was already set, this will
		// have no effect.
		ProcessedFile currentProcessedFile = resource.createCurrentProcessedFile(currentFile.getName());

		currentProcessedFile.setFileName(currentFile.getName());
		currentProcessedFile.setHasError(true);
		currentProcessedFile.setError(error);

		// persist the results of process to the database
		persistCurrent(resource);
	}

	/**
	 * Persists the fields added while processing the resource <code>resource</code>
	 * to MongoDB.
	 * 
	 * @param resource
	 *            resource to persist to MongoDB.
	 */
	public void persistResource(IResource resource) {
		String resourceId = resource.getResourceId();

		// update db with verified format
		updateResourcePropSet(resource.getDatasetName(), resourceId,
				DatasetStrings.RESOURCES + ".$." + ResourceStrings.VERIFIED_FORMAT,
				resource.getVerifiedFormat().toString());

		// update db with content type
		updateResourcePropSet(resource.getDatasetName(), resourceId,
				DatasetStrings.RESOURCES + ".$." + ResourceStrings.CONTENT_TYPE, resource.getContentType());

		// update db can_download field
		updateResourcePropSet(resource.getDatasetName(), resourceId,
				DatasetStrings.RESOURCES + ".$." + ResourceStrings.CAN_DOWNLOAD, resource.canDownloadDump());

		// update db can_download field
		updateResourcePropSet(resource.getDatasetName(), resourceId,
				DatasetStrings.RESOURCES + ".$." + ResourceStrings.IS_COMPRESSED, resource.isCompressed());

		// update db file_name field
		updateResourcePropSet(resource.getDatasetName(), resourceId,
				DatasetStrings.RESOURCES + ".$." + ResourceStrings.DUMP_FILENAME, resource.getDumpFileName());

		// update db download_error field
		updateResourcePropSet(resource.getDatasetName(), resourceId,
				DatasetStrings.RESOURCES + ".$." + ResourceStrings.DOWNLOAD_ERROR, resource.getDownloadError());

		final List<Document> processedFileDocs = new ArrayList<>();
		final Jsonb jsonB = JsonbBuilder.create();
		List<ShortProcessedFile> processedFiles = resource.getProcessedFiles();

		processedFiles.forEach(new Consumer<ShortProcessedFile>() {

			@Override
			public void accept(ShortProcessedFile arg0) {
				processedFileDocs.add(Document.parse(jsonB.toJson(arg0)));
			}
		});

		try {
			jsonB.close();
		} catch (Exception e) {
			logger.error("Exception while trying to close jsonb", e);
		}

		updateResourcePropSet(resource.getDatasetName(), resourceId,
				DatasetStrings.RESOURCES + ".$." + ResourceStrings.PROCESSED_FILES, processedFileDocs);

		updateResourcePropSet(resource.getDatasetName(), resourceId,
				DatasetStrings.RESOURCES + ".$." + ProcessedFileStrings.USED_CLASSES, resource.getClassSet());

		updateResourcePropSet(resource.getDatasetName(), resourceId,
				DatasetStrings.RESOURCES + ".$." + ProcessedFileStrings.USED_PROPERTIES, resource.getPropertySet());

		updateResourcePropSet(resource.getDatasetName(), resourceId,
				DatasetStrings.RESOURCES + ".$." + ProcessedFileStrings.VOCABULARIES, resource.getVocabularySet());

		updateResourcePropSet(resource.getDatasetName(), resourceId,
				DatasetStrings.RESOURCES + ".$." + ProcessedFileStrings.CLASS_LABELS, resource.getClassLabelSet());

		updateResourcePropSet(resource.getDatasetName(), resourceId,
				DatasetStrings.RESOURCES + ".$." + ProcessedFileStrings.PROPERTY_LABELS,
				resource.getPropertyLabelSet());

		updateResourcePropSet(resource.getDatasetName(), resourceId,
				DatasetStrings.RESOURCES + ".$." + ResourceStrings.PROCESSED, true);

	}

	/**
	 * Persists and clears the map that stores values for the current file that is
	 * being processed.
	 * 
	 * @return <code>true</code> if the values are persisted to database;
	 *         <code>false</code> otherwise.
	 */
	public void persistCurrent(IResource resource) {

		ProcessedFile currentProcessedFile = resource.getCurrentProcessedFile();

		if (currentProcessedFile != null) {
			mongoUtil.insertProcessedFile(currentProcessedFile);
			if (currentProcessedFile.getFd() != null || currentProcessedFile.getHeb() != null) {
				updateResourcePropSet(resource.getDatasetName(), resource.getResourceId(), "rdfsum",
						"RDF quotients available");
			}
		}

		resource.resetProcessedFile();
	}

	private void updateResourcePropSet(String datasetName, String resourceId, String prop, Object value) {
		mongoUtil.updateDataset(
				new Document(DatasetStrings.NAME, datasetName)
						.append(DatasetStrings.RESOURCES + "." + ResourceStrings.ID, resourceId),
				new Document("$set", new Document(prop, value)));
	}

	private void updateResourcePropPush(String datasetName, String resourceId, String prop, Object value) {
		mongoUtil.updateDataset(
				new Document(DatasetStrings.NAME, datasetName)
						.append(DatasetStrings.RESOURCES + "." + ResourceStrings.ID, resourceId),
				new Document("$push", new Document(prop, value)));
	}

	/**
	 * Downloads vocabularies for the current file being processed. Saves each
	 * vocabulary in the database and generates a unique vocabulary file
	 * concatenating them.
	 * 
	 * @param vocabularies
	 *            the list of vocabularies to download.
	 * @param file
	 *            the file whose vocabularies should be downloaded.
	 * @return path of the concatenated vocabulary file.
	 */
	public File downloadVocabulariesForFile(List<String> vocabularyList, File file) {
		File combinedVocabularyFile = null;

		List<Model> models = new LinkedList<Model>();

		for (String vocabulary : vocabularyList) {
			if (logger.isInfoEnabled()) {
				logger.info("");
				logger.info("");
				logger.info("Processing vocabulary " + vocabulary);
			}

			Vocabulary mongoVocab = mongoUtil.findVocabulary(vocabulary);
			OntModel model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
			if (mongoVocab == null) {

				try {
					if (logger.isDebugEnabled()) {
						logger.debug("Started downloading vocabulary " + vocabulary);
					}
					model.read(vocabulary);

					if (logger.isDebugEnabled()) {
						logger.debug("Finished reading");
					}
					
					logger.info("Getting lov info");
					Vocabulary lovVocabulary = LovConnector.getVocabulary(vocabulary);
					logger.info("Got LOV info");
					
					// model.listClasses()
					// class.listDeclaredProperties().toList()

					logger.info("Getting list of RDFS classes");
					StmtIterator iterator = model.listStatements(null, RDF.type, RDFS.Class.asResource());

					while (iterator.hasNext()) {
						org.apache.jena.rdf.model.Resource res = ((Statement) iterator.next()).getSubject();
						Statement label = res.getProperty(RDFS.label, "en");
						if (label != null) {
							lovVocabulary.addClass(new Label(res.getURI(), label.getString()));
						}

						label = res.getProperty(RDFS.label);
						if (label != null) {
							lovVocabulary.addClass(new Label(res.getURI(), label.getString()));
						}
					}

					logger.info("Getting list of properties");
					iterator = model.listStatements(null, RDF.type, RDF.Property.asResource());

					while (iterator.hasNext()) {
						org.apache.jena.rdf.model.Resource res = ((Statement) iterator.next()).getSubject();
						Statement label = res.getProperty(RDFS.label, "en");
						if (label != null) {
							lovVocabulary.addProperty(new Label(res.getURI(), label.getString()));
						}

						label = res.getProperty(RDFS.label);
						if (label != null) {
							lovVocabulary.addProperty(new Label(res.getURI(), label.getString()));
						}
					}

					mongoUtil.insertVocabulary(lovVocabulary);

					mongoVocab = mongoUtil.findVocabulary(vocabulary);
					String vocabularyId = mongoVocab.getId();

					if (logger.isDebugEnabled()) {
						logger.debug("Inserted vocabulary to db with id " + vocabularyId);
					}

					model.write(new FileOutputStream(new File(vocabularyDir, vocabularyId)), "RDF/XML");

					if (logger.isDebugEnabled()) {
						logger.debug(
								"Downloaded and saved vocabulary as nt " + vocabulary + " with id " + vocabularyId);
					}
					models.add(model);
				} catch (Exception e) {
					logger.error("Exception while downloding vocabulary " + vocabulary, e);

					mongoUtil.insertVocabulary(new Vocabulary(vocabulary, "", "", "", "", e.getMessage()));
				}

			} else if (!"".equals(mongoVocab.getError())) {
				if (logger.isWarnEnabled())
					logger.warn("Vocabulary with id " + mongoVocab.getId() + " and URI " + vocabulary
							+ "does not exist in db due to " + mongoVocab.getError());
			} else {
				if (logger.isInfoEnabled()) {
					logger.info("The vocabulary " + vocabulary + " is already downloaded");
					logger.info("Loading "
							+ Paths.get(vocabularyDir.toString(), new String[] { mongoVocab.getId() }).toString());
				}

				try {
//					logger.info("Starting");
					// model.read(Paths.get(vocabularyDir.toString(), new String[] {
					// mongoVocab.getId() }).toString(), "RDF/XML");
					model.read(vocabulary);
					logger.info("Read from url " + vocabulary);
//					logger.info("No problem");
				} catch (Exception ex) {
					logger.error("RiotException while reading vocabulary from file", ex);
					logger.info("Reading vocabulary from " + vocabulary);
					// model.read(vocabulary);
				}

				models.add(model);
				if (logger.isInfoEnabled()) {
					logger.info("Loaded vocabulary " + vocabulary + " to model");
				}
			}
		}

		if (models.size() > 0) {
			Model finalModel = ModelFactory.createDefaultModel();

			for (Model model : models) {
				finalModel.add(model);
			}
			try {
				if (!finalModel.isEmpty()) {
					combinedVocabularyFile = new File(file.getAbsolutePath() + "_vocab.nt");
					finalModel.write(new FileOutputStream(combinedVocabularyFile), "TTL");
					if (logger.isInfoEnabled()) {
						logger.info("Wrote concatenated vocabularies to " + combinedVocabularyFile.getAbsolutePath());
					}
				} else if (logger.isWarnEnabled()) {
					logger.warn("Vocabulary for " + file.getAbsolutePath() + " is empty.");
				}
			} catch (IOException e) {
				logger.error("Exception while saving concatenated vocabulary file for " + file.getAbsolutePath(), e);
				combinedVocabularyFile = null;
			}
			finalModel.close();
		}
		for (Model model : models) {
			model.close();
		}

		return combinedVocabularyFile;
	}

	/**
	 * Reads class and property labels for used classes and used properties for the
	 * specified <code>processedFile</code> from MongoDB and sets them to
	 * <code>processedFiles</code>.
	 * 
	 * @param processedFile
	 *            processed file for which class and property labels to be set.
	 */
	public void setLabels(ProcessedFile processedFile) {

		// set class labels
		List<String> classLabelList = mongoUtil.getLabelListFromVocabulary(VocabularyStrings.CLASSES,
				processedFile.getUsedClasses());

		processedFile.setClassLabels(classLabelList);

		// set property labels
		List<String> propertyLabelList = mongoUtil.getLabelListFromVocabulary(VocabularyStrings.PROPERTIES,
				processedFile.getUsedProperties());

		processedFile.setPropertyLabels(propertyLabelList);

	}

	public void updateResourceLists(IResource resource, ProcessedFile processedFile) {
		resource.addClasses(processedFile.getUsedClasses());
		resource.addProperties(processedFile.getUsedProperties());
		resource.addVocabularies(processedFile.getVocabularies());
		resource.addClassLabels(processedFile.getClassLabels());
		resource.addPropertyLabels(processedFile.getPropertyLabels());
	}

}
