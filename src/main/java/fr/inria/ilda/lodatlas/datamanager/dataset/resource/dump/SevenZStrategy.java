/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.datamanager.dataset.resource.IResource;

/**
 * A 7z file has extension "7z" or "7zip".
 * 
 * @author Hande Gözükan
 *
 */
public class SevenZStrategy extends AFileProcessStrategy {

	private static final Logger logger = LoggerFactory.getLogger(SevenZStrategy.class);

	public SevenZStrategy(ResourceProcessor dumpProcessor) {
		super(dumpProcessor);
	}

	@Override
	protected void process(IResource resource, File file) throws IOException, FileNotFoundException {
		SevenZFile compressedFile = new SevenZFile(file);
		SevenZArchiveEntry entry = compressedFile.getNextEntry();

		while (entry != null) {
			String entryName = entry.getName();
			File extractedFile = new File(file.getParentFile(), entry.getName());

			if (logger.isDebugEnabled()) {
				logger.debug("");
				logger.debug("7z entry is " + entryName);
			}

			if (entry.isDirectory()) {
				if (!extractedFile.exists()) {
					extractedFile.mkdir();
				}
				if (logger.isDebugEnabled())
					logger.debug("Skipping directory " + entryName);
			} else {
				long size = entry.getSize();

				if (size > 0) {
					FileOutputStream out = new FileOutputStream(extractedFile);
					final byte[] buf = new byte[1024];
					for (int len = 0; (len = compressedFile.read(buf)) > 0;) {
						out.write(buf, 0, len);
					}
					if (logger.isDebugEnabled())
						logger.debug("Written content to file");
					out.close();
				} else {
					if (logger.isWarnEnabled())
						logger.warn("Size for " + extractedFile.getName() + " is " + size + ". File is not processed.");
				}
				if (extractedFile != null) {
					resourceProcessor.processFile(resource, extractedFile);

					// delete the file after being processed.
					extractedFile.delete();
				}
			}

			entry = compressedFile.getNextEntry();
		}
		compressedFile.close();
	}

	@Override
	protected String getStrategyName() {
		return "SevenZStrategy";
	}

}
