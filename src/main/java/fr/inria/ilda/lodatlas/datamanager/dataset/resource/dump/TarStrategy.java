/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.datamanager.dataset.resource.IResource;

/**
 * A {@link IFileProcessStrategy} for .tar files.
 * 
 * Tar file contains files to be archived. It can be compressed using a
 * compressor. In that case, generally a format specific suffix is added.
 * 
 * @author Hande Gözükan
 *
 */
public class TarStrategy extends AFileProcessStrategy {

	private static final Logger logger = LoggerFactory.getLogger(TarStrategy.class);

	public TarStrategy(ResourceProcessor dumpProcessor) {
		super(dumpProcessor);
	}

	@Override
	protected void process(IResource resource, File file) throws IOException, FileNotFoundException {
		InputStream in = new FileInputStream(file);

		TarArchiveInputStream tarStream = new TarArchiveInputStream(in);
		TarArchiveEntry entry = tarStream.getNextTarEntry();

		while (entry != null) {

			String entryName = entry.getName();
			File extractedFile = new File(file.getParentFile(), entry.getName());

			if (logger.isDebugEnabled()) {
				logger.debug("Tar entry is " + entryName);
				logger.debug("File path " + extractedFile);
			}

			if (entry.isDirectory() || entryName.contains("/.")) {
				if (!extractedFile.exists()) {
					extractedFile.mkdir();
				}
				if (logger.isDebugEnabled())
					logger.debug("Skipping directory " + entryName);
			} else {
				File savedFile = saveStream(tarStream, extractedFile, entry.getSize());
				if (savedFile != null) {
					resourceProcessor.processFile(resource, savedFile);

					// delete the file after being processed.
					savedFile.delete();
				}
			}
			entry = tarStream.getNextTarEntry();
		}
		tarStream.close();
		in.close();
	}

	@Override
	protected String getStrategyName() {
		return "TarStrategy";
	}

}
