/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.jena.query.Dataset;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.tdb.TDBFactory;

import fr.inria.ilda.lodatlas.datamanager.dataset.resource.IResource;

/**
 * Any23 does not support trig format. TrigStrategy class is created to convert
 * trig format files using Jena API to a format accepted by Any23.
 * 
 * Trig format allows to contain many graphs. Jena reads trig format only to a
 * dataset and does not write a dataset with multiple graphs to N-triples
 * format.
 * 
 * So trig format files are first transformed to NQUADS by JENA, and then
 * processed by Any23 to convert to N-triples format.
 * 
 * @author Hande Gözükan
 *
 */
public class TrigStrategy extends AFileProcessStrategy {

	public TrigStrategy(ResourceProcessor dumpProcessor) {
		super(dumpProcessor);
	}

	@Override
	protected void process(IResource resource, File file) throws IOException, FileNotFoundException {
		int index = file.getAbsolutePath().lastIndexOf(".");
		File outputFile = null;
		if (index == -1) {
			outputFile = new File(file.getAbsolutePath() + ".nq");
		} else {
			outputFile = new File(file.getAbsolutePath().substring(0, index) + ".nq");
		}
		boolean hasError = false;

		if (!outputFile.exists()) {
			FileOutputStream out = new FileOutputStream(outputFile);
			if (out != null) {
				Dataset dataset = TDBFactory.createDataset();

				try {

					long start = System.currentTimeMillis();
					RDFDataMgr.read(dataset, file.getAbsolutePath());

					RDFDataMgr.write(out, dataset, Lang.NQUADS);

					long end = System.currentTimeMillis();
					if (logger.isInfoEnabled())
						logger.info("Conversion took " + ((end - start) / 60000) + "mins");

				} catch (Exception e) {
					hasError = true;
					setCurrentError(file, "Exception while converting trig to nquads.", e);
				} finally {
					dataset.close();
					out.close();
				}

			}
		}
		if (!hasError) {
			resource.createCurrentProcessedFile(file.getName());
			if (logger.isDebugEnabled())
				logger.debug("Converted file " + file.getName() + " to nq format.");
			this.resourceProcessor.processFile(resource, outputFile);
		}
		if (outputFile != null)
			outputFile.delete();
	}

	@Override
	protected String getStrategyName() {
		return "TrigStrategy";
	}

}
