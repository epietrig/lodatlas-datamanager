/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.datamanager.dataset.resource.IResource;

/**
 * A {@link IFileProcessStrategy} for .zip files.
 * 
 * A zip file can contain one or more files or directories that may have been
 * compressed.
 * 
 * @author Hande Gözükan
 *
 */
public class ZipStrategy extends AFileProcessStrategy {

	private static final Logger logger = LoggerFactory.getLogger(ZipStrategy.class);

	public ZipStrategy(ResourceProcessor dumpProcessor) {
		super(dumpProcessor);
	}

	protected void process(IResource resource, File file) throws IOException {
		ZipEntry entry;
		ZipFile zipFile = new ZipFile(file);
		Enumeration e = zipFile.entries();

		while (e.hasMoreElements()) {
			entry = (ZipEntry) e.nextElement();

			String entryName = entry.getName();
			File extractedFile = new File(file.getParentFile(), entry.getName());

			if (logger.isDebugEnabled()) {
				logger.debug("Zip entry: " + entryName);
				logger.debug("File path " + extractedFile);
			}

			// if directory or hidden file
			if (entry.isDirectory()) {
				if (!extractedFile.exists()) {
					extractedFile.mkdir();
				}
				if (logger.isDebugEnabled())
					logger.debug("Skipping directory " + entryName);
			} else {
				File savedFile = saveStream(zipFile.getInputStream(entry), extractedFile, entry.getSize());
				if (savedFile != null) {
					resourceProcessor.processFile(resource, savedFile);
					// delete the file after being processed.
					savedFile.delete();
				}
			}
		}
		zipFile.close();
	}

	@Override
	protected String getStrategyName() {
		return "ZipStrategy";
	}

}
