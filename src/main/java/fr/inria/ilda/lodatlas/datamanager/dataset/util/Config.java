package fr.inria.ilda.lodatlas.datamanager.dataset.util;

public class Config {
	
	public static final String MG_HOST = "mongoHost";

	public static final String MG_PORT = "mongoPort";

	public static final String MG_DB_NAME = "dbName";

	/**
	 * MongoDB collection name for datasets.
	 */
	public static final String DATASET_COLLECTION = "dataset";
	
	/**
	 * MongoDB collection name for resources.
	 */
	public static final String PROCESSED_FILE_COLLECTION = "processedFile";
	
	/**
	 * MongoDB collection name for vocabularies.
	 */
	public static final String VOCABULARY_COLLECTION = "vocabulary";

}
