/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.util;

import javax.json.JsonObject;

import fr.inria.ilda.lodatlas.commons.model.Vocabulary;
import fr.inria.ilda.lodatlas.datamanager.util.HttpUtil;
import fr.inria.ilda.lodatlas.datamanager.util.JsonChecker;

/**
 * A class to get vocabulary info from http://lov.okfn.org/.
 * 
 * @author Hande Gözükan
 *
 */
public class LovConnector {

	private static final String RDF_URL = "http://www.w3.org/1999/02/22-rdf-syntax-ns";

	private static final String RDF = "rdf";

	private static final String RDFS_URL = "http://www.w3.org/2000/01/rdf-schema";

	private static final String RDFS = "rdfs";

	public static final String LOV_SEARCH = "http://lov.okfn.org/dataset/lov/api/v2/vocabulary/info?vocab=";

	public static final String LOV_URI_PRE = "http://lov.okfn.org/dataset/lov/vocabs/";

	public static final String TITLES = "titles";

	public static final String VALUE = "value";

	public static final String PREFIX = "prefix";

	public static final String HOMEPAGE = "homepage";

	public static final String DESCRIPTIONS = "descriptions";

	/**
	 * Connects to http://lov.okfn.org/ and gets the vocabulary info for the
	 * specified vocabulary URI <code>uri</code>.
	 * 
	 * @param uri
	 *            URI of the vocabulary.
	 * @return
	 */
	public static Vocabulary getVocabulary(String uri) {
		String title = "-";
		String prefix = "";
		String homepage = "";
		String desc = "";

		String searchApi = "http://lov.okfn.org/dataset/lov/api/v2/vocabulary/info?vocab=";
		if (RDFS_URL.equals(uri)) {
			searchApi = searchApi + RDFS;
			prefix = RDFS;
		} else if (RDF_URL.equals(uri)) {
			searchApi = searchApi + RDF;
			prefix = RDF;
		} else {
			searchApi = searchApi + uri;
		}
		JsonObject jObj = HttpUtil.getJSONFromUrl(searchApi);

		if (jObj != null) {
			title = jObj.getJsonArray(TITLES).getJsonObject(0).getString(VALUE);
			prefix = JsonChecker.getStringValue(jObj, PREFIX);
			homepage = JsonChecker.getStringValue(jObj, HOMEPAGE);
			desc = jObj.getJsonArray(DESCRIPTIONS).getJsonObject(0).getString(VALUE);
		}

		return new Vocabulary(uri, title, prefix, homepage, desc, "");
	}

}
