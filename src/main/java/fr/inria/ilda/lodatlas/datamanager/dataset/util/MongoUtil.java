/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoCursorNotFoundException;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.UpdateResult;

import fr.inria.ilda.lodatlas.commons.model.Vocabulary;
import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.commons.strings.ProcessedFileStrings;
import fr.inria.ilda.lodatlas.commons.strings.VocabularyStrings;
import fr.inria.ilda.lodatlas.datamanager.dataset.Dataset;
import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;
import fr.inria.ilda.lodatlas.datamanager.dataset.resource.ProcessedFile;

/**
 * A utility class to connect to MongoDb and realize operations on dataset,
 * resource and vocabulary collections.
 * 
 * @author Hande Gözükan
 *
 */
public class MongoUtil {

	private static final Logger logger = LoggerFactory.getLogger(MongoUtil.class);

	private final MongoClient client;

	private final MongoDatabase mondoDB;

	private final MongoCollection<Document> datasetCollection;

	private final MongoCollection<Document> resourceCollection;

	private final MongoCollection<Document> vocabularyCollection;

	/**
	 * Constructs a MongoDB utility that connects to the database with specified
	 * <code>dbName</code> and lets the user realize basic database operations.
	 * 
	 * @param host
	 *            the host address that hosts the MongoDB.
	 * @param port
	 *            the port number to connect to MongoDB.
	 * @param dbName
	 *            name of the database to connect.
	 * @throws IllegalArgumentException
	 *             if either the specified <code>host</code> or the specified
	 *             <code>dbName</code> is <code>null</code> or empty string; or the
	 *             specified <code>port</code> is less than 0.
	 */
	public MongoUtil(String host, int port, String dbName) {
		if (host == null || "".equals(host)) {
			throw new IllegalArgumentException("Please specify a valid host.");
		}
		if (port < 0) {
			throw new IllegalArgumentException("Please specify a valid port.");
		}
		if (dbName == null || "".equals(dbName)) {
			throw new IllegalArgumentException("Please specify a valid database name.");
		}
		this.client = new MongoClient(host, port);
		this.mondoDB = this.client.getDatabase(dbName);

		this.datasetCollection = this.mondoDB.getCollection(Config.DATASET_COLLECTION);
		this.resourceCollection = this.mondoDB.getCollection(Config.PROCESSED_FILE_COLLECTION);
		this.vocabularyCollection = this.mondoDB.getCollection(Config.VOCABULARY_COLLECTION);
	}

	/**
	 * Finds and returns the document for the dataset specified with
	 * <code>datasetName</code> from MongoDB.
	 * 
	 * @param datasetName
	 *            name of the dataset.
	 * @return the document for the dataset specified.
	 * @throws IllegalArgumentException
	 *             if the specified <code>datasetName</code> is <code>null</code> or
	 *             empty string.
	 */
	public Document findDataset(String datasetName) {
		if (datasetName == null || "".equals(datasetName)) {
			throw new IllegalArgumentException("Dataset name cannot be null or empty string.");
		}

		try {
			return this.datasetCollection.find(new Document(DatasetStrings.NAME, datasetName)).first();
		} catch (Exception e) {
			logger.error("Encountered exception while finding dataset: " + datasetName, e);
		}
		return null;
	}

	/**
	 * Inserts the specified <code>dataset</code> document to MongoDB.
	 * 
	 * @param dataset
	 *            document for the dataset to insert to MongoDB.
	 * @throws IllegalArgumentException
	 *             if the specified <code>dataset</code> document is
	 *             <code>null</code>.
	 */
	public void insertDataset(Document dataset) {
		if (dataset == null) {
			throw new IllegalArgumentException("Cannot insert null dataset to the database.");
		}
		try {
			this.datasetCollection.insertOne(dataset);
			if (logger.isDebugEnabled())
				logger.debug("Inserted " + dataset.getString(DatasetStrings.NAME) + " to mongodb.");
		} catch (Exception e) {
			logger.error("Encountered exception while inserting dataset", e);
		}
	}

	/**
	 * Finds and updates the first document specified by <code>filter</code> with
	 * the specified <code>update</code> content.
	 * 
	 * @param filter
	 *            filter to select the document to update.
	 * @param update
	 *            update to apply to the selected document.
	 * @return the document that was updated before the update was applied. If no
	 *         document matches the query filter, then returns <code>null</code>.
	 * @throws IllegalArgumentException
	 *             if either the specified <code>filter</code> or
	 *             <code>update</code> is <code>null</code>.
	 */
	public Document updateDataset(Document filter, Document update) {
		if (filter == null || update == null) {
			throw new IllegalArgumentException("Filter and update cannot be null.");
		}
		try {
			return this.datasetCollection.findOneAndUpdate(filter, update);
		} catch (Exception e) {
			logger.error("Encountered exception while updating dataset", e);
		}
		return null;
	}

	/**
	 * Finds and replaces the document specified by <code>filter</code> with the
	 * document specified by <code>replacement</code>.
	 * 
	 * @param filter
	 *            filter to select the document to update.
	 * @param replacement
	 *            the document to replace the selected document.
	 * @return the result of the update.
	 * @throws IllegalArgumentException
	 *             if either the specified
	 */
	public UpdateResult replaceDataset(Document filter, Document replacement) {
		if (filter == null || replacement == null) {
			throw new IllegalArgumentException("Filter and replacement document cannot be null.");
		}
		try {
			return this.datasetCollection.replaceOne(filter, replacement);
		} catch (Exception e) {
			logger.error("Encountered exception while replacing dataset", e);
		}
		return null;
	}

	/**
	 * Returns the list of resources for the dataset specified with name
	 * <code>datasetName</code>.
	 * 
	 * If the specified <code>datasetName</code> is <code>null</code> or empty
	 * string; or no dataset exists with the specified <code>datasetName</code>,
	 * returns <code>null</code>.
	 * 
	 * 
	 * @param datasetName
	 *            the name of the dataset.
	 * @return list of documents for the resources of the dataset.
	 */
	public List<Document> getResourcesForDataset(String datasetName) {
		if (datasetName == null || "".equals(datasetName)) {
			throw new IllegalArgumentException("Dataset name cannot be null or empty string.");
		}

		Document datasetDoc = findDataset(datasetName);
		if (datasetDoc != null) {
			return (List<Document>) datasetDoc.get(DatasetStrings.RESOURCES);
		}
		return null;
	}

	/**
	 * 
	 * @param datasetName
	 * @param resourceId
	 * @return
	 */
	public Document getResourceForDataset(String datasetName, String resourceId) {
		if (datasetName == null || "".equals(datasetName)) {
			throw new IllegalArgumentException("Dataset name cannot be null or empty string.");
		}
		if (resourceId == null || "".equals(resourceId)) {
			throw new IllegalArgumentException("Resource ID cannot be null or empty string.");
		}

		AggregateIterable<Document> resourceResult = this.datasetCollection.aggregate(Arrays.asList(
				Aggregates.match(new Document(DatasetStrings.NAME, datasetName).append("resources.id", resourceId)),
				Aggregates.unwind("$resources"), Aggregates.project(new Document("_id", 0).append("resources", 1))));
		return resourceResult.first();
	}

	/**
	 * Gets the list of all dataset documents in the database and returns the names
	 * of the datasets as list.
	 * 
	 * @return list of dataset names in the database
	 */
	public List<IDataset> getWholeDatasetList(String jsonSource) {
		List<IDataset> datasetList = new LinkedList<>();

		// TODO change this only to fetch dataset names
		FindIterable<Document> result = datasetCollection.find()
				.projection(new Document(DatasetStrings.NAME, 1).append(DatasetStrings.REPO_NAME, 1))
				.noCursorTimeout(true);
		MongoCursor<Document> resultIterator = result.iterator();
		try {
			while (resultIterator.hasNext()) {
				Document next = resultIterator.next();
				datasetList.add(new Dataset(next.getString(DatasetStrings.NAME), jsonSource,
						next.getString(DatasetStrings.REPO_NAME)));
			}
		} catch (MongoCursorNotFoundException ex) {
			logger.error("Could not continue iterating datasets. MongoCursor timed out.");
		}
		return datasetList;
	}

	/**
	 * Finds and returns the vocabulary specified with <code>uri</code> from
	 * MongoDB.
	 * 
	 * @param uri
	 *            URI of the vocabulary.
	 * @return the vocabulary specified.
	 * @throws IllegalArgumentException
	 *             if the specified <code>uri</code> is <code>null</code> or empty
	 *             string.
	 */
	public Vocabulary findVocabulary(String uri) {
		if (uri == null || "".equals(uri)) {
			throw new IllegalArgumentException("Vocabulary URI cannot be null or empty string.");
		}

		try {
			Jsonb jsonb = JsonbBuilder.create();
			Document vocabulary = this.vocabularyCollection.find(new Document(VocabularyStrings.URI, uri)).first();
			if (vocabulary != null) {
				vocabulary.append("id", vocabulary.get("_id").toString());

				Vocabulary vocab = jsonb.fromJson(vocabulary.toJson(), Vocabulary.class);
				jsonb.close();
				return vocab;
			} else
				return null;
		} catch (Exception e) {
			logger.error("Encountered exception while finding vocabulary: " + uri, e);
		}
		return null;
	}

	/**
	 * Inserts the specified <code>vocabulary</code> to MongoDB.
	 * 
	 * @param vocabulary
	 *            vocabulary to insert to MongoDB.
	 * @throws IllegalArgumentException
	 *             if the specified <code>vocabulary</code> is <code>null</code>.
	 */
	public void insertVocabulary(Vocabulary vocabulary) {
		if (vocabulary == null) {
			throw new IllegalArgumentException("Cannot insert null vocabulary to the database.");
		}
		try {
			Jsonb jsonb = JsonbBuilder.create();
			String vocabJson = jsonb.toJson(vocabulary);
			this.vocabularyCollection.insertOne(Document.parse(vocabJson));
			if (logger.isDebugEnabled())
				logger.debug("Inserted " + vocabulary.getUri() + " to mongodb.");

			jsonb.close();
		} catch (Exception e) {
			logger.error("Encountered exception while inserting vocabulary", e);
		}
	}

	/**
	 * Returns the list of labels for the specified <code>type</code> of URIs in the
	 * vocabulary collection.<br>
	 * 
	 * Type should be one of {@link VocabularyStrings#PROPERTIES} or
	 * {@link VocabularyStrings#CLASSES}.
	 * 
	 * @param type
	 *            type whose labels are required.
	 * @param list
	 *            list of URIs to get the label for.
	 * @return Returns the list of labels for the specified <code>type</code> of
	 *         URIs in the vocabulary collection, if no label exists returns empty
	 *         list.
	 * @throws IllegalArgumentException
	 *             if
	 *             <ul>
	 *             <li>the specified <code>type</code> is <code>null</code> or empty
	 *             string</li>
	 *             <li>the specified <code>list</code> is <code>null</code></li>
	 *             </ul>
	 */
	public List<String> getLabelListFromVocabulary(String type, List<String> list) {
		if (type == null || "".equals(type)) {
			throw new IllegalArgumentException("Type cannot be null or empty string.");
		}
		if (list == null) {
			throw new IllegalArgumentException("List to get label for cannot be null.");
		}

		ArrayList<String> labelList = new ArrayList<String>();

		AggregateIterable<Document> iterable = this.vocabularyCollection.aggregate(Arrays.asList(
				Aggregates.unwind("$" + type), Aggregates.match(Filters.in(type + "." + VocabularyStrings.URI, list)),
				Aggregates.project(new Document(type + "." + VocabularyStrings.LABEL, 1).append("_id", 0))));

		iterable.forEach(new Block<Document>() {

			@Override
			public void apply(Document doc) {
				Document entry = (Document) doc.get(type);
				labelList.add(entry.getString(VocabularyStrings.LABEL));
			}
		});

		return labelList;
	}

	public List<Vocabulary> getVocabularyList() {
		List<Vocabulary> vocabularylist = new ArrayList<>();
		try {
			Jsonb jsonb = JsonbBuilder.create();
			FindIterable<Document> itr = this.vocabularyCollection.find().noCursorTimeout(true);
			Iterator<Document> vocabularyIterator = itr.iterator();
			while (vocabularyIterator.hasNext()) {
				Document vocabulary = vocabularyIterator.next();
				vocabulary.append("id", vocabulary.get("_id").toString());
				vocabularylist.add(jsonb.fromJson(vocabulary.toJson(), Vocabulary.class));
			}
			jsonb.close();
		} catch (Exception e) {
			logger.error("Encountered exception while getting list of vocabularies from MongoDB", e);
		}
		return vocabularylist;
	}

	public List<Document> getVocabularyListAsDoc() {
		List<Document> vocabularylist = new ArrayList<>();
		try {
			FindIterable<Document> itr = this.vocabularyCollection.find().noCursorTimeout(true);
			Iterator<Document> vocabularyIterator = itr.iterator();
			while (vocabularyIterator.hasNext()) {
				Document vocabulary = vocabularyIterator.next();
				vocabularylist.add(vocabulary);
			}
		} catch (Exception e) {
			logger.error("Encountered exception while getting list of vocabularies from MongoDB", e);
		}
		return vocabularylist;
	}

	/**
	 * 
	 * @param repoName
	 * @param datasetName
	 * @return
	 */
	public FindIterable<Document> getProcessedFiles(String repoName, String datasetName) {
		if (repoName == null || "".equals(repoName)) {
			throw new IllegalArgumentException("Repository name cannot be null or empty string.");
		}
		if (datasetName == null || "".equals(datasetName)) {
			throw new IllegalArgumentException("Dataset name cannot be null or empty string.");
		}

		return this.resourceCollection.find(new Document(ProcessedFileStrings.DATASET_NAME, datasetName)
				.append(ProcessedFileStrings.REPO_NAME, repoName)).noCursorTimeout(true);
	}

	/**
	 * Inserts the specified <code>processedFile</code> document to MongoDB.
	 * 
	 * @param processedFile
	 *            document for the processed file to insert to MongoDB.
	 * @throws IllegalArgumentException
	 *             if the specified <code>processedFile</code> document is
	 *             <code>null</code>.
	 */
	public void insertProcessedFile(Document processedFile) {
		if (processedFile == null) {
			throw new IllegalArgumentException("Cannot insert null processed file document to the database.");
		}
		try {
			this.resourceCollection.insertOne(processedFile);
			if (logger.isDebugEnabled())
				logger.debug("Inserted " + processedFile.getString(VocabularyStrings.URI) + " to mongodb.");
		} catch (Exception e) {
			logger.error("Encountered exception while inserting vocabulary", e);
		}
	}

	/**
	 * Inserts the specified <code>processedFile</code> to MongoDB.
	 * 
	 * @param processedFile
	 *            processed file to insert to MongoDB.
	 * @throws IllegalArgumentException
	 *             if the specified <code>processedFile</code> document is
	 *             <code>null</code>.
	 */
	public void insertProcessedFile(ProcessedFile processedFile) {
		if (processedFile == null) {
			throw new IllegalArgumentException("Cannot insert null processed file document to the database.");
		}
		try {
			Jsonb jsonb = JsonbBuilder.create();
			String processedFileJson = jsonb.toJson(processedFile);
			System.out.println(processedFileJson);
			this.resourceCollection.insertOne(Document.parse(processedFileJson));
			if (logger.isDebugEnabled())
				logger.debug("Inserted " + processedFile.getOriginalFileName() + " to mongodb.");

			jsonb.close();
		} catch (Exception e) {
			logger.error("Encountered exception while inserting processedFile", e);
		}
	}

	// TODO check if this method is necessary

	/**
	 * Finds and updates the document specified by <code>doc2Update</code> with the
	 * specified <code>update</code> content.
	 * 
	 * @param doc2Update
	 *            document to update.
	 * @param update
	 *            content of the update.
	 * @return the document that was updated before the update was applied. If no
	 *         document matches the query filter, then returns <code>null</code>.
	 */
	public Document updateProcessedFile(Document doc2Update, Document update) {
		try {
			return this.resourceCollection.findOneAndUpdate(doc2Update, update);
		} catch (Exception e) {
			// TODO update with relevant info below
			logger.error("Encountered exception while updating processed file.", e);
		}
		return null;
	}

	/**
	 * Closes MongoDb client connection and releases any other sources used.
	 */
	public void dispose() {
		client.close();
	}
}
