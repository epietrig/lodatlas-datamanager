/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.repository;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An abstract class that implements common {@link IRepoApiConnector} methods.
 * 
 * @see IRepoApiConnector
 * 
 * @author Hande Gözükan
 *
 */
public abstract class ARepoApiConnector implements IRepoApiConnector {

	private static final Logger logger = LoggerFactory.getLogger(CkanV3Connector.class);

	/**
	 * The number of datasets to include in the dataset list at once.
	 */
	protected static final int CHUNK_SIZE = 1000;

	/**
	 * Name of the repository. This is user defined.
	 */
	protected final String repoName;

	/**
	 * URL of the repository.
	 */
	protected final String repoUrl;

	/**
	 * The query to get relevant data list, if <code>null</code>, all datasets in
	 * the repository are downloaded. <br>
	 * Expected form: <br>
	 * q=xxx <br>
	 * to be added to the end of the repo URL.<br>
	 * ex. q=rdf<br>
	 * ex. q=tags:lod
	 * 
	 */
	protected final String query;

	/**
	 * The index to start getting dataset from the repository.
	 */
	protected final int startIndex;

	/**
	 * Total number of datasets to download from the repository.
	 */
	protected final int totalCount;

	/**
	 * Constructs a repository connector for the specified <code>repoUrl</code> with
	 * the specified <code>repoName</code>.
	 * 
	 * @param repoName
	 *            name of the repository to connect. This is user defined.
	 * @param repoUrl
	 *            URL of the repository to connect.
	 * @param query
	 *            the query to filter relevant dataset list, if <code>null</code> or
	 *            empty string, all datasets in the repository are downloaded. <br>
	 *            Expected form:<br>
	 *            q=xxx <br>
	 *            to be added to the end of the repo URL.<br>
	 *            ex. q=rdf<br>
	 *            ex. q=tags:lod
	 */
	public ARepoApiConnector(String repoName, String repoUrl, String query, int startIndex, int totalCount) {
		if (repoName == null || "".equals(repoName))
			throw new IllegalArgumentException("Repository name cannot be null or empty string.");
		else if (repoUrl == null || "".equals(repoUrl) || !isValidURL(repoUrl))
			throw new IllegalArgumentException("Repository URL must be a valid URL.");

		this.repoName = repoName;
		this.repoUrl = repoUrl.endsWith("/") ? repoUrl : repoUrl + "/";
		if (query == null) {
			this.query = "";
		} else {
			this.query = query.startsWith("/") ? query.replaceFirst("/", "") : query;
		}
		this.startIndex = startIndex;
		this.totalCount = totalCount;
	}

	@Override
	public String getJSONSource() {
		return repoName;
	}

	@Override
	public String getRepositoryUrl() {
		return repoUrl;
	}

	/**
	 * Tests whether the specified <code>url</code> is valid or not.
	 * 
	 * @param url
	 *            URL to be tested for validity.
	 * @return <code>true</code> if the specified <code>url</code> is valid;
	 *         <code>false</code> otherwise.
	 */
	public static boolean isValidURL(String url) {
		try {
			new URL(url).toURI();
			return true;
		} catch (MalformedURLException | URISyntaxException e) {
			logger.warn("The url " + url + " is malformed.");
			return false;
		}

	}

}
