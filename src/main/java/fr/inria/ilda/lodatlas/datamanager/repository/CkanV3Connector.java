/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.repository;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.datamanager.dataset.Dataset;
import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;
import fr.inria.ilda.lodatlas.datamanager.util.HttpUtil;
import fr.inria.ilda.lodatlas.datamanager.util.JsonChecker;

/**
 * A CKAN catalog connector that implements {@link IRepoApiConnector} to connect
 * to a CKAN catalog which uses API version 3 to retrieve dataset metadata.
 * 
 * @author Hande Gözükan
 *
 */
public class CkanV3Connector extends ARepoApiConnector implements IRepoApiConnector {

	private static final Logger logger = LoggerFactory.getLogger(CkanV3Connector.class);

	/**
	 * API string that specifies the API version.
	 */
	private static final String API = "api/3/action/";

	/**
	 * Search string to make search on datasets specifying a query.
	 */
	private static final String DATASET_SEARCH = "api/search/dataset?";

	private static final String DATASET = "dataset";
	private static final String PACKAGE_BY_ID = "package_show?id=";
	private static final String SUCCESS = "success";
	private static final String RESULT = "result";
	private static final String ERROR = "error";
	private static final String COUNT = "count";
	private static final String RESULTS = "results";

	private final String datasetListUrl;

	private int repoTotalCount;

	private int totalDatasetToDownload;

	private int startIndexTemp;

	/**
	 * Constructs a CKAN catalog connector which uses CKAN API version 3.
	 * 
	 * @param repoName
	 *            name of the repository to connect.
	 * @param repoUrl
	 *            URL of the repository to connect.
	 * @param query
	 *            the query to filter the relevant dataset list, if
	 *            <code>null</code> or empty string, all datasets in the repository
	 *            are downloaded. <br>
	 *            Expected form:<br>
	 *            q=xxx <br>
	 *            to be added to the end of the repo URL.<br>
	 *            ex. q=rdf<br>
	 *            ex. q=tags:lod
	 */
	public CkanV3Connector(String repoName, String repoUrl, String query, int startIndex, int totalCount) {
		super(repoName, repoUrl, query, startIndex, totalCount);
		this.datasetListUrl = getDatasetListUrl();
		getRepoInfo();
		reset();

	}

	@Override
	public void reset() {
		if (this.totalCount == -1) {
			this.totalDatasetToDownload = repoTotalCount;
		} else {
			this.totalDatasetToDownload = this.totalCount;
		}
		this.startIndexTemp = this.startIndex;
	}

	/**
	 * Builds the URL to get dataset query URL.
	 * 
	 * @return
	 */
	private String getDatasetListUrl() {
		// set the url to fetch dataset list
		String url = this.repoUrl + DATASET_SEARCH;

		// add query string
		if (!"".equals(this.query)) {
			url += this.query;
		}
		return url;
	}

	/**
	 * Sets the number of available datasets in the repo.
	 */
	private void getRepoInfo() {
		JsonObject jObj = HttpUtil.getJSONFromUrl(datasetListUrl);
		if (jObj != null && jObj.get(ERROR) == null) {
			this.repoTotalCount = jObj.getInt(COUNT);
			if (logger.isInfoEnabled()) {
				logger.info("Repository " + repoUrl + " has total of " + repoTotalCount + " datasets.");
			}
		} else {
			if (logger.isWarnEnabled()) {
				logger.warn("Problem while getting repo dataset list info from repository " + repoUrl);
			}
		}
	}

	@Override
	public boolean hasNextChunk() {
		if (this.startIndexTemp >= this.repoTotalCount || this.totalDatasetToDownload <= 0) {
			return false;
		}
		return true;
	}

	@Override
	public List<IDataset> getNextChunkList() {
		List<IDataset> datasetList = new LinkedList<>();

		int size = CHUNK_SIZE;

		if (this.totalDatasetToDownload <= size) {
			size = totalDatasetToDownload;
			totalDatasetToDownload = 0;
		} else {
			totalDatasetToDownload -= CHUNK_SIZE;
		}

		System.out.println("Getting datasets from " + startIndexTemp + " this much: " + size);
		String urlTemp = datasetListUrl + "&start=" + this.startIndexTemp + "&rows=" + size;
		if (logger.isInfoEnabled())
			logger.debug("Query URL is " + urlTemp);

		JsonObject jObj = HttpUtil.getJSONFromUrl(urlTemp);
		if (jObj != null) {

			JsonArray jResults = jObj.getJsonArray(RESULTS);

			for (JsonValue v : jResults) {
				datasetList.add(new Dataset(v.toString().replaceAll("\"", ""), getJSONSource(), getJSONSource()));
			}
		}
		this.startIndexTemp += size;
		return datasetList;
	}

	@Override
	public List<IDataset> getDatasetList() {
		List<IDataset> datasetList = new LinkedList<>();

		JsonObject jObj = HttpUtil.getJSONFromUrl(datasetListUrl);
//		System.out.println(datasetListUrl);

		if (jObj != null && jObj.get(ERROR) == null) {
			// get total number of datasets
			int totalCount = jObj.getInt(COUNT);

			// Fetch all datasets
			for (int i = 0; i <= totalCount; i += CHUNK_SIZE) {

				String urlTemp = datasetListUrl + "&start=" + i + "&rows=" + CHUNK_SIZE;
				if (logger.isInfoEnabled())
					logger.debug("Query URL is " + urlTemp);

				jObj = HttpUtil.getJSONFromUrl(urlTemp);

				JsonArray jResults = jObj.getJsonArray(RESULTS);

				for (JsonValue v : jResults) {
					datasetList.add(new Dataset(v.toString().replaceAll("\"", ""), getJSONSource(), getJSONSource()));
				}
			}
		} else {
			if (logger.isWarnEnabled()) {
				logger.warn("Problem while getting dataset list from repository " + repoUrl);
			}
		}
		return datasetList;
	}

	@Override
	public String getDatasetJson(String datasetName) {
		String datasetUrl = this.repoUrl + API + PACKAGE_BY_ID + datasetName;
		JsonObject jObj = HttpUtil.getJSONFromUrl(datasetUrl);

		// System.out.println(datasetUrl);

		// check if a JSON is downloaded
		if (jObj != null) {

			// check if JSON is downloaded successfully
			if (jObj.getBoolean(SUCCESS)) {
				jObj = processDatasetJson(jObj, datasetName, this.repoUrl + DATASET + "/" + datasetName);
				if (logger.isDebugEnabled())
					logger.debug("Processed dataset " + datasetName);
			} else {
				if (logger.isWarnEnabled())
					logger.warn("CKANClient returned error for dataset " + datasetName + " with error:  "
							+ jObj.getJsonObject("error").getString("message"));
				jObj = null;
			}
			if (logger.isDebugEnabled())
				logger.debug(jObj.toString());
		} else {
			if (logger.isWarnEnabled())
				logger.warn("CkanClient returned null for dataset " + datasetName);
			return null;
		}

		return jObj.toString();
	}

	/**
	 * Processes downloaded JSON to add new fields extracted from existing fields.
	 * The fields are <br>
	 * <ul>
	 * <li>package URL</li>
	 * <li>namespace</li>
	 * <li>triple count</li>
	 * <li>outgoing links</li>
	 * </ul>
	 * 
	 * 
	 * @param jsonObj
	 *            downloaded JSON.
	 * @return processed JSON with new fields.
	 */
	private JsonObject processDatasetJson(JsonObject jsonObj, String datasetName, String datasetUrl) {
		JsonObject jsonNew = jsonObj.getJsonObject(RESULT);

		// recreate a JSON object to modify, since JsonObject is immutable
		JsonObjectBuilder builder = Json.createObjectBuilder();
		for (Entry<String, JsonValue> entry : jsonNew.entrySet()) {
			builder.add(entry.getKey(), entry.getValue());
		}

		// add catalog URL
		builder.add(DatasetStrings.REPO_NAME, this.repoName);
		builder.add(DatasetStrings.REPO_DATASET_URL, datasetUrl);

		// add triple count and links (provided in extras)
		JsonArray extrasJson = jsonNew.getJsonArray(DatasetStrings.EXTRAS);
		JsonArrayBuilder linkBuilder = Json.createArrayBuilder();

		long tripleCount = 0;
		String nameSpace = "";
		for (int i = 0; i < extrasJson.size(); i++) {
			JsonObject extrasItem = extrasJson.getJsonObject(i);
			try {

				String key = JsonChecker.getStringValue(extrasItem, DatasetStrings.KEY);
				String value = JsonChecker.getStringValue(extrasItem, DatasetStrings.VALUE);

				if (DatasetStrings.TRIPLES.equals(key) && !value.equals("")) {
					value = value.replace(",", "").replace(".", "").replace(" ", "").replace("\"", "").replace(">", "");
					try {
						tripleCount = Long.parseLong(value);
					} catch (Exception e) {
						logger.error("Triple count for " + datasetName + " could not be saved. Triple count value is "
								+ value, e);
					}
				} else if (DatasetStrings.NAMESPACE.equals(key)) {
					nameSpace = value;
				} else if (key != null && key.startsWith(DatasetStrings.LINKS + ":")) {
					String linkName = key.split(":")[1];
					value = value.replace(",", "").replace(".", "").replace(" ", "").replace("\"", "").replace(">", "");
					long linkCount = 0;
					if (!value.equals("")) {
						try {
							linkCount = Long.parseLong(value);
						} catch (Exception e) {
							logger.error("Link count for " + datasetName + " for link " + linkName
									+ " could not be saved. Link count value is " + value, e);
						}
					}

					linkBuilder.add(Json.createObjectBuilder().add(DatasetStrings.NAME, linkName)
							.add(DatasetStrings.COUNT, linkCount));
				} else {
					if (logger.isWarnEnabled())
						logger.warn(
								"extras key: " + key + " is not processed for " + datasetName + " value is " + value);
				}
			} catch (Exception e) {
				if (logger.isWarnEnabled())
					logger.warn("Problem with extras item " + extrasJson.toString());
			}
		}

		builder.remove(DatasetStrings.EXTRAS);
		builder.add(DatasetStrings.TRIPLE_COUNT, tripleCount);
		builder.add(DatasetStrings.NAMESPACE, nameSpace);

		JsonArray linkArray = linkBuilder.build();
		builder.add(DatasetStrings.OUTGOING_LINKS, Json.createObjectBuilder()
				.add(DatasetStrings.COUNT, linkArray.size()).add(DatasetStrings.LINKS, linkArray));
		builder.add(DatasetStrings.INCOMING_LINKS, Json.createObjectBuilder().add(DatasetStrings.COUNT, 0)
				.add(DatasetStrings.LINKS, Json.createArrayBuilder().build()));

		return builder.build();
	}

}
