/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.repository;

import java.util.List;

import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;

/**
 * Interface for a repository connector which provides metadata on datasets.
 * 
 * It is expected from a metadata repository to provide a list of datasets, and
 * means to provide some expected metadata for the datasets.
 * 
 * Expected metadata is as follows:
 * 
 * *
 * 
 * <pre>
	 * { 
	 * 		"repoName": name of the repository that contains the dataset,
	 * 		"name": name of the dataset,
	 *      "url": URL of the dataset specified by the owner of the dataset,
	 * 		"repoDatasetUrl": URL of the dataset in the repository,
	 * 		"notes": description of the dataset,
	 *		"title": title of the dataset,
	 *		"author": author of the dataset,
	 *		"author_email": email of the author of the dataset,
	 *      "maintainer": full name of the maintainer of the dataset,
	 *      "maintainer_email": email of the maintainer of the dataset,
	 *      "namespace": the namespace of the dataset,
	 * 		"tripleCount": the total triple count of the dataset,
	 *      "metadata_created": creation date of dataset metadata,
	 *      "metadata_modified": last modification date of dataset metadata,
	 * 		"license_url": URL for the license of the dataset,
	 *		"license_title": title of the license of the dataset,
	 *      "version": version of the dataset,
	 *      "state": state of the dataset,
	 *		"organization": {
	 *		    "title": title of the organization that publishes this dataset,
	 *		},
	 *      "tags": [
	 *      			{
	 *			    		"display_name": display name for the dataset tag,
	 *			    		"name": name of the dataset tag
	 *					}
	 *				]
	 *		},
	 *	    "resources": {
	 *			    "id": id of the resource,
	 *			    "name": name of the resource,
	 *			    "description": description of the resource,
	 *			    "format": RDF format of the resource,
	 *			    "url": URL of the resource dump file,
	 *			    "state": state of the resource,
	 *			    "last_modified": last modification date of the resource
	 *		}
	 *		"outgoinglinks": {
	 *				"count": number of total outgoing links,
	 *			    "links": [
	 *			    		{
	 *						    "name": name of the linked dataset,
	 *						    "count": number of the links to that dataset
	 *						}
	 *				]
	 *		}
	 *	    "incominglinks": {
	 *       		"count": number of total incoming links,
	 *			    "links": [
	 *			    		{
	 *						    "name": name of the linking dataset,
	 *						    "count": number of the links from that dataset
	 *						}
	 *				]
	 *		}
 * </pre>
 * 
 * 
 * @author Hande Gözükan
 *
 */
public interface IRepoApiConnector {

	/**
	 * Getter method for the name of the repository.
	 * 
	 * @return name of the repository.
	 */
	public String getJSONSource();

	/**
	 * Getter method for the URL of the repository.
	 * 
	 * @return URL of the repository.
	 */
	public String getRepositoryUrl();

	/**
	 * 
	 */
	public void reset();

	/**
	 * Checks if there are still datasets that are not iterated.
	 * 
	 * @return <code>true</code> if there are still datasets that are not processed;
	 *         <code>false</code> otherwise.
	 */
	public boolean hasNextChunk();

	/**
	 * Gets the next chunk of of datasets that are not yet iterated.
	 * 
	 * @return the next chunk of of datasets that are not yet iterated.
	 */
	public List<IDataset> getNextChunkList();

	/**
	 * Getter method for the list of datasets in this repository.
	 * 
	 * @return the list of datasets provided by this repository as {@link IDataset},
	 *         containing dataset name and repository name, without JSON
	 *         <code>null</code> if no dataset could be fetched from the repository.
	 */
	public List<IDataset> getDatasetList();

	/**
	 * Getter method for the metadata of the dataset specified by
	 * <code>datasetName</code> as JSON String.
	 * 
	 * 
	 * @param datasetName
	 *            the name of the dataset whose metadata is queried.
	 * @return the metadata of the dataset as JSON String.
	 * 
	 *         The expected JSON is expected to have the following fields:
	 * 
	 *         *
	 * 
	 *         <pre>
	 * { 
	 * 		"repoName": name of the repository that contains the dataset,
	 * 		"name": name of the dataset,
	 *      "url": URL of the dataset specified by the owner of the dataset,
	 * 		"repoDatasetUrl": URL of the dataset in the repository,
	 * 		"notes": description of the dataset,
	 *		"title": title of the dataset,
	 *		"author": author of the dataset,
	 *		"author_email": email of the author of the dataset,
	 *      "maintainer": full name of the maintainer of the dataset,
	 *      "maintainer_email": email of the maintainer of the dataset,
	 *      "namespace": the namespace of the dataset,
	 * 		"tripleCount": the total triple count of the dataset,
	 *      "metadata_created": creation date of dataset metadata,
	 *      "metadata_modified": last modification date of dataset metadata,
	 * 		"license_url": URL for the license of the dataset,
	 *		"license_title": title of the license of the dataset,
	 *      "version": version of the dataset,
	 *      "state": state of the dataset,
	 *		"organization": {
	 *		    "title": title of the organization that publishes this dataset,
	 *		},
	 *      "tags": [
	 *      			{
	 *			    		"display_name": display name for the dataset tag,
	 *			    		"name": name of the dataset tag
	 *					}
	 *				]
	 *		},
	 *	    "resources": {
	 *			    "id": id of the resource,
	 *			    "name": name of the resource,
	 *			    "description": description of the resource,
	 *			    "format": RDF format of the resource,
	 *			    "url": URL of the resource dump file,
	 *			    "state": state of the resource,
	 *			    "last_modified": last modification date of the resource
	 *		}
	 *		"outgoinglinks": {
	 *				"count": number of total outgoing links,
	 *			    "links": [
	 *			    		{
	 *						    "name": name of the linked dataset,
	 *						    "count": number of the links to that dataset
	 *						}
	 *				]
	 *		}
	 *	    "incominglinks": {
	 *       		"count": number of total incoming links,
	 *			    "links": [
	 *			    		{
	 *						    "name": name of the linking dataset,
	 *						    "count": number of the links from that dataset
	 *						}
	 *				]
	 *		}
	 *         </pre>
	 * 
	 * 
	 */
	public String getDatasetJson(String datasetName);

}
