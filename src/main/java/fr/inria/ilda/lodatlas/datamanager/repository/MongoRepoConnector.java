/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.repository;

import java.util.List;

import org.bson.Document;

import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.MongoUtil;

/**
 * A class to use MongoDatabase as a repository to retrieve dataset list and
 * dataset JSON.
 * 
 * @author Hande Gözükan
 *
 */
public class MongoRepoConnector implements IRepoApiConnector {

	private final MongoUtil mongoUtil;

	/**
	 * The repository source for MongoDB repository connector.
	 */
	public static final String JSON_SOURCE = "mongo";

	private boolean hasReturnedList = false;

	/**
	 * Constructs a repository connector for MongoDB.
	 * 
	 * @param mongoUtil
	 *            instance of {@link MongoUtil} class to connect and retrieve data
	 *            from MongoDB.
	 */
	public MongoRepoConnector(MongoUtil mongoUtil) {
		assert (mongoUtil != null);
		this.mongoUtil = mongoUtil;

	}

	@Override
	public String getJSONSource() {
		return JSON_SOURCE;
	}

	@Override
	public String getRepositoryUrl() {
		return null;
	}

	@Override
	public List<IDataset> getDatasetList() {
		return mongoUtil.getWholeDatasetList(getJSONSource());
	}

	@Override
	public String getDatasetJson(String datasetName) {
		Document datasetDoc = mongoUtil.findDataset(datasetName);

		if (datasetDoc != null) {
			return datasetDoc.toJson();
		} else {
			return null;
		}
	}

	@Override
	public boolean hasNextChunk() {
		return !hasReturnedList;
	}

	@Override
	public List<IDataset> getNextChunkList() {
		this.hasReturnedList = true;
		return this.mongoUtil.getWholeDatasetList(getJSONSource());
	}

	@Override
	public void reset() {
		hasReturnedList = false;
	}

}