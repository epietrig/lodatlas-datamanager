/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.Reader;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;

import javax.json.Json;
import javax.json.JsonObject;
import javax.net.ssl.SSLException;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Hande Gözükan
 *
 */
public class HttpUtil {

	private final static Logger logger = LoggerFactory.getLogger(HttpUtil.class);

	/**
	 * Time to wait for socket timeout or connection timeout.
	 */
	public static final int TIMEOUT = 10000;

	private static final int MAX_REDIRECT = 10;

	private static final int MAX_RETRY = 5;

	/**
	 * Request retry handler for httpClient instance.
	 * 
	 * Code copied from Apache HttpClient tutorial.
	 * https://hc.apache.org/httpcomponents-client-ga/tutorial/html/fundamentals.html#d5e279
	 */
	public static final HttpRequestRetryHandler retryHandler = new HttpRequestRetryHandler() {

		public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
			if (executionCount > MAX_RETRY) {
				if (logger.isWarnEnabled())
					logger.warn("Will not retry for the 6th time ");
				return false;
			}
			if (exception instanceof SocketTimeoutException || exception instanceof InterruptedIOException
					|| exception instanceof ConnectTimeoutException || exception instanceof IOException) {
				if (logger.isWarnEnabled())
					logger.warn(exception.getClass().getSimpleName() + ", retrying...", exception);
				try {
					Thread.currentThread();
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					if (logger.isWarnEnabled())
						logger.warn("InterruptedException while waiting to retry, retrying...", exception);
				}
				return true;
			}
			if (exception instanceof UnknownHostException) {
				// Unknown host
				return false;
			}
			if (exception instanceof SSLException) {
				// SSL handshake
				return false;
			}
			HttpClientContext clientContext = HttpClientContext.adapt(context);
			HttpRequest request = clientContext.getRequest();
			boolean idempotent = !(request instanceof HttpEntityEnclosingRequest);
			if (idempotent) {
				// Retry if the request is considered idempotent
				try {
					Thread.currentThread();
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					if (logger.isWarnEnabled())
						logger.warn("InterruptedException while waiting to retry, retrying...", e);
				}
				return true;
			}
			return false;
		}

	};

	/**
	 * Request configuration.
	 */
	private static final RequestConfig requestConfig = RequestConfig.custom().setRedirectsEnabled(true)
			.setMaxRedirects(MAX_REDIRECT).setSocketTimeout(TIMEOUT).setConnectTimeout(TIMEOUT).build();

	/**
	 * The handler that handles HTTPResponse. It reads the stream and converts it to
	 * JSON.
	 */
	private static final ResponseHandler<JsonObject> responseHandler = new ResponseHandler<JsonObject>() {

		@Override
		public JsonObject handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {

			int status = response.getStatusLine().getStatusCode();

			if (status >= 200 && status < 400) {

				HttpEntity httpEntity = response.getEntity();

				if (httpEntity == null) {
					throw new ClientProtocolException("Response contains no content.");
				} else {
					ContentType contentType = ContentType.getOrDefault(httpEntity);
					Charset charset = contentType.getCharset();
					Reader reader = new InputStreamReader(httpEntity.getContent(), charset);
					JsonObject jObj = Json.createReader(reader).readObject();
					reader.close();

					return jObj;
				}
			} else {
				throw new ClientProtocolException("Response status " + status);
			}
		}
	};

	/**
	 * Retrieves the JSON provided by <code>url</code>.
	 * 
	 * @param url
	 *            url which provides JSON
	 * @return JSON string provided by the specified <code>url</code> as
	 *         {@link JsonObject}.
	 */
	public static JsonObject getJSONFromUrl(String url) {
		CloseableHttpClient httpClient = HttpClients.custom().setRetryHandler(retryHandler)
				.setRedirectStrategy(new LaxRedirectStrategy()).build();
		HttpGet httpGet = new HttpGet(url);
		httpGet.setConfig(requestConfig);
		if (logger.isDebugEnabled())
			logger.debug("Executing request " + httpGet.getRequestLine());

		JsonObject result = null;
		try {
			try {
				result = httpClient.execute(httpGet, responseHandler);
			} catch (ClientProtocolException e) {
				logger.error("ClientProtocolException while executing HttGet for URL " + url, e);
			} catch (IOException e) {
				logger.error("IOException while executing HttGet");
			} finally {
				httpClient.close();
			}
		} catch (IOException e) {
			logger.error("IOException while closing httpClient");
		}

		return result;
	}

}
