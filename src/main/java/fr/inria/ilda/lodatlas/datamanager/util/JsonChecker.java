/*******************************************************************************
 * LODAtlas Commons
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.util;

import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;

public class JsonChecker {

	/**
	 * Checks if the value for the specified <code>key</code> in the specified
	 * <code>json</code> is {@link ValueType#NULL}. Sets the value to
	 * <code>null</code>, if that is the case, gets the value as String otherwise.
	 * 
	 * @param json
	 *            the {@link JsonObject} whose value for the specified
	 *            <code>key</code> is required.
	 * @param key
	 *            key part of the JSON key-value pair
	 * 
	 * @return the value of the specified <code>key</code> as String ,if it is not
	 *         {@link ValueType#NULL}; <code>null</code> otherwise.
	 */
	public static String getStringValue(JsonObject json, String key) {
		String returnValue = null;
		JsonValue jsonVal = json.get(key);

		if (jsonVal != null) {
			returnValue = jsonVal.getValueType() == ValueType.NULL ? null : json.getString(key);
		}
		return returnValue;
	}
}
