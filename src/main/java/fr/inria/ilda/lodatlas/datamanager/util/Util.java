/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.util;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class which contains some utility methods.
 * 
 * @author Hande Gözükan
 *
 */
public class Util {

	private static final Logger logger = LoggerFactory.getLogger(Util.class);

	/**
	 * Loads the 2 column .csv file as map.
	 * 
	 * @param filePath
	 *            path for the CSV file.
	 * @param separator
	 *            separator between values.
	 * @return the map.
	 */
	public static Map<String, String> loadCsvAsMap(String filePath, String separator) {
		Map<String, String> map = new HashMap<>();

		Path path = Paths.get(filePath);
		List<String> lines = null;
		try {
			lines = Files.readAllLines(path);
		} catch (NoSuchFileException e) {
			logger.error("There is no such file '" + filePath + "'");
			System.exit(1);
		} catch (IOException e) {
			logger.error("An error occured while reading from file " + path.toAbsolutePath(), e);
			System.exit(1);
		}

		if (lines != null) {
			for (int i = 0; i < lines.size(); i++) {
				String[] line = lines.get(i).split(separator);
				map.put(line[0], line[1]);
			}
		}

		return map;
	}

	/**
	 * Load all lines of a file to a list.
	 * 
	 * @param filePath
	 *            path for the file to load
	 * @return the list of lines in the file.
	 */
	public static List<String> loadFromFile(String filePath) {
		Path path = Paths.get(filePath);
		List<String> lines = null;
		try {
			lines = Files.readAllLines(path);
		} catch (NoSuchFileException e) {
			logger.error("There is no such file '" + filePath + "'");
		} catch (IOException e) {
			logger.error("An error occured while reading from file " + path.toAbsolutePath(), e);
		}

		return lines;
	}
	
	/**
	 * Load properties from the specified file.
	 * 
	 * @param filePath
	 *            the path of the .properties file.
	 * @return {@link Properties} instance with the values loaded in the .properties
	 *         file with specified <code>filePath</code>.
	 */
	public static Properties loadProperties(String filePath) {
		assert (filePath != null && !"".equals(filePath));

		Properties prop = new Properties();

		InputStream in = null;

		try {
			in = Files.newInputStream(Paths.get(filePath));
		} catch (IOException e) {
			logger.error("Got IOException while loading .properties file from " + filePath, e);
			System.exit(1);
		}

		try {
			prop.load(in);
		} catch (IOException e) {
			logger.error("IOException while loading properties to instance from stream. ", e);
			System.exit(1);
		}

		if (logger.isInfoEnabled())
			logger.info("Loaded properties from file.");
		return prop;
	}


}
