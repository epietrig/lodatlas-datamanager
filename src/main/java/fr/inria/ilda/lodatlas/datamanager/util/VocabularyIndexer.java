package fr.inria.ilda.lodatlas.datamanager.util;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Properties;
import java.util.function.Consumer;

import org.bson.Document;
import org.elasticsearch.action.index.IndexResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.commons.es.EsStrings;
import fr.inria.ilda.lodatlas.commons.es.EsUtil;
import fr.inria.ilda.lodatlas.commons.es.exceptions.MappingFileException;
import fr.inria.ilda.lodatlas.datamanager.ProcessManager;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.MongoUtil;

public class VocabularyIndexer {

	private static final Logger logger = LoggerFactory.getLogger(VocabularyIndexer.class);

	private final static Properties prop = ProcessManager.prop;

	private final EsUtil esUtil;

	private final MongoUtil mongoUtil;

	private String indexName;

	private String mappingType;

	private boolean canIndex = false;

	public VocabularyIndexer(EsUtil esUtil, MongoUtil mongoUtil) {
		assert (esUtil != null);
		assert (mongoUtil != null);
		this.esUtil = esUtil;
		this.mongoUtil = mongoUtil;

		this.indexName = prop.getProperty(EsStrings.VOCABULARY_INDEX);
		this.mappingType = prop.getProperty(EsStrings.VOCABULARY_TYPE);

		try {
			canIndex = this.esUtil.createIndex(indexName, mappingType, prop.getProperty(EsStrings.VOCABULARY_MAPPING),
					prop.getProperty(EsStrings.ANALYZERS_FILE), true);
		} catch (FileNotFoundException e) {
			logger.error("The specified file does not exist on classpath.", e);
		} catch (MappingFileException e) {
			logger.error("Specified mapping file has problems.", e);
		}
	}

	public void indexAll() {
		if (logger.isInfoEnabled())
			logger.info("\tIndexing vocabularies");

		if (!canIndex) {
			if (logger.isWarnEnabled()) {
				logger.warn("Index is not created, cannot index.");
				return;
			}
		}

		List<Document> vocabularyList = mongoUtil.getVocabularyListAsDoc();

		vocabularyList.forEach(new Consumer<Document>() {

			@Override
			public void accept(Document vocabulary) {
				vocabulary.remove("_id");
				try {
					IndexResponse response = esUtil.indexDocument(indexName, mappingType, null, vocabulary.toJson());

					if (logger.isDebugEnabled())
						logger.debug("Index name " + response.getIndex() + " type: " + response.getType() + " id: "
								+ response.getId() + " version " + response.getVersion());
				} catch (Exception e) {
					logger.error("Exception  " + e.getMessage() + " while indexing vocabulary", e);
				}

			}

		});

	}

}
