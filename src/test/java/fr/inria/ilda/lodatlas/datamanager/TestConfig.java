/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager;

/**
 * Configuration values to be used for the tests.
 * 
 * @author Hande Gözükan
 *
 */
public class TestConfig {

	public static final String DATAHUB_COLLECTION = "datahub";

	public static final String VOCABULARY_COLLECTION = "vocabulary";

	/**
	 * The directory where test files reside. The files in this directory should not
	 * be used directly. They should be copied to TEMP_PATH before usage and deleted
	 * afterwards.
	 */
	public static final String TEST_FILES_PATH = "test_files";

	/**
	 * The directory to be used during the tests.
	 */
	public static final String TEMP_PATH = "./tmp";

}
