/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager;

import java.io.IOException;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.Properties;

import fr.inria.ilda.lodatlas.datamanager.util.Util;

public class TestProcessPackage {

	/**
	 * The file that contains runtime configuration parameters.
	 */
	public static final String CONFIG_FILE = "./config.properties";

	public static final Properties prop = Util.loadProperties(CONFIG_FILE);

	public static void main(String[] args) {

		ProcessManager.Builder builder = ProcessManager.builder();

		// // package list file specified
		// builder = builder.setPackageList("pkglist.csv");
		//
		// // download JSON
		// builder = builder.downloadJson();

		// extract summaries
		try {
			builder = builder.extractSummaries(Paths.get("./tmp").toRealPath(LinkOption.NOFOLLOW_LINKS).toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// create elasticsearch indexes
		builder = builder.createElasticIndexes();

		// update links
		builder = builder.updateLinks();

		ProcessManager processManager = builder.build();

		processManager.run();
	}

}
