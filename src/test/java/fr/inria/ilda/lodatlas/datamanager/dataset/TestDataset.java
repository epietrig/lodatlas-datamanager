/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.bson.Document;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.datamanager.dataset.Dataset;

/**
 * A test class to test {@link Dataset} class.
 * 
 * @author Hande Gözükan
 *
 */
class TestDataset {

	String DATASET_NAME = "test dataset";
	String REPO_NAME = "test repo";

	Dataset testDataset;
	Document testDocument;
	Document testDocumentNoName;
	Document testDocumentDiffName;

	@BeforeEach
	void setUp() throws Exception {
		testDataset = new Dataset(DATASET_NAME, REPO_NAME);
		testDocument = Document.parse("{\"name\": \"" + DATASET_NAME + "\", \"" + DatasetStrings.RESOURCES
				+ "\": [{\"id\" : \"b1662e53-67fb-4ceb-bde3-8d6d08bcae4c\"},{\"id\" : \"6bfd8464-ea6d-4e54-a9e2-50ae57eaaddf\"}]}");
		testDocumentNoName = Document.parse("{\"" + DatasetStrings.RESOURCES
				+ "\": [{\"id\" : \"b1662e53-67fb-4ceb-bde3-8d6d08bcae4c\"},{\"id\" : \"6bfd8464-ea6d-4e54-a9e2-50ae57eaaddf\"}]}");
		testDocumentDiffName = Document.parse("{\"name\": \"diffname\", \"" + DatasetStrings.RESOURCES
				+ "\": [{\"id\" : \"b1662e53-67fb-4ceb-bde3-8d6d08bcae4c\"},{\"id\" : \"6bfd8464-ea6d-4e54-a9e2-50ae57eaaddf\"}]}");
	}

	@AfterEach
	void tearDown() throws Exception {
		testDataset = null;
		testDocument = null;
	}

	@Test
	void testConstructor() {
		Dataset pkg = new Dataset(DATASET_NAME, REPO_NAME);
		assertNotNull(pkg);
		assertEquals(DATASET_NAME, pkg.getName());
		assertEquals(REPO_NAME, pkg.getRepoName());
		assertNull(pkg.getBson());

		// test for empty string dataset name
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			new Dataset("", "reponame");
		});

		assertEquals("Dataset name cannot be null or empty string.", ex.getMessage());

		// test for null package name
		ex = assertThrows(IllegalArgumentException.class, () -> {
			new Dataset(null, "reponame");
		});

		assertEquals("Dataset name cannot be null or empty string.", ex.getMessage());

		// test for empty repo name
		ex = assertThrows(IllegalArgumentException.class, () -> {
			new Dataset("pkgname", "");
		});

		assertEquals("Repository name cannot be null or empty string.", ex.getMessage());

		// test for null repo name
		ex = assertThrows(IllegalArgumentException.class, () -> {
			new Dataset("pkgName", null);
		});

		assertEquals("Repository name cannot be null or empty string.", ex.getMessage());
	}

	@Test
	void testGetPackageName() {
		assertEquals(DATASET_NAME, testDataset.getName());
	}

	@Test
	void testGetRepoName() {
		assertEquals(REPO_NAME, testDataset.getRepoName());
	}

	@Test
	void testSetGetBson() {
		assertNull(testDataset.getBson());

		// test for null document
		testDataset.setBson(null);
		assertNull(testDataset.getBson());
		
		// test for document with no name field
		testDataset.setBson(testDocumentNoName);
		assertNull(testDataset.getBson());

		// test for document with name field different from package name
		testDataset.setBson(testDocumentDiffName);
		assertNull(testDataset.getBson());
		
		testDataset.setBson(testDocument);
		assertEquals(testDocument, testDataset.getBson());
	}

}
