/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package fr.inria.ilda.lodatlas.datamanager.dataset;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import fr.inria.ilda.lodatlas.datamanager.dataset.DatasetIterator;

/**
 * A test class to test {@link DatasetIterator}.
 * 
 * @see DatasetIterator
 * 
 * @author Hande Gözükan
 *
 */
public class TestDatasetIterator {

	@BeforeEach
	public void setUp() throws Exception {
	}

	@AfterEach
	public void tearDown() throws Exception {
	}

	/**
	 * Tests
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.DatasetIterator#DatasetIterator(java.lang.String)}
	 * method.
	 */
	@Test
	@Disabled
	public void testDatasetIterator() {
		fail("Not yet implemented");
	}

	/**
	 * Tests {@link fr.inria.ilda.lodatlas.datamanager.dataset.DatasetIterator#size()}
	 * method.
	 */
	@Test
	@Disabled
	public void testSize() {
		fail("Not yet implemented");
	}

	/**
	 * Tests
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.DatasetIterator#hasNext()}
	 * method.
	 */
	@Test
	@Disabled
	public void testHasNext() {
		fail("Not yet implemented");
	}

	/**
	 * Tests {@link fr.inria.ilda.lodatlas.datamanager.dataset.DatasetIterator#next()}
	 * method.
	 */
	@Test
	@Disabled
	public void testNext() {
		fail("Not yet implemented");
	}

	/**
	 * Tests {@link fr.inria.ilda.lodatlas.datamanager.dataset.DatasetIterator#reset()}
	 * method.
	 */
	@Test
	@Disabled
	public void testReset() {
		fail("Not yet implemented");
	}

}
