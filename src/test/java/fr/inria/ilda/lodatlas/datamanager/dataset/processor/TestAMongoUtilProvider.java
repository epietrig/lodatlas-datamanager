/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import org.bson.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.MongoUtil;

/**
 * An abstract test class to be used by the decorator test classes which needs
 * {@link MongoUtil}.
 * 
 * @author Hande Gözükan
 *
 */
abstract class TestAMongoUtilProvider extends TestAProcessDecorator {

	String HOST = "localhost";
	int PORT = 27017;
	String DB_NAME = "lodatlas_test";

	String TEST_DOC_NAME = "test";
	String TEST_DOC_NON_EXISTING = "doesnotexist";

	MongoUtil mongoUtil;
	MongoClient mongoClient;
	MongoDatabase mongoDB;

	@BeforeAll
	void initAll() {
		super.initAll();
		mongoUtil = new MongoUtil(HOST, PORT, DB_NAME);

		mongoClient = new MongoClient(HOST, PORT);
		mongoClient.dropDatabase(DB_NAME);
		mongoDB = mongoClient.getDatabase(DB_NAME);

	}

	@BeforeEach
	void init() {
		mongoDB = mongoClient.getDatabase(DB_NAME);
		testDocument = Document.parse("{\"name\": \"" + TEST_DOC_NAME + "\", \"" + DatasetStrings.RESOURCES
				+ "\": [{\"id\" : \"b1662e53-67fb-4ceb-bde3-8d6d08bcae4c\"},{\"id\" : \"6bfd8464-ea6d-4e54-a9e2-50ae57eaaddf\"}]}");
	}

	@AfterEach
	void tearDown() {
		mongoDB.drop();

		mongoDB = null;
		testDocument = null;
	}

	@AfterAll
	void tearDownAll() {
		super.tearDownAll();
		mongoUtil.dispose();
		mongoUtil = null;

		mongoClient.close();
		mongoClient = null;
	}
}
