/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.bson.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;
import fr.inria.ilda.lodatlas.datamanager.dataset.processor.AProcessorDecorator;
import fr.inria.ilda.lodatlas.datamanager.dataset.processor.DatasetProcessor;
import fr.inria.ilda.lodatlas.datamanager.dataset.processor.IDatasetProcessor;

/**
 * An abstract test class to test {@link AProcessorDecorator} abstract class.
 * 
 * @author Hande Gözükan
 *
 */
abstract class TestAProcessDecorator {

	/**
	 * The dataset processor that will be used to test process decorator classes.
	 */
	IDatasetProcessor datasetProcessor;

	/**
	 * The dataset which will be used for testing process decorators.
	 */
	IDataset testDataset;

	/**
	 * A BSON document to be used for the tests.
	 */
	Document testDocument;

	@BeforeAll
	void initAll() {
		datasetProcessor = new DatasetProcessor();
	}

	@AfterAll
	void tearDownAll() {
		datasetProcessor = null;
		
		testDocument = null;
		testDataset = null;
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.processor.AProcessorDecorator#processDataset(fr.inria.ilda.lodatlas.datamanager.dataset.IDataset)}.
	 */
	@Test
	abstract void testProcessDataset();

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.processor.AProcessorDecorator#processDataset(fr.inria.ilda.lodatlas.datamanager.dataset.IDataset)}.
	 */
	@Test
	void testProcessDataset_NullDataset() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			datasetProcessor.processDataset(null);
		});

		assertEquals("The dataset must not be null.", ex.getMessage());
	}

}
