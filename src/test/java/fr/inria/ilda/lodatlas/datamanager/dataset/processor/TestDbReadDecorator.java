/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.inria.ilda.lodatlas.datamanager.dataset.Dataset;
import fr.inria.ilda.lodatlas.datamanager.dataset.processor.DbReadDecorator;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.Config;

/**
 * A test class to test {@link DbReadDecorator} class.
 * 
 * @author Hande Gözükan
 *
 */
class TestDbReadDecorator extends TestAMongoUtilProvider {

	Dataset non_existing_dataset;

	@BeforeAll
	@Override
	void initAll() {
		super.initAll();
		datasetProcessor = new DbReadDecorator(datasetProcessor, mongoUtil);
	}

	@BeforeEach
	@Override
	void init() {
		super.init();

		testDataset = new Dataset(TEST_DOC_NAME, "temp");
		non_existing_dataset = new Dataset(TEST_DOC_NON_EXISTING, "temp");

		// insert test document to be able to read from db
		mongoDB.getCollection(Config.DATASET_COLLECTION).insertOne(testDocument);
	}

	@AfterEach
	@Override
	void tearDown() {
		super.tearDown();

		testDataset = null;
		non_existing_dataset = null;
	}
	@Test
	void testProcessDataset() {
		datasetProcessor.processDataset(testDataset);
		assertEquals(testDocument, testDataset.getBson());

		datasetProcessor.processDataset(non_existing_dataset);
		assertNull(non_existing_dataset.getBson());
	}
}
