/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.HashMap;
import java.util.Map;

import org.bson.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.datamanager.dataset.Dataset;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.Config;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.MongoUtil;
import fr.inria.ilda.lodatlas.datamanager.repository.CkanV3Connector;
import fr.inria.ilda.lodatlas.datamanager.repository.IRepoApiConnector;

/**
 * A test class to test {@link DownloadDecorator} class.
 * 
 * @author Hande Gözükan
 *
 */
class TestDownloadDecorator extends TestAProcessDecorator {

	String DATASET_DBPEDIA = "dbpedia";
	String NON_EXISTING_DATASET_NAME = "doesnotexist";
	String NON_EXISTING_REPO_NAME = "doesnotexist";

	String REPO_NAME = "datahub";
	String REPO_URL = "https://old.datahub.io/";
	String QUERY = "q=tags:lod";

	IRepoApiConnector datahubConnector;
	Map<String, IRepoApiConnector> repoList;

	Dataset testDataset2;
	Dataset nonExistingDataset;

	int START_INDEX = 0;
	int TOTAL_COUNT = -1;

	String HOST = "localhost";
	int PORT = 27017;
	String DB_NAME = "lodatlas_test";

	MongoUtil mongoUtil;
	MongoClient mongoClient;
	MongoDatabase mongoDB;

	Document testDocument;

	@BeforeAll
	@Override
	void initAll() {
		super.initAll();
		repoList = new HashMap<>();
		datahubConnector = new CkanV3Connector(REPO_NAME, REPO_URL, QUERY, START_INDEX, TOTAL_COUNT);
		repoList.put(REPO_NAME, datahubConnector);

		mongoUtil = new MongoUtil(HOST, PORT, DB_NAME);

		mongoClient = new MongoClient(HOST, PORT);
		mongoClient.dropDatabase(DB_NAME);
		mongoDB = mongoClient.getDatabase(DB_NAME);

		testDocument = Document.parse(
				"{\"name\": \"" + DATASET_DBPEDIA + "\", \"" + DatasetStrings.REPO_NAME + "\": \"" + REPO_NAME + "\"}");

		datasetProcessor = new DownloadDecorator(datasetProcessor, repoList, mongoUtil);
	}

	@BeforeEach
	void init() {
		mongoDB = mongoClient.getDatabase(DB_NAME);

		testDataset = new Dataset(DATASET_DBPEDIA, REPO_NAME);
		testDataset2 = new Dataset(DATASET_DBPEDIA, NON_EXISTING_REPO_NAME);
		nonExistingDataset = new Dataset(NON_EXISTING_DATASET_NAME, REPO_NAME);
	}

	@AfterEach
	void tearDown() {
		testDataset = null;
		testDataset2 = null;
		nonExistingDataset = null;

		mongoDB.drop();
		mongoDB = null;
	}

	@AfterAll
	@Override
	void tearDownAll() {
		super.tearDownAll();
		repoList.clear();
		repoList = null;

		datahubConnector = null;
		datasetProcessor = null;

		mongoUtil.dispose();
		mongoUtil = null;

		mongoClient.close();
		mongoClient = null;
		
		testDocument = null;
	}

	/**
	 * Tests
	 * {@link DownloadDecorator#processDataset(fr.inria.ilda.lodatlas.datamanager.dataset.IDataset)}
	 * method.
	 */
	@Test
	void testProcessDataset() {
		datasetProcessor.processDataset(testDataset);
		assertNotNull(testDataset.getBson());
	}

	/**
	 * Tests
	 * {@link DownloadDecorator#processDataset(fr.inria.ilda.lodatlas.datamanager.dataset.IDataset)}
	 * method where repo name for dataset does not match any repo in the repo list.
	 */
	@Test
	void testProcessDataset2() {
		datasetProcessor.processDataset(testDataset2);
		assertNull(testDataset2.getBson());
	}

	/**
	 * Tests
	 * {@link DownloadDecorator#processDataset(fr.inria.ilda.lodatlas.datamanager.dataset.IDataset)}
	 * method for the case where the dataset does not exist in the repo.
	 */
	@Test
	void testProcessDataset3() {
		datasetProcessor.processDataset(nonExistingDataset);
		assertNull(nonExistingDataset.getBson());
	}

	/**
	 * Tests
	 * {@link DownloadDecorator#processDataset(fr.inria.ilda.lodatlas.datamanager.dataset.IDataset)}
	 * method for the case where the dataset does not exist in the repo.
	 */
	@Test
	void testProcessDataset4() {
		// insert dataset to download
		mongoDB.getCollection(Config.DATASET_COLLECTION).insertOne(testDocument);
		assertEquals(1, mongoDB.getCollection(Config.DATASET_COLLECTION).count());

		datasetProcessor.processDataset(testDataset);
		assertNull(testDataset.getBson());
	}

}
