/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.datamanager.dataset.Dataset;
import fr.inria.ilda.lodatlas.datamanager.dataset.processor.InsertDecorator;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.Config;

/**
 * A test class to test {@link InsertDecorator} class.
 * 
 * @author Hande Gözükan
 *
 */
class TestInsertDecorator extends TestAMongoUtilProvider {

	@BeforeAll
	@Override
	void initAll() {
		super.initAll();
		datasetProcessor = new InsertDecorator(datasetProcessor, mongoUtil);

	}

	@BeforeEach
	@Override
	void init() {
		super.init();
		testDataset = new Dataset(TEST_DOC_NAME, "temp");
	}

	@AfterEach
	@Override
	void tearDown() {
		super.tearDown();
		testDataset = null;
	}

	@Test
	void testProcessDataset() {
		// test for dataset whose bson document is not yet set
		assertEquals(0, mongoDB.getCollection(Config.DATASET_COLLECTION).count());
		datasetProcessor.processDataset(testDataset);
		assertEquals(0, mongoDB.getCollection(Config.DATASET_COLLECTION).count());

		// test with dataset whose bson document is set
		testDataset.setBson(testDocument);
		datasetProcessor.processDataset(testDataset);
		assertEquals(1, mongoDB.getCollection(Config.DATASET_COLLECTION).count());
		assertEquals(TEST_DOC_NAME,
				mongoDB.getCollection(Config.DATASET_COLLECTION).find().first().getString(DatasetStrings.NAME));
	}

}
