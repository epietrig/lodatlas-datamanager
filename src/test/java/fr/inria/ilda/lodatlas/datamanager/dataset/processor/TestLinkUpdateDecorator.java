/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.bson.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import com.mongodb.client.MongoCollection;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.datamanager.dataset.Dataset;
import fr.inria.ilda.lodatlas.datamanager.dataset.processor.LinkUpdateDecorator;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.Config;

/**
 * A test class to test {@link LinkUpdateDecorator} class.
 * 
 * @author Hande Gözükan
 *
 */
class TestLinkUpdateDecorator extends TestAMongoUtilProvider {

	String NON_EXISTING_DATASET = "nonexisting";
	String REPO_NAME = "reponame";
	String DOC_WITH_NO_LINKS_IN_DB = "nolinksindb";
	String DOC_WITH_LINKS_IN_DB = "withlinks";
	String DOC_WITH_LINKS_IN_DB2 = "withlinks2";
	String DOC_LINK1_NAME = "link1";
	String DOC_LINK2_NAME = "link2";

	Document docWithNoLinksInDb;
	Document docWithLinksInDb;
	Document docWithLinksInDb2;
	Document docLink1;
	Document docLink2;

	Dataset nonExistingDataset;
	Dataset datasetWithNoLinksInDb;
	Dataset datasetWithLinksInDb;
	Dataset datasetWithLinksInDb2;

	@BeforeAll
	@Override
	void initAll() {
		super.initAll();

		datasetProcessor = new LinkUpdateDecorator(datasetProcessor, mongoUtil);
		docWithNoLinksInDb = Document
				.parse("{\"name\": \"" + DOC_WITH_NO_LINKS_IN_DB + "\", \"" + DatasetStrings.OUTGOING_LINKS
						+ "\" : {\"count\" : 1,	\"links\" : [{\"name\" : \"dbpedia\", \"count\" : 200} ] } }");

		docWithLinksInDb = Document.parse("{\"name\": \"" + DOC_WITH_LINKS_IN_DB + "\", \""
				+ DatasetStrings.OUTGOING_LINKS + "\" : {\"count\" : 1,	\"links\" : [{\"name\" : \"" + DOC_LINK1_NAME
				+ "\", \"count\" : 1, \"name\" : \"" + DOC_LINK2_NAME + "\", \"count\" : 2} ] } }");

		docWithLinksInDb2 = Document.parse("{\"name\": \"" + DOC_WITH_LINKS_IN_DB2 + "\", \""
				+ DatasetStrings.OUTGOING_LINKS + "\" : {\"count\" : 1,	\"links\" : [{\"name\" : \"" + DOC_LINK1_NAME
				+ "\", \"count\" : 3} ] } }");

		docLink1 = Document.parse("{\"name\": \"" + DOC_LINK1_NAME + "\"}");

		docLink2 = Document.parse("{\"name\": \"" + DOC_LINK2_NAME + "\"}");

	}

	@BeforeEach
	@Override
	void init() {
		super.init();

		nonExistingDataset = new Dataset(NON_EXISTING_DATASET, REPO_NAME);

		// dataset with no links defined
		testDataset = new Dataset(TEST_DOC_NAME, REPO_NAME);
		testDataset.setBson(testDocument);

		// dataset where linked dataset does not exist in db
		datasetWithNoLinksInDb = new Dataset(DOC_WITH_NO_LINKS_IN_DB, REPO_NAME);
		datasetWithNoLinksInDb.setBson(docWithNoLinksInDb);

		// dataset with two outgoing links and the links exist in db (link1 and link2)
		datasetWithLinksInDb = new Dataset(DOC_WITH_LINKS_IN_DB, REPO_NAME);
		datasetWithLinksInDb.setBson(docWithLinksInDb);

		// dataset with one outgoing link and the same link with previous doc (link1)
		datasetWithLinksInDb2 = new Dataset(DOC_WITH_LINKS_IN_DB2, REPO_NAME);
		datasetWithLinksInDb2.setBson(docWithLinksInDb2);

		// insert all necessary documents to db
		MongoCollection<Document> datasetCollection = mongoDB.getCollection(Config.DATASET_COLLECTION);
		datasetCollection.insertOne(docLink1);
		datasetCollection.insertOne(docLink2);
		datasetCollection.insertOne(testDocument);
		datasetCollection.insertOne(docWithLinksInDb);
		datasetCollection.insertOne(docWithLinksInDb2);
		datasetCollection.insertOne(docWithNoLinksInDb);
	}

	@AfterEach
	@Override
	void tearDown() {
		super.tearDown();

		nonExistingDataset = null;
		testDataset = null;
		datasetWithNoLinksInDb = null;
		datasetWithLinksInDb = null;
		datasetWithLinksInDb2 = null;
	}

	@AfterAll
	@Override
	void tearDownAll() {
		super.tearDownAll();

		docWithNoLinksInDb = null;
		docWithLinksInDb = null;
		docWithLinksInDb2 = null;
		docLink1 = null;
		docLink2 = null;
	}

	@Override
	void testProcessDataset() {
		// test for dataset that does not exist, should not throw exception
		datasetProcessor.processDataset(nonExistingDataset);

		// test for dataset with no links defined, should not throw exception
		datasetProcessor.processDataset(testDataset);

		// test for dataset where the linked dataset does not exist in db, should not
		// throw exception
		datasetProcessor.processDataset(datasetWithNoLinksInDb);

		// test for dataset with links (link1 and link2) that all exist in db
		datasetProcessor.processDataset(datasetWithLinksInDb);
		Document link1 = mongoDB.getCollection(Config.DATASET_COLLECTION)
				.find(new Document(DatasetStrings.NAME, DOC_LINK1_NAME)).first();
		Document incomingLink1 = (Document) link1.get(DatasetStrings.INCOMING_LINKS);
		assertNotNull(incomingLink1);
		assertEquals(1, incomingLink1.get(DatasetStrings.COUNT));
		List<Document> linkList1 = (List<Document>) incomingLink1.get(DatasetStrings.LINKS);
		assertEquals(1, linkList1.size());
		assertEquals(DOC_WITH_LINKS_IN_DB, linkList1.get(0).getString(DatasetStrings.NAME));
		assertEquals(1, linkList1.get(0).get(DatasetStrings.COUNT));

		Document link2 = mongoDB.getCollection(Config.DATASET_COLLECTION)
				.find(new Document(DatasetStrings.NAME, DOC_LINK2_NAME)).first();
		Document incomingLink2 = (Document) link2.get(DatasetStrings.INCOMING_LINKS);
		assertNotNull(incomingLink2);
		assertEquals(1, incomingLink2.get(DatasetStrings.COUNT));
		List<Document> linkList2 = (List<Document>) incomingLink2.get(DatasetStrings.LINKS);
		assertEquals(1, linkList2.size());
		assertEquals(DOC_WITH_LINKS_IN_DB, linkList2.get(0).getString(DatasetStrings.NAME));
		assertEquals(2, linkList2.get(0).get(DatasetStrings.COUNT));

		// test for dataset with a link (link1) that exist in db and already has
		// incoming links
		datasetProcessor.processDataset(datasetWithLinksInDb2);
		link1 = mongoDB.getCollection(Config.DATASET_COLLECTION).find(new Document(DatasetStrings.NAME, DOC_LINK1_NAME))
				.first();
		incomingLink1 = (Document) link1.get(DatasetStrings.INCOMING_LINKS);
		assertNotNull(incomingLink1);
		assertEquals(2, incomingLink1.get(DatasetStrings.COUNT));
		linkList1 = (List<Document>) incomingLink1.get(DatasetStrings.LINKS);
		assertEquals(2, linkList1.size());
		assertEquals(DOC_WITH_LINKS_IN_DB, linkList1.get(0).getString(DatasetStrings.NAME));
		assertEquals(1, linkList1.get(0).get(DatasetStrings.COUNT));

		assertEquals(DOC_WITH_LINKS_IN_DB2, linkList1.get(1).getString(DatasetStrings.NAME));
		assertEquals(3, linkList1.get(1).get(DatasetStrings.COUNT));
	}

}
