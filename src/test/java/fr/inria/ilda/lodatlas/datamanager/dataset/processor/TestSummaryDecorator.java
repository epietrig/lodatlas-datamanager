/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package fr.inria.ilda.lodatlas.datamanager.dataset.processor;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.bson.Document;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;

/**
 * @author Hande Gözükan
 *
 */
class TestSummaryDecorator {

	private Document docBlukBnb;

	private IDataset pkg;

	private static final String TEMP_PATH = "./tmp";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.inria.ilda.jckanmanager.pkg.processor.TestAMongoProcessDecorator#initAll()
	 */
	@BeforeAll
	void initAll() {
		// pkgProcessor = new SummaryDecorator(pkgProcessor, TEMP_PATH, collection);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.inria.ilda.jckanmanager.pkg.processor.TestAMongoProcessDecorator#init()
	 */
	@BeforeEach
	void init() {
		// super.init();
		//
		// docBlukBnb = Document.parse(
		// "{\"license_title\":\"Creative Commons
		// CCZero\",\"maintainer\":\"\",\"relationships_as_object\":[],\"private\":false,\"maintainer_email\":\"metadata@bl.uk\",\"num_tags\":15,\"id\":\"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\",\"metadata_created\":\"2011-07-20T09:32:36.692753\",\"metadata_modified\":\"2017-07-13T13:57:30.191597\",\"author\":\"The
		// British Library Metadata
		// Services\",\"author_email\":\"metadata@bl.uk\",\"state\":\"active\",\"version\":\"\",\"creator_user_id\":\"665204e4-cc46-4d42-a230-7d7a83bebda9\",\"type\":\"dataset\",\"resources\":[{\"mimetype\":\"\",\"cache_url\":\"\",\"hash\":\"\",\"description\":\"SPARQL
		// Endpoint\",\"format\":\"api/sparql\",\"url\":\"http://bnb.data.bl.uk/sparql\",\"datastore_active\":false,\"cache_last_updated\":null,\"package_id\":\"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\",\"name\":\"SPARQL
		// Endpoint\",\"state\":\"active\",\"mimetype_inner\":\"\",\"last_modified\":null,\"position\":0,\"revision_id\":\"f25de347-1693-467c-9712-7d8eba9b0d0b\",\"url_type\":null,\"id\":\"7e3ba852-c9c0-430c-b829-5a68867fcb67\",\"resource_type\":\"file\",\"size\":null},{\"mimetype\":\"\",\"cache_url\":\"\",\"hash\":\"\",\"description\":\"Search
		// Service\",\"format\":\"api/search\",\"url\":\"http://bnb.data.bl.uk/search\",\"datastore_active\":false,\"cache_last_updated\":null,\"package_id\":\"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\",\"name\":\"Search
		// Service\",\"state\":\"active\",\"mimetype_inner\":\"\",\"last_modified\":null,\"position\":1,\"revision_id\":\"f25de347-1693-467c-9712-7d8eba9b0d0b\",\"url_type\":null,\"id\":\"c4614594-e372-4937-b999-91ddb0db1ff4\",\"resource_type\":\"file\",\"size\":null},{\"mimetype\":\"\",\"cache_url\":\"\",\"hash\":\"\",\"description\":\"Example
		// resource -
		// RDF\",\"format\":\"example/rdf+xml\",\"url\":\"http://bnb.data.bl.uk/doc/resource/006893251.rdf\",\"datastore_active\":false,\"cache_last_updated\":null,\"package_id\":\"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\",\"name\":\"Example
		// resource -
		// RDF\",\"state\":\"active\",\"mimetype_inner\":\"\",\"last_modified\":null,\"position\":2,\"revision_id\":\"f25de347-1693-467c-9712-7d8eba9b0d0b\",\"url_type\":null,\"id\":\"a814828a-0ea9-43aa-9628-cd6b999e14cb\",\"resource_type\":\"file\",\"size\":null},{\"mimetype\":\"\",\"cache_url\":\"\",\"hash\":\"\",\"description\":\"Example
		// resource -
		// Turtle\",\"format\":\"example/rdf+ttl\",\"url\":\"http://bnb.data.bl.uk/doc/resource/006893251.ttl\",\"datastore_active\":false,\"cache_last_updated\":null,\"package_id\":\"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\",\"name\":\"Example
		// resource -
		// Turtle\",\"state\":\"active\",\"mimetype_inner\":\"\",\"last_modified\":null,\"position\":3,\"revision_id\":\"f25de347-1693-467c-9712-7d8eba9b0d0b\",\"url_type\":null,\"id\":\"402cf623-3057-440a-8e5f-4ca3619f3e28\",\"resource_type\":\"file\",\"size\":null},{\"mimetype\":\"\",\"cache_url\":\"\",\"hash\":\"\",\"description\":\"Example
		// resource -
		// JSON\",\"format\":\"example/rdf+json\",\"url\":\"http://bnb.data.bl.uk/doc/resource/006893251.json\",\"datastore_active\":false,\"cache_last_updated\":null,\"package_id\":\"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\",\"name\":\"Example
		// resource -
		// JSON\",\"state\":\"active\",\"mimetype_inner\":\"\",\"last_modified\":null,\"position\":4,\"revision_id\":\"f25de347-1693-467c-9712-7d8eba9b0d0b\",\"url_type\":null,\"id\":\"7d219dcb-4028-4f9f-a7e2-f627e046c620\",\"resource_type\":\"file\",\"size\":null},{\"mimetype\":\"\",\"cache_url\":\"\",\"hash\":\"\",\"description\":\"Example
		// resource -
		// html\",\"format\":\"example/html\",\"url\":\"http://bnb.data.bl.uk/doc/resource/006893251.html\",\"datastore_active\":false,\"cache_last_updated\":null,\"package_id\":\"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\",\"name\":\"Example
		// resource -
		// html\",\"state\":\"active\",\"mimetype_inner\":\"\",\"last_modified\":null,\"position\":5,\"revision_id\":\"f25de347-1693-467c-9712-7d8eba9b0d0b\",\"url_type\":null,\"id\":\"11e18177-3c02-451f-9dc6-62532f8aaebe\",\"resource_type\":\"file\",\"size\":null},{\"mimetype\":\"\",\"cache_url\":\"\",\"hash\":\"\",\"description\":\"Example
		// resource RDF - ttl/json/html - default
		// html\",\"format\":\"example\",\"url\":\"http://bnb.data.bl.uk/doc/resource/006893251\",\"datastore_active\":false,\"cache_last_updated\":null,\"package_id\":\"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\",\"name\":\"Example
		// resource RDF - ttl/json/html - default
		// html\",\"state\":\"active\",\"mimetype_inner\":\"\",\"last_modified\":null,\"position\":6,\"revision_id\":\"f25de347-1693-467c-9712-7d8eba9b0d0b\",\"url_type\":null,\"id\":\"13aaf04d-8920-478a-908a-7b0dd858be51\",\"resource_type\":\"file\",\"size\":null},{\"mimetype\":\"\",\"cache_url\":\"\",\"hash\":\"\",\"description\":\"British
		// Library Terms RDF
		// Schema\",\"format\":\"XML\",\"url\":\"http://www.bl.uk/schemas/bibliographic/blterms\",\"datastore_active\":false,\"cache_last_updated\":null,\"package_id\":\"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\",\"name\":\"British
		// Library Terms RDF
		// Schema\",\"state\":\"active\",\"mimetype_inner\":\"\",\"last_modified\":\"2012-01-09T00:00:00\",\"position\":7,\"revision_id\":\"f25de347-1693-467c-9712-7d8eba9b0d0b\",\"url_type\":null,\"id\":\"2f5394cf-d013-469e-bd02-785fb32e527b\",\"resource_type\":\"documentation\",\"size\":null},{\"mimetype\":\"\",\"cache_url\":\"\",\"hash\":\"\",\"description\":\"Current
		// Information on releases and
		// documentation\",\"format\":\"HTML\",\"url\":\"http://www.bl.uk/bibliographic/datafree.html\",\"datastore_active\":false,\"cache_last_updated\":null,\"package_id\":\"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\",\"name\":\"Current
		// Information on releases and
		// documentation\",\"state\":\"active\",\"mimetype_inner\":\"\",\"last_modified\":\"2012-01-19T00:00:00\",\"position\":8,\"revision_id\":\"f25de347-1693-467c-9712-7d8eba9b0d0b\",\"url_type\":null,\"id\":\"d5d87f85-52d7-4e82-bdd5-4b3461a4087d\",\"resource_type\":\"documentation\",\"size\":null},{\"mimetype\":\"application/pdf\",\"cache_url\":\"\",\"hash\":\"\",\"description\":\"This
		// is a diagram detailing the data model developed for books, including books
		// published over time.\",\"format\":\"html |
		// pdf\",\"url\":\"http://www.bl.uk/bibliographic/pdfs/bldatamodelbook.pdf\",\"datastore_active\":false,\"cache_last_updated\":null,\"package_id\":\"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\",\"name\":\"Data
		// model
		// (Book)\",\"state\":\"active\",\"mimetype_inner\":\"\",\"last_modified\":\"2012-08-17T07:42:59.832014\",\"position\":9,\"revision_id\":\"f25de347-1693-467c-9712-7d8eba9b0d0b\",\"url_type\":null,\"id\":\"2406ea11-d00c-43fe-a81b-9fcd3c774ca1\",\"resource_type\":\"file\",\"size\":\"131385\"},{\"mimetype\":\"application/pdf\",\"cache_url\":\"\",\"hash\":\"\",\"description\":\"This
		// is the diagram detailing the data model developed for serials, i.e. journals,
		// newpapers, etc.\",\"name\":\"Data model (Serial)\",\"format\":\"html |
		// pdf\",\"url\":\"http://www.bl.uk/bibliographic/pdfs/bldatamodelserial.pdf\",\"datastore_active\":false,\"cache_last_updated\":null,\"package_id\":\"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\",\"created\":\"2012-08-17T07:47:39.548378\",\"state\":\"active\",\"mimetype_inner\":\"\",\"last_modified\":\"2012-08-17T06:31:28\",\"position\":10,\"revision_id\":\"f25de347-1693-467c-9712-7d8eba9b0d0b\",\"url_type\":null,\"id\":\"139d8f2d-727d-4db7-bfe7-26b0162cc063\",\"resource_type\":\"file\",\"size\":\"134955\"},{\"mimetype\":null,\"cache_url\":null,\"hash\":\"\",\"description\":\"This
		// SPARQL editor enables the query of the BNB using SPARQL.\",\"name\":\"SPARQL
		// Editor\",\"format\":\"\",\"url\":\"http://bnb.data.bl.uk/flint\",\"datastore_active\":false,\"cache_last_updated\":null,\"package_id\":\"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\",\"created\":\"2013-09-09T08:27:18.649078\",\"state\":\"active\",\"mimetype_inner\":null,\"last_modified\":null,\"position\":11,\"revision_id\":\"f25de347-1693-467c-9712-7d8eba9b0d0b\",\"url_type\":null,\"id\":\"285f0a3c-795f-4e8d-a7b2-c75479688a13\",\"resource_type\":\"api\",\"size\":null},{\"mimetype\":null,\"cache_url\":null,\"hash\":\"\",\"description\":\"This
		// page provides access to bulk downloads of the datasets (Books,
		// Serials)\",\"name\":\"Bulk
		// downloads\",\"format\":\"HTML\",\"url\":\"http://www.bl.uk/bibliographic/download.html#lodbnb\",\"datastore_active\":false,\"cache_last_updated\":null,\"package_id\":\"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\",\"created\":\"2013-09-27T05:12:59.372304\",\"state\":\"active\",\"mimetype_inner\":null,\"last_modified\":null,\"position\":12,\"revision_id\":\"f25de347-1693-467c-9712-7d8eba9b0d0b\",\"url_type\":null,\"id\":\"f6fccaa9-0595-4fc4-aa88-da0cb79d513a\",\"resource_type\":\"file\",\"size\":null},{\"mimetype\":null,\"cache_url\":null,\"hash\":\"\",\"description\":\"This
		// is a diagram detailing the data model developed for forthcoming
		// books\",\"name\":\"Data model (Forthcoming
		// book)\",\"format\":\"html/pdf\",\"url\":\"http://www.bl.uk/bibliographic/pdfs/bldatamodelcip.pdf\",\"datastore_active\":false,\"cache_last_updated\":null,\"package_id\":\"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\",\"created\":\"2017-07-13T11:21:58.575868\",\"state\":\"active\",\"mimetype_inner\":null,\"last_modified\":null,\"position\":13,\"revision_id\":\"33cda69e-a368-4a7f-9b08-0d831a0ec4ff\",\"url_type\":null,\"id\":\"b2fb3e06-54ae-440f-8c34-b6f5100c2546\",\"resource_type\":null,\"size\":null}],\"num_resources\":14,\"tags\":[{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"bibliographic\",\"id\":\"01d76f4d-6bf5-4cec-a2a8-59dc3f24ab2d\",\"name\":\"bibliographic\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"british-library\",\"id\":\"bd30d367-b9a9-4a60-aba7-ef19f1ffaf09\",\"name\":\"british-library\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"cc0\",\"id\":\"c317d3df-654b-4695-b2ee-ea7326d00ded\",\"name\":\"cc0\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"crossdomain\",\"id\":\"833848ee-ef06-4283-bb26-164f6c3474b4\",\"name\":\"crossdomain\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"format-rdf\",\"id\":\"9dd6b48e-299d-427b-95a9-b54708459ab9\",\"name\":\"format-rdf\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"format-xhtml\",\"id\":\"05609255-b76b-4056-a895-1fb3da7c734a\",\"name\":\"format-xhtml\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"format-xml\",\"id\":\"daeead63-cb28-4025-a706-c2f59d388955\",\"name\":\"format-xml\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"library\",\"id\":\"7ec1ef56-6c27-4299-a3fa-578756ddc421\",\"name\":\"library\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"lld\",\"id\":\"c1dea2c0-3fb8-47b8-9b37-c076756e6420\",\"name\":\"lld\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"lod\",\"id\":\"54a512d9-f003-4b79-badf-c85bfa977a7e\",\"name\":\"lod\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"lodcloud-diagram-2011-09-19\",\"id\":\"fc965007-562f-4999-a511-d73aed0e7506\",\"name\":\"lodcloud-diagram-2011-09-19\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"lodcloud-diagram-2014-08-30\",\"id\":\"6f8ff1ee-4550-4648-86ae-5416aa53fad0\",\"name\":\"lodcloud-diagram-2014-08-30\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"no-proprietary-vocab\",\"id\":\"aef04cb8-a7aa-4419-8184-396b0ef6e680\",\"name\":\"no-proprietary-vocab\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"publications\",\"id\":\"c93ebe8f-83a0-498c-8704-1de94db149e9\",\"name\":\"publications\"},{\"vocabulary_id\":null,\"state\":\"active\",\"display_name\":\"published-by-producer\",\"id\":\"fcdb5c0a-2b91-44cb-83e5-19fd0d81125c\",\"name\":\"published-by-producer\"}],\"groups\":[],\"license_id\":\"cc-zero\",\"relationships_as_subject\":[],\"organization\":{\"description\":\"\",\"created\":\"2014-01-09T19:31:53.978627\",\"title\":\"British
		// Library\",\"name\":\"british-library\",\"is_organization\":true,\"state\":\"active\",\"image_url\":\"http://www.bl.uk/images/bl_logo_100.gif\",\"revision_id\":\"72aca5fa-481e-4e6e-a54f-d1019b6fa477\",\"type\":\"organization\",\"id\":\"e9930c36-818c-42e1-af42-8138e85a323c\",\"approval_status\":\"approved\"},\"name\":\"bluk-bnb\",\"isopen\":true,\"url\":\"http://bnb.data.bl.uk\",\"notes\":\"British
		// National Bibliography (BNB) published as Linked Data by the British Library,
		// linked to external sources including: [VIAF](http://viaf.org/),
		// [ISNI](http://isni.org), [LCSH](http://id.loc.gov/authorities/),
		// [Lexvo](http://www.lexvo.org/),
		// [GeoNames](http://www.geonames.org/ontology/documentation.html), MARC
		// [country](http://id.loc.gov/vocabulary/countries.html), and
		// [language](http://id.loc.gov/vocabulary/languages.html),
		// [Dewey.info](http://dewey.info/). Published to [this data model for
		// books](https://www.bl.uk/bibliographic/pdfs/bldatamodelbook.pdf); [this data
		// model for
		// serials](https://www.bl.uk/bibliographic/pdfs/bldatamodelserial.pdf) and this
		// data model for forthcoming books
		// (http://www.bl.uk/bibliographic/pdfs/bldatamodelcip.pdf).\\r\\n\\r\\nCurrent
		// release of approximately 3.9 million descriptions (146,901,135 triples) of
		// books (including monographs published over time); forthcoming books and
		// serials published in the UK over the last 60 years.The dataset, covering UK
		// publications since 1950, is updated
		// monthly.\",\"owner_org\":\"e9930c36-818c-42e1-af42-8138e85a323c\",\"extras\":[{\"key\":\"links:ddc
		// (books)\",\"value\":\"222110\"},{\"key\":\"links:ddc (forthcoming
		// books)\",\"value\":\"100687\"},{\"key\":\"links:ddc
		// (serials)\",\"value\":\"29263\"},{\"key\":\"links:isni
		// (books)\",\"value\":\"775485\"},{\"key\":\"links:isni (forthcoming
		// books)\",\"value\":\"273293\"},{\"key\":\"links:isni
		// (serials)\",\"value\":\"7592\"},{\"key\":\"links:lcsh
		// (books)\",\"value\":\"142026\"},{\"key\":\"links:lcsh (forthcoming
		// books)\",\"value\":\"83189\"},{\"key\":\"links:lcsh
		// (serials)\",\"value\":\"18642\"},{\"key\":\"links:viaf
		// (books)\",\"value\":\"1127476\"},{\"key\":\"links:viaf (forthcoming
		// books)\",\"value\":\"359437\"},{\"key\":\"links:viaf
		// (serials)\",\"value\":\"22286\"},{\"key\":\"namespace\",\"value\":\"http://bnb.data.bl.uk/id/\"},{\"key\":\"shortname\",\"value\":\"BNB\"},{\"key\":\"triples\",\"value\":\"146901135\"}],\"license_url\":\"http://www.opendefinition.org/licenses/cc-zero\",\"title\":\"British
		// National Bibliography (BNB) - Linked Open
		// Data\",\"revision_id\":\"73722ec8-2795-498d-a0de-b1030a262d4e\",\"repoName\":\"datahub\",\"repoDatasetUrl\":\"http://old.datahub.io/api/3/action/package_show?id=bluk-bnb\",\"tripleCount\":146901135,\"namespace\":\"http://bnb.data.bl.uk/id/\",\"outgoinglinks\":{\"count\":12,\"links\":[{\"name\":\"ddc
		// (books)\",\"count\":222110},{\"name\":\"ddc (forthcoming
		// books)\",\"count\":100687},{\"name\":\"ddc
		// (serials)\",\"count\":29263},{\"name\":\"isni
		// (books)\",\"count\":775485},{\"name\":\"isni (forthcoming
		// books)\",\"count\":273293},{\"name\":\"isni
		// (serials)\",\"count\":7592},{\"name\":\"lcsh
		// (books)\",\"count\":142026},{\"name\":\"lcsh (forthcoming
		// books)\",\"count\":83189},{\"name\":\"lcsh
		// (serials)\",\"count\":18642},{\"name\":\"viaf
		// (books)\",\"count\":1127476},{\"name\":\"viaf (forthcoming
		// books)\",\"count\":359437},{\"name\":\"viaf
		// (serials)\",\"count\":22286}]},\"incominglinks\":{\"count\":0,\"links\":[]}}");
		//
		//// pkg = new Package("bluk-bnb");
		// pkg.setBson(docBlukBnb);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.inria.ilda.jckanmanager.pkg.processor.TestAMongoProcessDecorator#tearDown(
	 * )
	 */
	@AfterEach
	void tearDown() throws IOException {

		pkg = null;
		docBlukBnb = null;

		FileUtils.cleanDirectory(new File(TEMP_PATH));
	}

	/**
	 * 
	 * /** Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.processor.AProcessorDecorator#processDataset(fr.inria.ilda.lodatlas.datamanager.dataset.IDataset)}.
	 */
	@Test
	@Disabled
	void testProcessPackage() {
		// pkgProcessor.processPackage(pkg);
		// TODO
	}

}
