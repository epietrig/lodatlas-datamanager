package fr.inria.ilda.lodatlas.datamanager.dataset.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import fr.inria.ilda.lodatlas.commons.model.FormatType;

/**
 * A test class to test {@link FormatType} enum class.
 * 
 * @see FormatType
 * 
 * @author Hande Gözükan
 *
 */
class TestFormatType {

	/**
	 * Tests {@link FormatType#canDownload()} method.
	 */
	@Test
	void testCanDownload() {
		assertFalse(FormatType.SPARQL.canDownload());
		assertTrue(FormatType.RDF.canDownload());
		assertTrue(FormatType.TTL.canDownload());
		assertTrue(FormatType.N3.canDownload());
		assertTrue(FormatType.NT.canDownload());
		assertTrue(FormatType.NQ.canDownload());
		assertTrue(FormatType.TRIG.canDownload());
		assertTrue(FormatType.LD_JSON.canDownload());
		assertTrue(FormatType.TRIX.canDownload());
		assertFalse(FormatType.META.canDownload());
		assertFalse(FormatType.VOID.canDownload());
		assertFalse(FormatType.OWL.canDownload());
		assertFalse(FormatType.RDFS.canDownload());
		assertFalse(FormatType.DCAT.canDownload());
		assertTrue(FormatType.RDFA.canDownload());
		assertTrue(FormatType.LINKED_DATA.canDownload());
		assertFalse(FormatType.HDT.canDownload());
		assertTrue(FormatType.NONE.canDownload());
		assertFalse(FormatType.OTHER.canDownload());
	}

	/**
	 * Tests {@link FormatType#isLinkedDataFormat()} method.
	 */
	@Test
	void testIsLinkedDataFormat() {
		assertTrue(FormatType.SPARQL.isLinkedDataFormat());
		assertTrue(FormatType.RDF.isLinkedDataFormat());
		assertTrue(FormatType.TTL.isLinkedDataFormat());
		assertTrue(FormatType.N3.isLinkedDataFormat());
		assertTrue(FormatType.NT.isLinkedDataFormat());
		assertTrue(FormatType.NQ.isLinkedDataFormat());
		assertTrue(FormatType.TRIG.isLinkedDataFormat());
		assertTrue(FormatType.LD_JSON.isLinkedDataFormat());
		assertTrue(FormatType.TRIX.isLinkedDataFormat());
		assertTrue(FormatType.META.isLinkedDataFormat());
		assertTrue(FormatType.VOID.isLinkedDataFormat());
		assertTrue(FormatType.OWL.isLinkedDataFormat());
		assertTrue(FormatType.RDFS.isLinkedDataFormat());
		assertTrue(FormatType.DCAT.isLinkedDataFormat());
		assertTrue(FormatType.RDFA.isLinkedDataFormat());
		assertTrue(FormatType.LINKED_DATA.isLinkedDataFormat());
		assertTrue(FormatType.HDT.isLinkedDataFormat());
		assertTrue(FormatType.NONE.isLinkedDataFormat());
		assertFalse(FormatType.OTHER.isLinkedDataFormat());
	}

	/**
	 * Tests {@link FormatType#getFormat(String)} method.
	 */
	@Test
	void testGetFormat() {

		assertEquals(FormatType.NONE, FormatType.getFormat(null));
		assertEquals(FormatType.NONE, FormatType.getFormat(""));
		assertEquals(FormatType.SPARQL, FormatType.getFormat("sparqlendpoint"));
		assertEquals(FormatType.NT, FormatType.getFormat("ntriples"));
		assertEquals(FormatType.NT, FormatType.getFormat("n-triples"));
		assertEquals(FormatType.TTL, FormatType.getFormat("turtle"));
		assertEquals(FormatType.NQ, FormatType.getFormat("nquads"));
		assertEquals(FormatType.NQ, FormatType.getFormat("n-quads"));
		assertEquals(FormatType.RDF, FormatType.getFormat("application/rdf"));
		assertEquals(FormatType.OTHER, FormatType.getFormat("pdf"));
		assertEquals(FormatType.OTHER, FormatType.getFormat("csv"));
		assertEquals(FormatType.NONE, FormatType.getFormat("fsd"));

		// TODO check more conditions
	}

	/**
	 * Tests {@link FormatType#getFormatByMimeType(String)} method.
	 */
	@Test
	void testGetFormatByContentType() {
		assertEquals(FormatType.NONE, FormatType.getFormatByMimeType(null));
		assertEquals(FormatType.NONE, FormatType.getFormatByMimeType(""));
		assertEquals(FormatType.OTHER, FormatType.getFormatByMimeType("text/html"));
		assertEquals(FormatType.OTHER, FormatType.getFormatByMimeType("text/xml"));
		assertEquals(FormatType.OTHER, FormatType.getFormatByMimeType("application/json"));
		assertEquals(FormatType.NT, FormatType.getFormatByMimeType("application/n-triples"));
		assertEquals(FormatType.TTL, FormatType.getFormatByMimeType("text/turtle"));
		assertEquals(FormatType.RDF, FormatType.getFormatByMimeType("application/rdf+xml"));
		assertEquals(FormatType.NQ, FormatType.getFormatByMimeType("application/n-quads"));
		assertEquals(FormatType.N3, FormatType.getFormatByMimeType("text/n3"));
		assertEquals(FormatType.LD_JSON, FormatType.getFormatByMimeType("application/ld+json"));
		assertEquals(FormatType.TRIG, FormatType.getFormatByMimeType("application/trig"));
		assertEquals(FormatType.TRIX, FormatType.getFormatByMimeType("application/trix"));
		assertEquals(FormatType.NONE, FormatType.getFormatByMimeType("temp"));
	}

}
