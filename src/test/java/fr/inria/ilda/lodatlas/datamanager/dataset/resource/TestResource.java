/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package fr.inria.ilda.lodatlas.datamanager.dataset.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.bson.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.inria.ilda.lodatlas.commons.model.FormatType;
import fr.inria.ilda.lodatlas.datamanager.TestConfig;

/**
 * @author Hande Gözükan
 *
 */
class TestResource {

	String REPO_NAME = "datahub";
	String DATASET_NAME = "bluk-bnb";

	String FILE_NAME = "006893251";

	String ID_SPARQL = "7e3ba852-c9c0-430c-b829-5a68867fcb67";
	String ID_RDF = "a814828a-0ea9-43aa-9628-cd6b999e14cb";
	String ID_NT = "13aaf04d-8920-478a-908a-7b0dd858be51";
	String ID_NON_RDF = "b2fb3e06-54ae-440f-8c34-b6f5100c2546";

	String URL_SPARQL = "http://bnb.data.bl.uk/sparql";
	String URL_RDF = "http://bnb.data.bl.uk/doc/resource/006893251.rdf";
	String URL_NT = "http://bnb.data.bl.uk/doc/resource/006893251";
	String URL_NON_RDF = "http://www.bl.uk/bibliographic/pdfs/bldatamodelcip.pdf";
	String URL_INVALID = "htp:/";

	String SPARQL_RESOURCE_JSON = "{ \"mimetype\" : \"\", \"cache_url\" : \"\", \"hash\" : \"\", \"description\" : \"SPARQL Endpoint\", \"format\" : \"rdf\", \"url\" : \""
			+ URL_SPARQL
			+ "\", \"datastore_active\" : false, \"cache_last_updated\" : null, \"package_id\" : \"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\", \"name\" : \"SPARQL Endpoint\", \"state\" : \"active\", \"mimetype_inner\" : \"\", \"last_modified\" : null, \"position\" : 0, \"revision_id\" : \"f25de347-1693-467c-9712-7d8eba9b0d0b\", \"url_type\" : null, \"id\" : \""
			+ ID_SPARQL + "\", \"resource_type\" : \"file\", \"size\" : null }";

	String RDF_RESOURCE_JSON = "{ \"mimetype\" : \"\", \"cache_url\" : \"\", \"hash\" : \"\", \"description\" : \"Example resource - RDF\", \"format\" : \"example/rdf+xml\", \"url\" : \""
			+ URL_RDF
			+ "\", \"datastore_active\" : false, \"cache_last_updated\" : null, \"package_id\" : \"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\", \"name\" : \"Example resource - RDF\", \"state\" : \"active\", \"mimetype_inner\" : \"\", \"last_modified\" : null, \"position\" : 2, \"revision_id\" : \"f25de347-1693-467c-9712-7d8eba9b0d0b\", \"url_type\" : null, \"id\" : \""
			+ ID_RDF + "\", \"resource_type\" : \"file\", \"size\" : null }";

	String MALFORMED_URL_RESOURCE_JSON = "{ \"mimetype\" : \"\", \"cache_url\" : \"\", \"hash\" : \"\", \"description\" : \"Example resource - RDF\", \"format\" : \"example/rdf+xml\", \"url\" : \""
			+ URL_INVALID
			+ "\", \"datastore_active\" : false, \"cache_last_updated\" : null, \"package_id\" : \"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\", \"name\" : \"Example resource - RDF\", \"state\" : \"active\", \"mimetype_inner\" : \"\", \"last_modified\" : null, \"position\" : 2, \"revision_id\" : \"f25de347-1693-467c-9712-7d8eba9b0d0b\", \"url_type\" : null, \"id\" : \""
			+ ID_RDF + "\", \"resource_type\" : \"file\", \"size\" : null }";

	String NON_RDF_RESOURCE_JSON = "{ \"mimetype\" : null, \"cache_url\" : null, \"hash\" : \"\", \"description\" : \"This is a diagram detailing the data model developed for forthcoming books\", \"name\" : \"Data model (Forthcoming book)\", \"format\" : \"html/pdf\", \"url\" : \""
			+ URL_NON_RDF
			+ "\", \"datastore_active\" : false, \"cache_last_updated\" : null, \"package_id\" : \"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\", \"created\" : \"2017-07-13T11:21:58.575868\", \"state\" : \"active\", \"mimetype_inner\" : null, \"last_modified\" : null, \"position\" : 13, \"revision_id\" : \"33cda69e-a368-4a7f-9b08-0d831a0ec4ff\", \"url_type\" : null, \"id\" : \""
			+ ID_NON_RDF + "\", \"resource_type\" : null, \"size\" : null }";

	Document sparqlDoc;
	Document rdfDoc;
	Document malformedUrlDoc;
	Document nonRdfDoc;

	Path tempPath;

	Resource resourceSparql;
	Resource resourceRdf;
	Resource resourceNt;
	Resource resourceNonRdf;
	Resource resourceMalformedUrl;

	URL urlRdf;
	URL urlSparql;
	URL urlNt;
	URL urlNonRDF;

	@BeforeAll
	void initAll() throws Exception {
		urlRdf = new URL(URL_RDF);
		urlSparql = new URL(URL_SPARQL);
		urlNt = new URL(URL_NT);
		urlNonRDF = new URL(URL_NON_RDF);

		sparqlDoc = Document.parse(SPARQL_RESOURCE_JSON);
		rdfDoc = Document.parse(RDF_RESOURCE_JSON);
		malformedUrlDoc = Document.parse(MALFORMED_URL_RESOURCE_JSON);
		nonRdfDoc = Document.parse(NON_RDF_RESOURCE_JSON);
	}

	@BeforeEach
	void init() throws Exception {
		tempPath = Paths.get(TestConfig.TEMP_PATH);
		FileUtils.forceMkdir(tempPath.toFile());

		// format type is defined as RDF
		resourceSparql = new Resource(REPO_NAME, DATASET_NAME, sparqlDoc);

		resourceRdf = new Resource(REPO_NAME, DATASET_NAME, rdfDoc);

		resourceNt = new Resource(REPO_NAME, DATASET_NAME, Document.parse(
				"{ \"mimetype\" : \"\", \"cache_url\" : \"\", \"hash\" : \"\", \"description\" : \"Example resource RDF - ttl/json/html - default html\", \"format\" : \"example\", \"url\" : \""
						+ URL_NT
						+ "\", \"datastore_active\" : false, \"cache_last_updated\" : null, \"package_id\" : \"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\", \"name\" : \"Example resource RDF - ttl/json/html - default html\", \"state\" : \"active\", \"mimetype_inner\" : \"\", \"last_modified\" : null, \"position\" : 6, \"revision_id\" : \"f25de347-1693-467c-9712-7d8eba9b0d0b\", \"url_type\" : null, \"id\" : \""
						+ ID_NT + "\", \"resource_type\" : \"file\", \"size\" : null }"));

		resourceNonRdf = new Resource(REPO_NAME, DATASET_NAME, nonRdfDoc);

		resourceMalformedUrl = new Resource(REPO_NAME, DATASET_NAME, malformedUrlDoc);
	}

	@AfterEach
	void tearDown() throws Exception {
		resourceSparql = null;
		resourceRdf = null;
		resourceNt = null;
		resourceNonRdf = null;
		resourceMalformedUrl = null;

		FileUtils.deleteDirectory(tempPath.toFile());
	}

	@AfterAll
	void tearDownAll() {
		urlRdf = null;
		urlSparql = null;
		urlNt = null;
		urlNonRDF = null;

		sparqlDoc = null;
		rdfDoc = null;
		malformedUrlDoc = null;
		nonRdfDoc = null;
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#Resource(String, String, Document)}
	 * constructor method.
	 */
	@Test
	void testResource() {
		assertEquals(REPO_NAME, resourceRdf.getRepoName());
		assertEquals(DATASET_NAME, resourceRdf.getDatasetName());
		assertEquals(rdfDoc, resourceRdf.getDoc());
		assertEquals(ID_RDF, resourceRdf.getResourceId());
		assertEquals(urlRdf, resourceRdf.getUrl());
		assertEquals(FormatType.RDF, resourceRdf.getVerifiedFormat());
		assertEquals("", resourceRdf.getContentType());
		assertEquals(0, resourceRdf.getContentLength());
		assertEquals("", resourceRdf.getDownloadError());
		assertNull(resourceRdf.getDumpPath());
		assertFalse(resourceRdf.hasDump());
		assertEquals("", resourceRdf.getDumpFileName());
		assertEquals("", resourceRdf.getDumpFileNameNoX());
		assertFalse(resourceRdf.isCompressed());
		assertTrue(resourceRdf.canDownloadDump());
		assertTrue(resourceRdf.isLinkedData());
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#Resource(String, String, Document)}
	 * constructor method with malformed URL.
	 */
	@Test
	void testResource_MalformedURL() {
		assertEquals(REPO_NAME, resourceMalformedUrl.getRepoName());
		assertEquals(DATASET_NAME, resourceMalformedUrl.getDatasetName());
		assertEquals(malformedUrlDoc, resourceMalformedUrl.getDoc());
		assertEquals(ID_RDF, resourceMalformedUrl.getResourceId());
		assertEquals(null, resourceMalformedUrl.getUrl());
		assertEquals(FormatType.RDF, resourceMalformedUrl.getVerifiedFormat());
		assertEquals("", resourceMalformedUrl.getContentType());
		assertEquals(0, resourceMalformedUrl.getContentLength());
		assertEquals("MalformedURLException for " + URL_INVALID, resourceMalformedUrl.getDownloadError());
		assertNull(resourceMalformedUrl.getDumpPath());
		assertFalse(resourceMalformedUrl.hasDump());
		assertEquals("", resourceMalformedUrl.getDumpFileName());
		assertEquals("", resourceMalformedUrl.getDumpFileNameNoX());
		assertFalse(resourceMalformedUrl.isCompressed());
		assertFalse(resourceMalformedUrl.canDownloadDump());
		assertTrue(resourceMalformedUrl.isLinkedData());
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#Resource(String, String, Document)}
	 * constructor method for a resource for sparql endpoint.
	 */
	@Test
	void testResource_SparqlEndpoint() {
		assertEquals(REPO_NAME, resourceSparql.getRepoName());
		assertEquals(DATASET_NAME, resourceSparql.getDatasetName());
		assertEquals(sparqlDoc, resourceSparql.getDoc());
		assertEquals(ID_SPARQL, resourceSparql.getResourceId());
		assertEquals(urlSparql, resourceSparql.getUrl());
		assertEquals(FormatType.SPARQL, resourceSparql.getVerifiedFormat());
		assertEquals("", resourceSparql.getContentType());
		assertEquals(0, resourceSparql.getContentLength());
		assertEquals("Unsupported format for download: " + FormatType.SPARQL, resourceSparql.getDownloadError());
		assertNull(resourceSparql.getDumpPath());
		assertFalse(resourceSparql.hasDump());
		assertEquals("", resourceSparql.getDumpFileName());
		assertEquals("", resourceSparql.getDumpFileNameNoX());
		assertFalse(resourceSparql.isCompressed());
		assertFalse(resourceSparql.canDownloadDump());
		assertTrue(resourceSparql.isLinkedData());
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#Resource(String, String, Document)}
	 * constructor method for a non RDF resource.
	 */
	@Test
	void testResource_NonRDF() {
		assertEquals(REPO_NAME, resourceNonRdf.getRepoName());
		assertEquals(DATASET_NAME, resourceNonRdf.getDatasetName());
		assertEquals(nonRdfDoc, resourceNonRdf.getDoc());
		assertEquals(ID_NON_RDF, resourceNonRdf.getResourceId());
		assertEquals(urlNonRDF, resourceNonRdf.getUrl());
		assertEquals(FormatType.OTHER, resourceNonRdf.getVerifiedFormat());
		assertEquals("", resourceNonRdf.getContentType());
		assertEquals(0, resourceNonRdf.getContentLength());
		assertEquals("Not a linked data format: " + FormatType.OTHER, resourceNonRdf.getDownloadError());
		assertNull(resourceNonRdf.getDumpPath());
		assertFalse(resourceNonRdf.hasDump());
		assertEquals("", resourceNonRdf.getDumpFileName());
		assertEquals("", resourceNonRdf.getDumpFileNameNoX());
		assertFalse(resourceNonRdf.isCompressed());
		assertFalse(resourceNonRdf.canDownloadDump());
		assertFalse(resourceNonRdf.isLinkedData());
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#Resource(String, String, Document)}
	 * constructor method for <code>null</code> <code>repoName</code> value.
	 */
	@Test
	void testResource_Ex1() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			resourceRdf = new Resource(null, DATASET_NAME, rdfDoc);
		});

		assertEquals("The repository name cannot not be null or empty string.", ex.getMessage());
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#Resource(String, String, Document)}
	 * constructor method for empty string <code>repoName</code> value.
	 */
	@Test
	void testResource_Ex2() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			resourceRdf = new Resource("", DATASET_NAME, rdfDoc);
		});

		assertEquals("The repository name cannot not be null or empty string.", ex.getMessage());
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#Resource(String, String, Document)}
	 * constructor method for null <code>datasetName</code> value.
	 */
	@Test
	void testResource_Ex3() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			resourceRdf = new Resource(REPO_NAME, null, rdfDoc);
		});

		assertEquals("The dataset name cannot not be null or empty string.", ex.getMessage());
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#Resource(String, String, Document)}
	 * constructor method for empty string <code>datasetName</code> value.
	 */
	@Test
	void testResource_Ex4() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			resourceRdf = new Resource(REPO_NAME, "", rdfDoc);
		});

		assertEquals("The dataset name cannot not be null or empty string.", ex.getMessage());
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#Resource(String, String, Document)}
	 * constructor method for <code>null</code> <code>resourceDoc</code> value.
	 */
	@Test
	void testResource_Ex5() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			resourceRdf = new Resource(REPO_NAME, DATASET_NAME, null);
		});

		assertEquals("Resource doc cannot be null.", ex.getMessage());
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#getRepoName()}.
	 */
	@Test
	void testGetRepoName() {
		assertEquals(REPO_NAME, resourceSparql.getRepoName());
		assertEquals(REPO_NAME, resourceRdf.getRepoName());
		assertEquals(REPO_NAME, resourceNt.getRepoName());
		assertEquals(REPO_NAME, resourceNonRdf.getRepoName());
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#getDatasetName()}.
	 */
	@Test
	void testGetDatasetName() {
		assertEquals(DATASET_NAME, resourceSparql.getDatasetName());
		assertEquals(DATASET_NAME, resourceRdf.getDatasetName());
		assertEquals(DATASET_NAME, resourceNt.getDatasetName());
		assertEquals(DATASET_NAME, resourceNonRdf.getDatasetName());
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#getDoc()}.
	 */
	@Test
	void testGetDoc() {
		assertEquals(sparqlDoc, resourceSparql.getDoc());
		assertEquals(rdfDoc, resourceRdf.getDoc());
		assertEquals(nonRdfDoc, resourceNonRdf.getDoc());
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#getResourceId()}.
	 */
	@Test
	void testGetResourceId() {
		assertEquals(ID_SPARQL, resourceSparql.getResourceId());
		assertEquals(ID_RDF, resourceRdf.getResourceId());
		assertEquals(ID_NT, resourceNt.getResourceId());
		assertEquals(ID_NON_RDF, resourceNonRdf.getResourceId());
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#getUrl()}.
	 */
	@Test
	void testGetUrl() {
		assertEquals(urlSparql, resourceSparql.getUrl());
		assertEquals(urlRdf, resourceRdf.getUrl());
		assertEquals(urlNt, resourceNt.getUrl());
		assertEquals(urlNonRDF, resourceNonRdf.getUrl());
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#getVerifiedFormat()}.
	 */
	@Test
	void testGetVerifiedFormat() {
		// format type is defined as RDF, sparql type should be extracted from URL of
		// the resource
		assertEquals(FormatType.SPARQL, resourceSparql.getVerifiedFormat());
		assertEquals(FormatType.RDF, resourceRdf.getVerifiedFormat());
		assertEquals(FormatType.NONE, resourceNt.getVerifiedFormat());
		assertEquals(FormatType.OTHER, resourceNonRdf.getVerifiedFormat());
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#isLinkedData()}.
	 */
	@Test
	void testIsLinkedData() {
		assertTrue(resourceSparql.isLinkedData());
		assertTrue(resourceRdf.isLinkedData());
		assertTrue(resourceNt.isLinkedData());
		assertFalse(resourceNonRdf.isLinkedData());
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#canDownloadDump()}.
	 */
	@Test
	void testCanDownlodDump() {
		assertFalse(resourceSparql.canDownloadDump());
		assertTrue(resourceRdf.canDownloadDump());
		assertTrue(resourceNt.canDownloadDump());
		assertFalse(resourceNonRdf.canDownloadDump());
	}

	// /**
	// * Test method for
	// * {@link
	// fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#getDumpPath()}.
	// */
	// @Test
	// void testGetDumpPath() {
	// resourceRdf.downloadDump(tempPath);
	// assertEquals(Paths.get(tempPath.toString(), FILE_NAME + "." + FORMAT_RDF),
	// resourceRdf.getDumpPath());
	//
	// resourceNt.downloadDump(tempPath);
	// assertEquals(Paths.get(tempPath.toString(), FILE_NAME),
	// resourceNt.getDumpPath());
	// }
	//
	// /**
	// * Test method for
	// * {@link
	// fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#getFileNameNoX()}.
	// */
	// @Test
	// void testGetFileNameNoX() {
	// resourceRdf.downloadDump(tempPath);
	// assertEquals(FILE_NAME, resourceRdf.getFileNameNoX());
	//
	// resourceNt.downloadDump(tempPath);
	// assertEquals(FILE_NAME, resourceNt.getFileNameNoX());
	// }
	//
	// /**
	// * Test method for
	// * {@link
	// fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#hasDump()}.
	// */
	// @Test
	// void testHasDump() {
	// resourceRdf.downloadDump(tempPath);
	// assertTrue(resourceRdf.hasDump());
	//
	// resourceNt.downloadDump(tempPath);
	// assertTrue(resourceNt.hasDump());
	// }
	//
	// /**
	// * Test method for
	// * {@link
	// fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource#loadDump(java.lang.String)}.
	// */
	// @Test
	// void testloadDump() {
	// resourceSparql.downloadDump(tempPath);
	// assertEquals(0, tempPath.toFile().listFiles().length);
	//
	// resourceRdf.downloadDump(tempPath);
	// assertTrue(Files.exists(Paths.get(tempPath.toString(), FILE_NAME + "." +
	// FORMAT_RDF)));
	//
	// resourceNt.downloadDump(tempPath);
	// assertTrue(Files.exists(Paths.get(tempPath.toString(), FILE_NAME)));
	//
	// // TODO check resource values
	//
	// // TODO test with zip files
	//
	// }

}
