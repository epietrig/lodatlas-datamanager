/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump;

import java.io.File;
import java.io.IOException;

import org.bson.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import fr.inria.ilda.lodatlas.datamanager.dataset.resource.IResource;
import fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump.IFileProcessStrategy;
import fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump.ResourceProcessor;

/**
 * @author Hande Gözükan
 *
 */
abstract class TestADumpProcessStrategy {

	ResourceProcessor dumpProcessor;

	IFileProcessStrategy strategy;

	File file;

	Document resourceDoc;

	IResource resource;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void initAll() throws Exception {
//		dumpProcessor = new ResourceProcessor();
	}

	@AfterEach
	void tearDown() throws IOException {
		resource = null;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	void tearDownAll() throws Exception {
		file = null;
		dumpProcessor = null;
		strategy = null;
	}

}
