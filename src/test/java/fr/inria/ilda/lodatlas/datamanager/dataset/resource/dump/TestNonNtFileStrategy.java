/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump;

import java.io.File;
import java.io.IOException;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.bson.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;

import fr.inria.ilda.lodatlas.datamanager.TestConfig;
import fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource;

/**
 * @author Hande Gözükan
 *
 */
class TestNonNtFileStrategy extends TestADumpProcessStrategy {

	String FILE_NAME = "006893251.rdf";

	String REPO_NAME = "datahub";

	String DATASET_NAME = "bluk-bnb";

	MongoCollection<Document> collection;

	MongoCollection<Document> vocabularyCollection;

	Path tempPath;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	@Override
	void initAll() throws Exception {
		super.initAll();

		tempPath = Paths.get(TestConfig.TEMP_PATH).toRealPath(LinkOption.NOFOLLOW_LINKS);

		resourceDoc = Document.parse(
				"{ \"mimetype\" : \"\", \"cache_url\" : \"\", \"hash\" : \"\", \"description\" : \"SPARQL Endpoint\", \"format\" : \"api/sparql\", \"url\" : \"http://bnb.data.bl.uk/sparql\", \"datastore_active\" : false, \"cache_last_updated\" : null, \"package_id\" : \"1010c4fc-421b-4576-a9c2-5b6df7c0ef96\", \"name\" : \"SPARQL Endpoint\", \"state\" : \"active\", \"mimetype_inner\" : \"\", \"last_modified\" : null, \"position\" : 0, \"revision_id\" : \"f25de347-1693-467c-9712-7d8eba9b0d0b\", \"url_type\" : null, \"id\" : \"7e3ba852-c9c0-430c-b829-5a68867fcb67\", \"resource_type\" : \"file\", \"size\" : null }");

		// collection =
		// MongoConnectorUtility.getCollection(TestConfig.DATAHUB_COLLECTION);
		// vocabularyCollection =
		// MongoConnectorUtility.getCollection(TestConfig.VOCABULARY_COLLECTION);

		strategy = new NonNtFileStrategy(dumpProcessor);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void init() throws Exception {
		FileUtils.copyFile(Paths.get(TestConfig.TEST_FILES_PATH, FILE_NAME).toFile(),
				Paths.get(TestConfig.TEMP_PATH, FILE_NAME).toFile());

		file = new File(tempPath.toFile(), FILE_NAME);

		resource = new Resource(REPO_NAME, DATASET_NAME, resourceDoc);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	@Override
	void tearDown() throws IOException {
		super.tearDown();

		FileUtils.cleanDirectory(tempPath.toFile());
		file = null;

		// clean the database after each test run
		collection.deleteMany(new BasicDBObject());

		// clean the database after each test run
		vocabularyCollection.deleteMany(new BasicDBObject());
	}

	@Override
	@AfterAll
	void tearDownAll() throws Exception {
		super.tearDownAll();

		tempPath = null;

		collection = null;
		vocabularyCollection = null;

		// MongoConnectorUtility.dispose();
	}

	@Test
	void test() {
		// strategy.process(file);
	}

}
