/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.bson.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.commons.strings.ProcessedFileStrings;
import fr.inria.ilda.lodatlas.datamanager.TestConfig;
import fr.inria.ilda.lodatlas.datamanager.dataset.resource.IResource;
import fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource;

/**
 * @author Hande Gözükan
 *
 */
class TestNtFileStrategy extends TestADumpProcessStrategy {

	String FILE_NAME = "AATOut_Contribs.nt";

	String REPO_NAME = "datahub";

	String DATASET_NAME = "getty-aat";

	String FORMAT = "nt";

	MongoCollection<Document> collection;

	MongoCollection<Document> vocabularyCollection;

	Path tempPath;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	@Override
	void initAll() throws Exception {
		// super.initAll();
		//
		// tempPath =
		// Paths.get(TestConfig.TEMP_PATH).toRealPath(LinkOption.NOFOLLOW_LINKS);
		//
		// resourceDoc = Document.parse(
		// "{\"mimetype\": null, \"cache_url\": null, \"hash\": \"\", \"description\":
		// \"NTriples zip, file size is 133.5 Mb\", \"name\": \"Total AAT statements\",
		// \"format\": \"application/n-triples, application/zip\", \"url\":
		// \"http://vocab.getty.edu/dataset/aat/full.zip\", \"datastore_active\": false,
		// \"cache_last_updated\": null, \"package_id\":
		// \"d38a09aa-faa3-42e9-a1bd-1c9fcb647ea4\", \"created\":
		// \"2014-10-09T17:45:05.078672\", \"state\": \"active\", \"mimetype_inner\":
		// null, \"last_modified\": null, \"position\": 8, \"revision_id\":
		// \"a52bd634-1e8d-4a55-94f9-617637aedc5b\", \"url_type\": null, \"id\":
		// \"2d4a6dc8-2d69-4e2c-84ec-b9e55e042006\", \"resource_type\": \"file\",
		// \"size\": null}");
		//
		// collection =
		// MongoConnectorUtility.getCollection(TestConfig.DATAHUB_COLLECTION);
		// vocabularyCollection =
		// MongoConnectorUtility.getCollection(TestConfig.VOCABULARY_COLLECTION);
		//
		// strategy = new NtFileStrategy(dumpProcessor);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void init() throws Exception {
		FileUtils.copyFile(Paths.get(TestConfig.TEST_FILES_PATH, FILE_NAME).toFile(),
				Paths.get(TestConfig.TEMP_PATH, FILE_NAME).toFile());

		file = new File(tempPath.toFile(), FILE_NAME);

		resource = new Resource(REPO_NAME, DATASET_NAME, resourceDoc);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	@Override
	void tearDown() throws IOException {
		super.tearDown();

		FileUtils.cleanDirectory(tempPath.toFile());
		file = null;

		// clean the database after each test run
		collection.deleteMany(new BasicDBObject());

		// clean the database after each test run
		vocabularyCollection.deleteMany(new BasicDBObject());
	}

	@Override
	@AfterAll
	void tearDownAll() throws Exception {
		super.tearDownAll();

		tempPath = null;

		collection = null;
		vocabularyCollection = null;

		// MongoConnectorUtility.dispose();
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump.NtFileStrategy#processFileWithLodstats(IResource, File)}.
	 */
	@Test
	void testProcessFileWithLodstats() {
		String entry = ((NtFileStrategy) strategy).processFileWithLodstats(resource, file, FORMAT);
		entry = entry.replaceAll("False", "false");
		entry = entry.replaceAll("True", "true");

		Document lodstatsDoc = Document.parse(entry);

		assertAll("lodstatsDoc", () -> {
			assertAll("singlevalues",
					() -> assertEquals("AATOut_Contribs.nt", lodstatsDoc.get(ProcessedFileStrings.FILE_NAME)),
					() -> assertEquals(1576, lodstatsDoc.get(DatasetStrings.TRIPLE_COUNT)),
					() -> assertEquals(false, lodstatsDoc.get(ProcessedFileStrings.HAS_ERROR)),
					() -> assertEquals("", lodstatsDoc.get(ProcessedFileStrings.ERROR)));
		}, () -> {
			List<String> vocabularies = (List<String>) lodstatsDoc.get(ProcessedFileStrings.VOCABULARIES);

			assertAll("vocabularies",
					() -> assertEquals(true, vocabularies.contains("http://www.w3.org/2000/01/rdf-schema")),
					() -> assertEquals(true, vocabularies.contains("http://rdfs.org/ns/void")),
					() -> assertEquals(true, vocabularies.contains("http://www.w3.org/1999/02/22-rdf-syntax-ns")),
					() -> assertEquals(true, vocabularies.contains("http://xmlns.com/foaf/0.1/")),
					() -> assertEquals(true, vocabularies.contains("http://creativecommons.org/ns")),
					() -> assertEquals(true, vocabularies.contains("http://purl.org/dc/elements/1.1/")),
					() -> assertEquals(true, vocabularies.contains("http://purl.org/dc/terms/")),
					() -> assertEquals(7, vocabularies.size()));

		}, () -> {
			List<String> usedClasses = (List<String>) lodstatsDoc.get(ProcessedFileStrings.USED_CLASSES);

			assertAll("usedClasses", () -> assertEquals(true, usedClasses.contains("http://xmlns.com/foaf/0.1/Agent")),
					() -> assertEquals(1, usedClasses.size()));
		}, () -> {
			List<String> propertyUsage = (List<String>) lodstatsDoc.get(ProcessedFileStrings.USED_PROPERTIES);

			assertAll("propertyUsage",
					() -> assertEquals(true, propertyUsage.contains("http://www.w3.org/2000/01/rdf-schema#seeAlso")),
					() -> assertEquals(true, propertyUsage.contains("http://xmlns.com/foaf/0.1/nick")),
					() -> assertEquals(true, propertyUsage.contains("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")),
					() -> assertEquals(true, propertyUsage.contains("http://purl.org/dc/elements/1.1/identifier")),
					() -> assertEquals(true, propertyUsage.contains("http://purl.org/dc/terms/license")),
					() -> assertEquals(true, propertyUsage.contains("http://rdfs.org/ns/void#inDataset")),
					() -> assertEquals(true, propertyUsage.contains("http://creativecommons.org/ns#license")),
					() -> assertEquals(true, propertyUsage.contains("http://xmlns.com/foaf/0.1/name")),
					() -> assertEquals(8, propertyUsage.size()));
		});
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump.NtFileStrategy#processFileWithRdfSums(IResource, File)}.
	 */
	@Test
	void testProcessFileWithRdfSums() {
		String entry = ((NtFileStrategy) strategy).processFileWithRdfSums(file);
		assertEquals("{'fileName': '" + (file.getName() + "_rdfsum.nt") + "','hasError': false, 'error': ''}", entry);

		File rdfSumFile = new File(tempPath.toFile(), file.getName() + "_rdfsum.nt");
		assertEquals(true, rdfSumFile.exists());
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump.NtFileStrategy#process(File)}.
	 */
	@Test
	void testProcess() {
		// strategy.process(file);
	}

}
