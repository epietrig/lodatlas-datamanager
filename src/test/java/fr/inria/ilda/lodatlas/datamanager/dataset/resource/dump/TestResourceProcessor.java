package fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.bson.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.inria.ilda.lodatlas.commons.model.FormatType;
import fr.inria.ilda.lodatlas.datamanager.TestConfig;
import fr.inria.ilda.lodatlas.datamanager.dataset.resource.IResource;
import fr.inria.ilda.lodatlas.datamanager.dataset.resource.Resource;

/**
 * A test class to test {@link ResourceProcessor} class.
 * 
 * @see ResourceProcessor
 * 
 * @author Hande Gözükan
 *
 */
class TestResourceProcessor {

	String REPO_NAME = "datahub";

	String HTTP_DATASET_NAME = "comune-pisa-bandi-gara";
	String HTTP_RESOURCE_JSON = "{\"id\":\"c0d29d71-2e9f-41ae-aef0-4e5760980696\", \"format\":\"rdf\",\"url\":\"http://www.comune.pisa.it/it/lod/rdfDataModel-bandi-lavori-pubblici.rdf\"}";

	String FTP_DATASET_NAME = "data-bnf-fr";
	String FTP_RESOURCE_JSON = "{\"id\":\"cdaaef2b-9258-4800-ac3c-8d5ddd513b12\", \"format\":\"rdf\",\"url\":\"ftp://databnf:databnf@pef.bnf.fr/DATA/databnf_all_rdf_xml.tar.gz\"}";

	Document httpDoc;
	Document ftpDoc;

	IResource resourceHttp;
	IResource resourceFtp;

	Path tmpDir;

	Path httpDatasetPath;
	Path ftpDatasetPath;

	ResourceProcessor resourceProcessor;

	@BeforeAll
	void initAll() throws Exception {
		httpDoc = Document.parse(HTTP_RESOURCE_JSON);
		resourceHttp = new Resource(REPO_NAME, HTTP_DATASET_NAME, httpDoc);

		ftpDoc = Document.parse(FTP_RESOURCE_JSON);
		resourceFtp = new Resource(REPO_NAME, FTP_DATASET_NAME, ftpDoc);

		tmpDir = Paths.get(TestConfig.TEMP_PATH);
		tmpDir.toFile().mkdirs();

		httpDatasetPath = Paths.get(tmpDir.toString(), HTTP_DATASET_NAME);
		httpDatasetPath.toFile().mkdirs();

		ftpDatasetPath = Paths.get(tmpDir.toString(), FTP_DATASET_NAME);
		ftpDatasetPath.toFile().mkdirs();

		resourceProcessor = new ResourceProcessor(null, null);
	}

	@BeforeEach
	void init() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@AfterAll
	void tearDownAll() throws Exception {
		httpDoc = null;
		resourceHttp = null;

		ftpDoc = null;
		resourceFtp = null;

		FileUtils.cleanDirectory(tmpDir.toFile());

		resourceProcessor = null;
	}

	/**
	 * Tests {@link ResourceProcessor#downloadDump(Path, IResource)} method for an
	 * HTTP resource which is not compressed and which does not exceed download
	 * size.
	 */
	@Test
	void testDownloadDump_HTTP() {
		resourceProcessor.downloadDump(httpDatasetPath, resourceHttp);

		// check state of resource after download
		assertEquals(FormatType.RDF, resourceHttp.getVerifiedFormat());
		assertEquals(196561, resourceHttp.getContentLength());
		assertEquals("application/rdf+xml", resourceHttp.getContentType());
		assertFalse(resourceHttp.isCompressed());
		assertEquals(TestConfig.TEMP_PATH
				+ "/comune-pisa-bandi-gara/c0d29d71-2e9f-41ae-aef0-4e5760980696/rdfDataModel-bandi-lavori-pubblici.rdf",
				resourceHttp.getDumpPath().toString());
		assertEquals("rdfDataModel-bandi-lavori-pubblici.rdf", resourceHttp.getDumpFileName());
		assertEquals("rdfDataModel-bandi-lavori-pubblici", resourceHttp.getDumpFileNameNoX());
		assertEquals("", resourceHttp.getDownloadError());

		// check if the file is downloaded
		assertTrue(Paths.get(httpDatasetPath.toString(), "c0d29d71-2e9f-41ae-aef0-4e5760980696",
				"rdfDataModel-bandi-lavori-pubblici.rdf").toFile().exists());
	}

	/**
	 * Tests {@link ResourceProcessor#downloadDump(Path, IResource)} method for an
	 * FTP resource which is compressed and which does not exceed download size.
	 */
	@Test
	void testDownloadDump_FTP() {
		resourceProcessor.downloadDump(ftpDatasetPath, resourceFtp);

		// check state of resource after download
		assertEquals(FormatType.RDF, resourceFtp.getVerifiedFormat());
		// assertEquals(196561, resourceFtp.getContentLength());
		assertEquals("application/rdf+xml", resourceFtp.getContentType());
		assertTrue(resourceFtp.isCompressed());
		assertEquals(
				TestConfig.TEMP_PATH + "/data-bnf-fr/c0d29d71-2e9f-41ae-aef0-4e5760980696/databnf_all_rdf_xml.tar.gz",
				resourceFtp.getDumpPath().toString());
		assertEquals("databnf_all_rdf_xml.tar.gz", resourceFtp.getDumpFileName());
		assertEquals("databnf_all_rdf_xml", resourceFtp.getDumpFileNameNoX());
		assertEquals("", resourceFtp.getDownloadError());

		// check if the file is downloaded
		assertTrue(Paths
				.get(ftpDatasetPath.toString(), "c0d29d71-2e9f-41ae-aef0-4e5760980696", "databnf_all_rdf_xml.tar.gzs")
				.toFile().exists());

		System.out.println(resourceFtp.toString());
	}

	// TODO add tests for the following cases
	// 404 error
	// redirects
	// content length > limits for compressed and uncompressed
	// format rar
	// with download errors
	// already downloaded

}
