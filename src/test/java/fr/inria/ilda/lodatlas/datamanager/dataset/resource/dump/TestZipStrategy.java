/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.resource.dump;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.inria.ilda.lodatlas.datamanager.TestConfig;

/**
 * 
 * @author Hande Gözükan
 *
 */
class TestZipStrategy extends TestADumpProcessStrategy {

	static final String FILE_NAME_SINGLE = "test_single.zip";

	static final String FILE_NAME_SINGLE_SUB1 = "test_single1.xml";

	static final String FILE_NAME_MULTIPLE = "test_multiple.zip";
	static final String FILE_NAME_MULTIPLE_SUB1 = "test_multiple1.rdf";
	static final String FILE_NAME_MULTIPLE_SUB2 = "test_multiple2.ttl";
	static final String FILE_NAME_MULTIPLE_SUB3 = "test_multiple3.ttl";
	static final String FILE_NAME_MULTIPLE_SUB4 = "test_multiple4.ttl";

	static final String FILE_NAME_AVEC_SUBDIR = "test_subdir.zip";
	static final String FILE_NAME_AVEC_SUBDIR_DIR1 = "test_subdir";

	File fileMultiple;
	File fileAvecSubDir;

	@BeforeEach
	void setUp() throws Exception {
		file = new File(new File(TestConfig.TEST_FILES_PATH), FILE_NAME_SINGLE);
		fileMultiple = new File(new File(TestConfig.TEST_FILES_PATH), FILE_NAME_MULTIPLE);
		fileAvecSubDir = new File(new File(TestConfig.TEST_FILES_PATH), FILE_NAME_AVEC_SUBDIR);

//		dumpProcessor.setStrategy(new ZipStrategy(dumpProcessor));
	}

	@AfterEach
	void tearDown() {
		fileMultiple = null;
		fileAvecSubDir = null;

		FileUtils.deleteQuietly(new File(new File(TestConfig.TEST_FILES_PATH), FILE_NAME_SINGLE_SUB1));
		FileUtils.deleteQuietly(new File(new File(TestConfig.TEST_FILES_PATH), FILE_NAME_MULTIPLE_SUB1));
		FileUtils.deleteQuietly(new File(new File(TestConfig.TEST_FILES_PATH), FILE_NAME_MULTIPLE_SUB2));
		FileUtils.deleteQuietly(new File(new File(TestConfig.TEST_FILES_PATH), FILE_NAME_MULTIPLE_SUB3));
		FileUtils.deleteQuietly(new File(new File(TestConfig.TEST_FILES_PATH), FILE_NAME_AVEC_SUBDIR_DIR1));

//		FileUtils.deleteDirectory(new File(new File(TestConfig.TEST_FILES_PATH), FILE_NAME_AVEC_SUBDIR_DIR1));
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.pkg.resource.dump.ZipExtractStrategy#process(File)}
	 * for a zip file containing single compressed file.
	 */
	@Test
	void testExtract() {
//		List<IDump> dumps = dumpProcessor.process(null, file);
//		assertEquals(1, dumps.size());
//		assertEquals(file.getParentFile().getAbsolutePath() + "/" + FILE_NAME_SINGLE_SUB1, dumps.get(0).getDumpPath());
//		assertTrue(Files.exists(Paths.get(file.getParentFile().getAbsolutePath(), FILE_NAME_SINGLE_SUB1)));
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.pkg.resource.dump.ZipExtractStrategy#process(File)}
	 * for a zip file containing multiple compressed files.
	 */
	@Test
	void testExtract_Multiple() {
//		List<IDump> dumps = dumpProcessor.process(null, fileMultiple);
//		assertEquals(4, dumps.size());
//		assertEquals(fileMultiple.getParentFile().getAbsolutePath() + "/" + FILE_NAME_MULTIPLE_SUB1,
//				dumps.get(0).getDumpPath());
//		assertTrue(Files.exists(Paths.get(file.getParentFile().getAbsolutePath(), FILE_NAME_MULTIPLE_SUB1)));
//		assertEquals(fileMultiple.getParentFile().getAbsolutePath() + "/" + FILE_NAME_MULTIPLE_SUB2,
//				dumps.get(1).getDumpPath());
//		assertTrue(Files.exists(Paths.get(file.getParentFile().getAbsolutePath(), FILE_NAME_MULTIPLE_SUB2)));
//		assertEquals(fileMultiple.getParentFile().getAbsolutePath() + "/" + FILE_NAME_MULTIPLE_SUB3,
//				dumps.get(2).getDumpPath());
//		assertTrue(Files.exists(Paths.get(file.getParentFile().getAbsolutePath(), FILE_NAME_MULTIPLE_SUB3)));
//		assertEquals(fileMultiple.getParentFile().getAbsolutePath() + "/" + FILE_NAME_MULTIPLE_SUB4,
//				dumps.get(3).getDumpPath());
//		assertTrue(Files.exists(Paths.get(file.getParentFile().getAbsolutePath(), FILE_NAME_MULTIPLE_SUB4)));
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.pkg.resource.dump.ZipExtractStrategy#process(File)}
	 * for a zip file containing multiple compressed directories and files.
	 */
	@Test
	void testExtract_AvecSubDir() {
//		List<IDump> dumps = dumpProcessor.process(null, fileAvecSubDir);
//		assertEquals(14, dumps.size());
//		assertEquals(fileMultiple.getParentFile().getAbsolutePath() + "/" + FILE_NAME_MULTIPLE_SUB1,
//				dumps.get(0).getDumpPath());
//		assertTrue(Files.exists(Paths.get(file.getParentFile().getAbsolutePath(), FILE_NAME_MULTIPLE_SUB1)));
//		assertEquals(fileMultiple.getParentFile().getAbsolutePath() + "/" + FILE_NAME_MULTIPLE_SUB2,
//				dumps.get(1).getDumpPath());
//		assertTrue(Files.exists(Paths.get(file.getParentFile().getAbsolutePath(), FILE_NAME_MULTIPLE_SUB2)));
//		assertEquals(fileMultiple.getParentFile().getAbsolutePath() + "/" + FILE_NAME_MULTIPLE_SUB3,
//				dumps.get(2).getDumpPath());
//		assertTrue(Files.exists(Paths.get(file.getParentFile().getAbsolutePath(), FILE_NAME_MULTIPLE_SUB3)));
//		assertEquals(fileMultiple.getParentFile().getAbsolutePath() + "/" + FILE_NAME_MULTIPLE_SUB4,
//				dumps.get(3).getDumpPath());
//		assertTrue(Files.exists(Paths.get(file.getParentFile().getAbsolutePath(), FILE_NAME_MULTIPLE_SUB4)));
	}

	/**
	 * Test method for
	 * {@link fr.inria.ilda.lodatlas.datamanager.pkg.resource.dump.ZipExtractionStrategy#process(File)}.
	 */
	@Test
	void testExtract_nullFile() {
//		List<IDump> dumps = dumpProcessor.process(null, null);
//		assertEquals(0, dumps.size());
	}

}
