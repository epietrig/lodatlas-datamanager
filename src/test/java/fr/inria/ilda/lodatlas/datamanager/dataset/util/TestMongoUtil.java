/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.datamanager.dataset.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.bson.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.UpdateResult;

import fr.inria.ilda.lodatlas.commons.model.Label;
import fr.inria.ilda.lodatlas.commons.model.Vocabulary;
import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.commons.strings.ResourceStrings;
import fr.inria.ilda.lodatlas.commons.strings.VocabularyStrings;
import fr.inria.ilda.lodatlas.datamanager.dataset.IDataset;

/**
 * A test class to test {@link MongoUtil} class.
 * 
 * @author Hande Gözükan
 *
 */
class TestMongoUtil {

	String HOST = "localhost";
	int PORT = 27017;
	String DB_NAME = "lodatlas_test";

	String TEST_DATASET_NAME = "test";
	String TEST_DATASET_NAME2 = "test2";
	String TEST_DOC_NON_EXISTING = "doesnotexist";

	String TEST_RESOURCE_ID = "b1662e53-67fb-4ceb-bde3-8d6d08bcae4c";

	Document testDocument;
	Document testDocument2;
	MongoUtil mongoUtil;
	MongoClient mongoClient;
	MongoDatabase mongoDB;

	String TEST_VOCAB_URI = "http://iflastandards.info/ns/isbd/elements/";

	Document testVocabularyDoc;
	Vocabulary testVocabulary;

	List<String> propertyList;
	List<String> classList;

	@BeforeAll
	void initAll() throws Exception {
		mongoUtil = new MongoUtil(HOST, PORT, DB_NAME);

		mongoClient = new MongoClient(HOST, PORT);
		mongoClient.dropDatabase(DB_NAME);

		testDocument = Document.parse("{\"name\": \"" + TEST_DATASET_NAME + "\", \"" + DatasetStrings.RESOURCES
				+ "\": [{\"id\" : \"" + TEST_RESOURCE_ID + "\"},{\"id\" : \"6bfd8464-ea6d-4e54-a9e2-50ae57eaaddf\"}]}");
		testDocument2 = Document.parse("{\"name\": \"" + TEST_DATASET_NAME2 + "\", \"" + DatasetStrings.RESOURCES
				+ "\": [{\"id\" : \"b1662e53-67fb-4ceb-bde3-8d6d08bcae4c\"},{\"id\" : \"6bfd8464-ea6d-4e54-a9e2-50ae57eaaddf\"}]}");

		testVocabularyDoc = Document.parse("{\"uri\" : \"" + TEST_VOCAB_URI + "\", \"classes\" : [{"
				+ "	\"uri\" : \"http://iflastandards.info/ns/isbd/elements/P1122\","
				+ "	\"label\" : \"has bibliographic format of older monographic resource\"" + "},"
				+ "	{ \"uri\" : \"http://iflastandards.info/ns/isbd/elements/P1160\","
				+ "	\"label\" : \"has edition area\"" + "		} ], \"properties\" : [{"
				+ "	\"uri\" : \"http://iflastandards.info/ns/isbd/elements/P1023\","
				+ "	\"label\" : \"has other physical details\"" + "		}, {"
				+ "	\"uri\" : \"http://iflastandards.info/ns/isbd/elements/P1061\","
				+ "	\"label\" : \"has reduction ratio\"" + "		} ],"
				+ "	\"title\" : \"ISBD elements\", \"prefix\" : \"http://lov.okfn.org/dataset/lov/vocabs/isbd\","
				+ "	\"homepage\" : \"" + TEST_VOCAB_URI + "\","
				+ "	\"description\" : \"This is a registration of classes and properties from International Standard Bibliographic Description (ISBD), consolidated edition, published by De Gruyter Saur in July 2011 (ISBN 978-3-11-026379-4).\""
				+ "}" + "");

		testVocabulary = new Vocabulary(TEST_VOCAB_URI, "ISBD elements", "http://lov.okfn.org/dataset/lov/vocabs/isbd",
				TEST_VOCAB_URI,
				"This is a registration of classes and properties from International Standard Bibliographic Description (ISBD), consolidated edition, published by De Gruyter Saur in July 2011 (ISBN 978-3-11-026379-4).",
				"");
		testVocabulary.addClass(new Label("http://iflastandards.info/ns/isbd/elements/P1122",
				"has bibliographic format of older monographic resource"));
		testVocabulary.addClass(new Label("http://iflastandards.info/ns/isbd/elements/P1160", "has edition area"));
		testVocabulary.addProperty(
				new Label("http://iflastandards.info/ns/isbd/elements/P1023", "has other physical details"));
		testVocabulary
				.addProperty(new Label("http://iflastandards.info/ns/isbd/elements/P1061", "has reduction ratio"));

		propertyList = Arrays.asList(new String[] { "http://iflastandards.info/ns/isbd/elements/P1061",
				"http://iflastandards.info/ns/isbd/elements/P1023" });

		classList = Arrays.asList(new String[] { "http://iflastandards.info/ns/isbd/elements/P1122",
				"http://iflastandards.info/ns/isbd/elements/P1160" });
	}

	@AfterAll
	void tearDownAll() throws Exception {
		mongoUtil.dispose();
		mongoUtil = null;

		mongoDB = null;
		mongoClient.close();
		mongoClient = null;

		testDocument = null;
		testDocument2 = null;

		testVocabularyDoc = null;
		testVocabulary = null;

		propertyList = null;
		classList = null;
	}

	@BeforeEach
	void init() throws Exception {
		mongoDB = mongoClient.getDatabase(DB_NAME);
	}

	@AfterEach
	void tearDown() throws Exception {
		mongoDB.drop();
	}

	/**
	 * Tests {@link MongoUtil#MongoUtil(String, int, String)} method.
	 */
	@Test
	void testConstructor() {
		assertNotNull(new MongoUtil(HOST, PORT, DB_NAME));
	}

	/**
	 * Tests {@link MongoUtil#MongoUtil(String, int, String)} method for
	 * <code>null</code> <code>host</code> value.
	 */
	@Test
	void testConstructor_Ex1() {
		// test for null host
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			new MongoUtil(null, PORT, DB_NAME);
		});
		assertEquals("Please specify a valid host.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#MongoUtil(String, int, String)} method for empty
	 * string <code>host</code> value.
	 */
	@Test
	void testConstructor_Ex2() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			new MongoUtil("", PORT, DB_NAME);
		});
		assertEquals("Please specify a valid host.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#MongoUtil(String, int, String)} method for
	 * <code>port</code> value less than 0.
	 */
	@Test
	void testConstructor_Ex3() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			new MongoUtil(HOST, -1, DB_NAME);
		});
		assertEquals("Please specify a valid port.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#MongoUtil(String, int, String)} method for
	 * <code>null</code> <code>dbName</code> value.
	 */
	@Test
	void testConstructor_Ex4() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			new MongoUtil(HOST, PORT, null);
		});
		assertEquals("Please specify a valid database name.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#MongoUtil(String, int, String)} method for empty
	 * string <code>dbName</code> value.
	 */
	@Test
	void testConstructor_Ex5() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			new MongoUtil(HOST, PORT, "");
		});
		assertEquals("Please specify a valid database name.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#findDataset(String)} method.
	 */
	@Test
	void testFindDataset1() {
		// insert dataset to search
		mongoDB.getCollection(Config.DATASET_COLLECTION).insertOne(testDocument);
		assertEquals(1, mongoDB.getCollection(Config.DATASET_COLLECTION).count());

		// search for inserted dataset
		Document testDoc = mongoUtil.findDataset(TEST_DATASET_NAME);
		assertNotNull(testDoc);
		assertEquals(TEST_DATASET_NAME, testDoc.getString(DatasetStrings.NAME));
	}

	/**
	 * Tests {@link MongoUtil#findDataset(String)} method for non-existing dataset.
	 */
	@Test
	void testFindDataset2() {
		assertNull(mongoUtil.findDataset(TEST_DOC_NON_EXISTING));
	}

	/**
	 * Tests {@link MongoUtil#findDataset(String)} method for <code>null</code>
	 * <code>datasetName</code> value.
	 */
	@Test
	void testFindDataset_Ex1() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.findDataset(null);
		});
		assertEquals("Dataset name cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#findDataset(String)} method for empty string
	 * <code>datasetName</code> value.
	 */
	@Test
	void testFindDataset_Ex2() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.findDataset("");
		});
		assertEquals("Dataset name cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#insertDataset(Document)} method.
	 */
	@Test
	void testInsertDataset() {
		// assert that no document exists in the database
		assertEquals(0, mongoDB.getCollection(Config.DATASET_COLLECTION).count());

		// insert document
		mongoUtil.insertDataset(testDocument);
		// assert that the database contains exactly one document
		assertEquals(1, mongoDB.getCollection(Config.DATASET_COLLECTION).count());

		// assert that the document matches the inserted document
		assertNotNull(mongoDB.getCollection(Config.DATASET_COLLECTION)
				.find(new Document(DatasetStrings.NAME, TEST_DATASET_NAME)));
	}

	/**
	 * Tests {@link MongoUtil#insertDataset(Document)} method for <code>null</code>
	 * <code>dataset</code> document value.
	 */
	@Test
	void testInsertDataset_Ex1() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.insertDataset(null);
		});
		assertEquals("Cannot insert null dataset to the database.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#updateDataset(Document, Document)} method when there
	 * is a document in the database that matches the specified <code>filter</code>.
	 */
	@Test
	void testUpdateDataset1() {
		// test for the case where no matching document exists in the database
		String tempDatasetName = "temp";

		// insert dataset to search
		mongoDB.getCollection(Config.DATASET_COLLECTION).insertOne(testDocument);

		// test for the case where there is a matching document in database
		mongoUtil.updateDataset(new Document(DatasetStrings.NAME, TEST_DATASET_NAME),
				new Document("$set", new Document(DatasetStrings.NAME, tempDatasetName)));
		// assert that a document with tempDatasetName exists in the database
		assertNotNull(mongoDB.getCollection(Config.DATASET_COLLECTION)
				.find(new Document(DatasetStrings.NAME, tempDatasetName)).first());

		// assert that the database contains exactly one document
		assertEquals(1, mongoDB.getCollection(Config.DATASET_COLLECTION).count());

		// assert that there is no document with TEST_DOC_NAME
		assertNotNull(mongoDB.getCollection(Config.DATASET_COLLECTION)
				.find(new Document(DatasetStrings.NAME, TEST_DATASET_NAME)));
	}

	/**
	 * Tests {@link MongoUtil#updateDataset(Document, Document)} method when no
	 * document in the database that matches the specified <code>filter.</code>.
	 */
	@Test
	void testUpdateDataset2() {
		// test for the case where no matching document exists in the database
		String tempDatasetName = "temp";

		assertNull(mongoUtil.updateDataset(new Document(DatasetStrings.NAME, TEST_DATASET_NAME),
				new Document("$set", new Document(DatasetStrings.NAME, tempDatasetName))));
	}

	/**
	 * Tests {@link MongoUtil#updateDataset(Document, Document)} method for
	 * <code>null</code> <code>filter</code> value.
	 */
	@Test
	void testUpdateDataset_Ex1() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.updateDataset(null, new Document("$set", new Document(DatasetStrings.NAME, TEST_DATASET_NAME)));
		});
		assertEquals("Filter and update cannot be null.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#updateDataset(Document, Document)} method for
	 * <code>null</code> <code>update</code> value.
	 */
	@Test
	void testUpdateDataset_Ex2() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.updateDataset(new Document(DatasetStrings.NAME, TEST_DATASET_NAME), null);
		});
		assertEquals("Filter and update cannot be null.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#replaceDataset(Document, Document)} method when there
	 * is a document in the database that matches the specified <code>filter</code>.
	 */
	@Test
	void testReplaceDataset1() {
		String tempDatasetName = "temp";

		// insert dataset to search
		mongoDB.getCollection(Config.DATASET_COLLECTION).insertOne(testDocument);

		// replace document
		UpdateResult result = mongoUtil.replaceDataset(new Document(DatasetStrings.NAME, TEST_DATASET_NAME),
				new Document(DatasetStrings.NAME, tempDatasetName));
		assertEquals(1, result.getMatchedCount());
		assertEquals(1, result.getModifiedCount());

		// assert that a document with tempDatasetName exists in the database
		assertNotNull(mongoDB.getCollection(Config.DATASET_COLLECTION)
				.find(new Document(DatasetStrings.NAME, tempDatasetName)).first());

		// assert that the database contains exactly one document
		assertEquals(1, mongoDB.getCollection(Config.DATASET_COLLECTION).count());

		// assert that there is no document with TEST_DOC_NAME
		assertNotNull(mongoDB.getCollection(Config.DATASET_COLLECTION)
				.find(new Document(DatasetStrings.NAME, TEST_DATASET_NAME)));
	}

	/**
	 * Tests {@link MongoUtil#replaceDataset(Document, Document)} method when there
	 * is no document in the database that matches the specified
	 * <code>filter</code>.
	 */
	@Test
	void testReplaceDataset2() {
		String tempDatasetName = "temp";

		UpdateResult result = mongoUtil.replaceDataset(new Document(DatasetStrings.NAME, TEST_DATASET_NAME),
				new Document(DatasetStrings.NAME, tempDatasetName));

		assertEquals(0, result.getMatchedCount());
		assertEquals(0, result.getModifiedCount());
	}

	/**
	 * Tests {@link MongoUtil#replaceDataset(Document, Document)} method for
	 * <code>null</code> <code>filter</code> value.
	 */
	@Test
	void testReplaceDataset_Ex1() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.replaceDataset(null, new Document(DatasetStrings.NAME, TEST_DATASET_NAME));
		});
		assertEquals("Filter and replacement document cannot be null.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#replaceDataset(Document, Document)} method for
	 * <code>null</code> <code>update</code> value.
	 */
	@Test
	void testReplaceDataset_Ex2() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.replaceDataset(new Document(DatasetStrings.NAME, TEST_DATASET_NAME), null);
		});
		assertEquals("Filter and replacement document cannot be null.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#getResourcesForDataset(String)} method when dataset
	 * specified with <code>datasetName</code> exists in the database.
	 */
	@Test
	void testGetResourcesForDataset1() {
		// insert dataset to search
		mongoDB.getCollection(Config.DATASET_COLLECTION).insertOne(testDocument);
		assertEquals(1, mongoDB.getCollection(Config.DATASET_COLLECTION).count());

		List<Document> resources = mongoUtil.getResourcesForDataset(TEST_DATASET_NAME);
		assertNotNull(resources);
		assertEquals(2, resources.size());
	}

	/**
	 * Tests {@link MongoUtil#getResourcesForDataset(String)} method when dataset
	 * specified with <code>datasetName</code> does not exist in the database.
	 */
	@Test
	void testGetResourcesForDataset2() {
		assertNull(mongoUtil.getResourcesForDataset(TEST_DOC_NON_EXISTING));
	}

	/**
	 * Tests {@link MongoUtil#getResourcesForDataset(String)} method when the
	 * specified <code>datasetName</code> is <code>null</code>.
	 */
	@Test
	void testGetResourcesForDataset_Ex1() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.getResourcesForDataset(null);
		});
		assertEquals("Dataset name cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#getResourcesForDataset(String)} method when the
	 * specified <code>datasetName</code> is empty string.
	 */
	@Test
	void testGetResourcesForDataset_Ex2() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.getResourcesForDataset("");
		});
		assertEquals("Dataset name cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#getResourceForDataset(String, String)} method.
	 */
	void testGetResourceForDataset1() {
		// insert dataset to search
		mongoDB.getCollection(Config.DATASET_COLLECTION).insertOne(testDocument);
		assertEquals(1, mongoDB.getCollection(Config.DATASET_COLLECTION).count());

		Document resource = mongoUtil.getResourceForDataset(TEST_DATASET_NAME, TEST_RESOURCE_ID);
		assertNotNull(resource);
		assertEquals(TEST_RESOURCE_ID, resource.get(ResourceStrings.ID));
	}

	/**
	 * Tests {@link MongoUtil#getResourceForDataset(String, String)} method where no
	 * resource exists for the specified resource id.
	 */
	void testGetResourceForDataset() {
		// insert dataset to search
		mongoDB.getCollection(Config.DATASET_COLLECTION).insertOne(testDocument);
		assertEquals(1, mongoDB.getCollection(Config.DATASET_COLLECTION).count());

		Document resource = mongoUtil.getResourceForDataset(TEST_DATASET_NAME, TEST_DOC_NON_EXISTING);
		assertNull(resource);
	}

	/**
	 * Tests {@link MongoUtil#getResourceForDataset(String, String)} method when the
	 * specified <code>datasetName</code> is <code>null</code>.
	 */
	@Test
	void testGetResourceForDataset_Ex1() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.getResourceForDataset(null, TEST_RESOURCE_ID);
		});
		assertEquals("Dataset name cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#getResourceForDataset(String, String)} method when the
	 * specified <code>datasetName</code> is empty string.
	 */
	@Test
	void testGetResourceForDataset_Ex2() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.getResourceForDataset("", TEST_RESOURCE_ID);
		});
		assertEquals("Dataset name cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#getResourceForDataset(String, String)} method when the
	 * specified <code>resourceId</code> is <code>null</code>.
	 */
	@Test
	void testGetResourceForDataset_Ex3() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.getResourceForDataset(TEST_DATASET_NAME, null);
		});
		assertEquals("Resource ID cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#getResourceForDataset(String, String)} method when the
	 * specified <code>resourceId</code> is empty string.
	 */
	@Test
	void testGetResourceForDataset_Ex4() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.getResourceForDataset(TEST_DATASET_NAME, "");
		});
		assertEquals("Resource ID cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#updateProcessedFile(Document, Document)} method.
	 */
	@Test
	@Disabled
	void testUpdateProcessedFile() {
		// TODO complete this
	}

	/**
	 * Tests {@link MongoUtil#getWholeDatasetList()} method when no documents exist
	 * in the database.
	 */
	@Test
	void testGetWholeDatasetList1() {
		assertEquals(0, mongoUtil.getWholeDatasetList("mongo").size());
	}

	/**
	 * Tests {@link MongoUtil#getWholeDatasetList()} method.
	 */
	@Test
	void testGetWholeDatasetList2() {
		// insert dataset
		mongoDB.getCollection(Config.DATASET_COLLECTION).insertOne(testDocument);
		assertEquals(1, mongoDB.getCollection(Config.DATASET_COLLECTION).count());

		List<IDataset> datasets = mongoUtil.getWholeDatasetList("mongo");

		assertEquals(1, datasets.size());
		datasets.get(0).getName().equals(TEST_DATASET_NAME);

		// insert another dataset
		mongoDB.getCollection(Config.DATASET_COLLECTION).insertOne(testDocument2);
		assertEquals(2, mongoDB.getCollection(Config.DATASET_COLLECTION).count());

		datasets = mongoUtil.getWholeDatasetList("mongo");

		assertEquals(2, datasets.size());
	}

	/**
	 * Tests {@link MongoUtil#findVocabulary(String)} method.
	 */
	@Test
	void testFindVocabulary1() {
		// insert vocabulary to search
		mongoDB.getCollection(Config.VOCABULARY_COLLECTION).insertOne(testVocabularyDoc);
		assertEquals(1, mongoDB.getCollection(Config.VOCABULARY_COLLECTION).count());

		// search for inserted vocabulary
		Vocabulary resultVocab = mongoUtil.findVocabulary(TEST_VOCAB_URI);
		assertNotNull(resultVocab);
		assertTrue(testVocabulary.equals(resultVocab));
		assertEquals(mongoDB.getCollection(Config.VOCABULARY_COLLECTION).find(new Document("uri", TEST_VOCAB_URI))
				.first().get("_id").toString(), resultVocab.getId());
		
		
	}

	/**
	 * Tests {@link MongoUtil#findVocabulary(String)} method for non-existing
	 * vocabulary.
	 */
	@Test
	void testFindVocabulary2() {
		assertNull(mongoUtil.findVocabulary(TEST_DOC_NON_EXISTING));
	}

	/**
	 * Tests {@link MongoUtil#findVocabulary(String)} method for <code>null</code>
	 * <code>uri</code> value.
	 */
	@Test
	void testFindVocabulary_Ex1() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.findVocabulary(null);
		});
		assertEquals("Vocabulary URI cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#findVocabulary(String)} method for empty string
	 * <code>uri</code> value.
	 */
	@Test
	void testFindVocabulary_Ex2() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.findVocabulary("");
		});
		assertEquals("Vocabulary URI cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#insertVocabulary(Document)} method.
	 */
	@Test
	void testInsertVocabulary() {
		// assert that no document exists in the database
		assertEquals(0, mongoDB.getCollection(Config.VOCABULARY_COLLECTION).count());

		// insert vocabulary
		mongoUtil.insertVocabulary(testVocabulary);
		// assert that the database contains exactly one document
		assertEquals(1, mongoDB.getCollection(Config.VOCABULARY_COLLECTION).count());

		// assert that the document matches the inserted document
		assertNotNull(mongoDB.getCollection(Config.VOCABULARY_COLLECTION)
				.find(new Document(VocabularyStrings.URI, TEST_VOCAB_URI)));
	}

	/**
	 * Tests {@link MongoUtil#insertDataset(Document)} method for <code>null</code>
	 * <code>vocabulary</code> value.
	 */
	@Test
	void testInsertVocabulary_Ex1() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.insertVocabulary(null);
		});
		assertEquals("Cannot insert null vocabulary to the database.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#getLabelListFromVocabulary(String, List)} method for
	 * properties.
	 */
	@Test
	void testGetLabelListFromVocabulary1() {
		// insert vocabulary to search
		mongoDB.getCollection(Config.VOCABULARY_COLLECTION).insertOne(testVocabularyDoc);
		assertEquals(1, mongoDB.getCollection(Config.VOCABULARY_COLLECTION).count());

		List<String> labelList = mongoUtil.getLabelListFromVocabulary(VocabularyStrings.PROPERTIES, propertyList);

		assertEquals(2, labelList.size());
		assertTrue(labelList.contains("has reduction ratio"));
		assertTrue(labelList.contains("has other physical details"));
	}

	/**
	 * Tests {@link MongoUtil#getLabelListFromVocabulary(String, List)} method for
	 * classes.
	 */
	@Test
	void testGetLabelListFromVocabulary2() {
		// insert vocabulary to search
		mongoDB.getCollection(Config.VOCABULARY_COLLECTION).insertOne(testVocabularyDoc);
		assertEquals(1, mongoDB.getCollection(Config.VOCABULARY_COLLECTION).count());

		List<String> labelList = mongoUtil.getLabelListFromVocabulary(VocabularyStrings.CLASSES, classList);

		assertEquals(2, labelList.size());
		assertTrue(labelList.contains("has bibliographic format of older monographic resource"));
		assertTrue(labelList.contains("has edition area"));
	}

	/**
	 * Tests {@link MongoUtil#getLabelListFromVocabulary(String, List)} method when
	 * <code>null</code> <code>type</code> specified.
	 */
	@Test
	void testGetLabelListFromVocabulary_Ex1() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.getLabelListFromVocabulary(null, classList);
		});
		assertEquals("Type cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#getLabelListFromVocabulary(String, List)} method when
	 * empty string <code>type</code> specified.
	 */
	@Test
	void testGetLabelListFromVocabulary_Ex2() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.getLabelListFromVocabulary("", classList);
		});
		assertEquals("Type cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link MongoUtil#getLabelListFromVocabulary(String, List)} method when
	 * <code>null</code> <code>list</code> specified.
	 */
	@Test
	void testGetLabelListFromVocabulary_Ex3() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			mongoUtil.getLabelListFromVocabulary(VocabularyStrings.PROPERTIES, null);
		});
		assertEquals("List to get label for cannot be null.", ex.getMessage());
	}

}
