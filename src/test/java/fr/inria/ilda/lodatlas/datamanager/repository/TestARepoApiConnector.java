package fr.inria.ilda.lodatlas.datamanager.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

/**
 * An abstract test class to test {@link ARepoApiConnector} abstract class.
 * 
 * @see ARepoApiConnector
 * 
 * @author Hande Gözükan
 *
 */
abstract class TestARepoApiConnector {

	String REPO_NAME = "repoName";
	String REPO_URL;
	String INVALID_REPO_URL = "http";
	String NON_EXISTING = "nonexisting";

	IRepoApiConnector connector;

	@AfterEach
	void tearDown() throws Exception {
		connector = null;
	}

	/**
	 * Tests
	 * {@link fr.inria.ilda.lodatlas.datamanager.repository.ARepoApiConnector#getJSONSource()}
	 * method.
	 */
	@Test
	void testGetRepositoryName() {
		assertEquals(REPO_NAME, connector.getJSONSource());
	}

	/**
	 * Tests
	 * {@link fr.inria.ilda.lodatlas.datamanager.repository.ARepoApiConnector#getRepositoryUrl()}
	 * method.
	 */
	@Test
	void testGetRepositoryUrl() {
		assertEquals(REPO_URL, connector.getRepositoryUrl());
	}

	/**
	 * Tests {@link ARepoApiConnector#getDatasetList()} method.
	 */
	@Test
	abstract void testGetDatasetList();

	/**
	 * Tests {@link ARepoApiConnector#getDatasetList()} method when the specified
	 * <code>datasetName</code> does not exist in the repository.
	 */
	@Test
	void testGetDatasetJson() {
		assertNull(connector.getDatasetJson(NON_EXISTING));
	}

}
