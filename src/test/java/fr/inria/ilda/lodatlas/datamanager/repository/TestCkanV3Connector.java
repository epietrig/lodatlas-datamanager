/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package fr.inria.ilda.lodatlas.datamanager.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author Hande Gözükan
 *
 */
class TestCkanV3Connector extends TestARepoApiConnector {

	String QUERY_STRING = "q=tags:lod";
	String DBPEDIA = "dbpedia";

	int START_INDEX = 0;
	int TOTAL_COUNT = -1;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		REPO_URL = "https://old.datahub.io/";
		connector = new CkanV3Connector(REPO_NAME, REPO_URL, QUERY_STRING, START_INDEX, TOTAL_COUNT);
	}

	/**
	 * Tests
	 * {@link fr.inria.ilda.lodatlas.datamanager.repository.CkanV3Connector#CkanV3Connector(java.lang.String, java.lang.String, java.lang.String)}
	 * method.
	 */
	@Test
	void testCkanV3Connector() {
		connector = new CkanV3Connector(REPO_NAME, REPO_URL, QUERY_STRING, START_INDEX, TOTAL_COUNT);
		assertEquals(REPO_NAME, connector.getJSONSource());
		assertEquals(REPO_URL, connector.getRepositoryUrl());
		assertTrue(connector.getDatasetList().size() > 0);
	}

	/**
	 * Tests
	 * {@link fr.inria.ilda.lodatlas.datamanager.repository.CkanV3Connector#getDatasetList()}
	 * method.
	 */
	@Test
	void testGetDatasetList() {
		assertTrue(connector.getDatasetList().size() >= 1281);
	}

	/**
	 * Tests
	 * {@link fr.inria.ilda.lodatlas.datamanager.repository.CkanV3Connector#getDatasetList()}
	 * method when <code>null</code> is specified as <code>query</code>.
	 */
	@Test
	void testGetDatasetList1() {
		connector = new CkanV3Connector(REPO_NAME, REPO_URL, null, START_INDEX, TOTAL_COUNT);
		assertTrue(connector.getDatasetList().size() > 0);
	}

	/**
	 * Tests
	 * {@link fr.inria.ilda.lodatlas.datamanager.repository.CkanV3Connector#getDatasetList()}
	 * method when empty string is specified as <code>query</code>.
	 */
	@Test
	void testGetDatasetList2() {
		connector = new CkanV3Connector(REPO_NAME, REPO_URL, "", START_INDEX, TOTAL_COUNT);
		assertTrue(connector.getDatasetList().size() > 0);
	}

	/**
	 * Tests
	 * {@link fr.inria.ilda.lodatlas.datamanager.repository.CkanV3Connector#getDatasetList()}
	 * method when empty string is specified as <code>query</code>.
	 */
	@Test
	void testGetDatasetList3() {
		connector = new CkanV3Connector(REPO_NAME, "http://com", "", START_INDEX, TOTAL_COUNT);
		assertEquals(0, connector.getDatasetList().size());
	}

	/**
	 * Tests
	 * {@link fr.inria.ilda.lodatlas.datamanager.repository.CkanV3Connector#getDatasetJson(java.lang.String)}
	 * method.
	 */
	@Test
	@Override
	void testGetDatasetJson() {
		super.testGetDatasetJson();
		assertNotNull(connector.getDatasetJson(DBPEDIA));
	}

	/**
	 * Tests {@link CkanV3Connector#hasNextChunk()} method.
	 */
	@Test
	void testHasNextChunk1() {
		connector = new CkanV3Connector(REPO_NAME, REPO_URL, "", 0, 5);
		assertTrue(connector.hasNextChunk());
		connector.getNextChunkList();
		assertFalse(connector.hasNextChunk());
	}

	/**
	 * Tests {@link CkanV3Connector#hasNextChunk()} method when the number of
	 * datasets to download is 0.
	 */
	@Test
	void testHasNextChunk2() {
		connector = new CkanV3Connector(REPO_NAME, REPO_URL, "", 0, 0);
		assertFalse(connector.hasNextChunk());
	}

	/**
	 * Tests {@link CkanV3Connector#hasNextChunk()} method when the number of
	 * datasets to download is specified as -1.
	 */
	@Test
	void testHasNextChunk3() {
		connector = new CkanV3Connector(REPO_NAME, REPO_URL, "", 0, -1);
		assertTrue(connector.hasNextChunk());
		connector.getNextChunkList();
		assertTrue(connector.hasNextChunk());
	}

	/**
	 * Tests {@link CkanV3Connector#getNextChunk()} method when the number of
	 * datasets to download is specified as 5.
	 */
	@Test
	void testGetNextChunk1() {
		connector = new CkanV3Connector(REPO_NAME, REPO_URL, "", 0, 5);
		assertTrue(connector.hasNextChunk());
		assertEquals(5, connector.getNextChunkList().size());
	}
	
	/**
	 * Tests {@link CkanV3Connector#getNextChunk()} method when the number of
	 * datasets to download is specified as 1005.
	 */
	@Test
	void testGetNextChunk2() {
		connector = new CkanV3Connector(REPO_NAME, REPO_URL, "", 0, 1005);
		assertTrue(connector.hasNextChunk());
		assertEquals(1000, connector.getNextChunkList().size());
		assertTrue(connector.hasNextChunk());
		assertEquals(5, connector.getNextChunkList().size());
	}

}
