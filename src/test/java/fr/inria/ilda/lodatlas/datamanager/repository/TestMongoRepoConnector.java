package fr.inria.ilda.lodatlas.datamanager.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.bson.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.Config;
import fr.inria.ilda.lodatlas.datamanager.dataset.util.MongoUtil;

/**
 * A test class to test {@link MongoRepoConnector} class.
 * 
 * @see MongoRepoConnector
 * 
 * @author Hande Gözükan
 *
 */
class TestMongoRepoConnector extends TestARepoApiConnector {

	String HOST = "localhost";
	int PORT = 27017;
	String DB_NAME = "lodatlas_test";

	String TEST_DOC_NAME = "test";
	String TEST_DOC_NAME2 = "test2";

	Document testDocument;
	Document testDocument2;

	MongoUtil mongoUtil;
	MongoClient mongoClient;
	MongoDatabase mongoDB;

	@BeforeAll
	void initAll() throws Exception {
		mongoUtil = new MongoUtil(HOST, PORT, DB_NAME);
		mongoClient = new MongoClient(HOST, PORT);

		REPO_NAME = MongoRepoConnector.JSON_SOURCE;

		testDocument = Document.parse("{\"name\": \"" + TEST_DOC_NAME + "\", \"" + DatasetStrings.RESOURCES
				+ "\": [{\"id\" : \"b1662e53-67fb-4ceb-bde3-8d6d08bcae4c\"},{\"id\" : \"6bfd8464-ea6d-4e54-a9e2-50ae57eaaddf\"}]}");
		testDocument2 = Document.parse("{\"name\": \"" + TEST_DOC_NAME2 + "\", \"" + DatasetStrings.RESOURCES
				+ "\": [{\"id\" : \"b1662e53-67fb-4ceb-bde3-8d6d08bcae4c\"},{\"id\" : \"6bfd8464-ea6d-4e54-a9e2-50ae57eaaddf\"}]}");
	}

	@BeforeEach
	void init() throws Exception {
		mongoDB = mongoClient.getDatabase(DB_NAME);

		connector = new MongoRepoConnector(mongoUtil);
	}

	@AfterEach
	void tearDown() throws Exception {
		super.tearDown();
		mongoDB.drop();
	}

	@AfterAll
	void tearDownAll() {
		mongoUtil.dispose();
		mongoUtil = null;

		mongoDB = null;
		mongoClient.close();
		mongoClient = null;

		testDocument = null;
		testDocument2 = null;
	}

	/**
	 * Tests {@link ARepoApiConnector#getDatasetList()} method when there is no
	 * document in the database.
	 */
	@Test
	@Override
	void testGetDatasetList() {
		assertEquals(0, connector.getDatasetList().size());
	}

	/**
	 * Tests {@link ARepoApiConnector#getDatasetList()} method when the database
	 * contains dataset documents.
	 */
	@Test
	void testGetDatasetList1() {
		// insert document to MongoDB
		mongoDB.getCollection(Config.DATASET_COLLECTION).insertOne(testDocument);
		mongoDB.getCollection(Config.DATASET_COLLECTION).insertOne(testDocument2);
		assertEquals(2, mongoDB.getCollection(Config.DATASET_COLLECTION).count());

		// assert that the number of datasets names in the list is equal to number of
		// docs added
		assertEquals(2, connector.getDatasetList().size());
	}

	/**
	 * Tests {@link ARepoApiConnector#getDatasetJson(String)} method.
	 */
	@Test
	void testGetDatasetJson() {
		mongoDB.getCollection(Config.DATASET_COLLECTION).insertOne(testDocument);

		String datasetJson = connector.getDatasetJson(TEST_DOC_NAME);
		assertEquals(testDocument.toJson(), datasetJson);
	}

}
