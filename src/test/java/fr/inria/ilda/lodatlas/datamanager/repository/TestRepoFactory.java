package fr.inria.ilda.lodatlas.datamanager.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestRepoFactory {

	String API = "ckanv3";
	String NON_EXISTING_API = "nonexisting";
	String REPO_NAME = "datahub";
	String REPO_URL = "https://old.datahub.io/";
	String INVALID_REPO_URL = "http";
	String QUERY_STRING = "q=tags:lod";

	int START_INDEX = 0;
	int TOTAL_COUNT = -1;

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * Tests
	 * {@link RepositoryFactory#getRepoConnector(String, String, String, String)}
	 * method with valid arguments.
	 */
	@Test
	void testGetRepoConnector1() {
		IRepoApiConnector repoConnector = RepositoryFactory.getRepoConnector(API, REPO_NAME, REPO_URL, QUERY_STRING,
				START_INDEX, TOTAL_COUNT);
		assertNotNull(repoConnector);
		assertEquals(REPO_NAME, repoConnector.getJSONSource());
		assertEquals(REPO_URL, repoConnector.getRepositoryUrl());
	}

	/**
	 * Tests
	 * {@link RepositoryFactory#getRepoConnector(String, String, String, String)}
	 * method with invalid <code>api</code> string.
	 */
	@Test
	void testGetRepoConnector2() {
		assertNull(
				RepositoryFactory.getRepoConnector(null, REPO_NAME, REPO_URL, QUERY_STRING, START_INDEX, TOTAL_COUNT));
		assertNull(RepositoryFactory.getRepoConnector("", REPO_NAME, REPO_URL, QUERY_STRING, START_INDEX, TOTAL_COUNT));
		assertNull(RepositoryFactory.getRepoConnector(NON_EXISTING_API, REPO_NAME, REPO_URL, QUERY_STRING, START_INDEX,
				TOTAL_COUNT));
	}

	/**
	 * Tests
	 * {@link RepositoryFactory#getRepoConnector(String, String, String, String)}
	 * method with invalid <code>repoName</code> string.
	 */
	@Test
	void testGetRepoConnector3() {
		assertNull(
				RepositoryFactory.getRepoConnector(null, REPO_NAME, REPO_URL, QUERY_STRING, START_INDEX, TOTAL_COUNT));
		assertNull(RepositoryFactory.getRepoConnector("", REPO_NAME, REPO_URL, QUERY_STRING, START_INDEX, TOTAL_COUNT));
		assertNull(RepositoryFactory.getRepoConnector(NON_EXISTING_API, REPO_NAME, REPO_URL, QUERY_STRING, START_INDEX,
				TOTAL_COUNT));
	}

	/**
	 * Tests
	 * {@link RepositoryFactory#getRepoConnector(String, String, String, String)}
	 * method with invalid <code>repoName</code> string.
	 */
	@Test
	void testGetRepoConnector4() {
		assertNull(RepositoryFactory.getRepoConnector(API, null, REPO_URL, QUERY_STRING, START_INDEX, TOTAL_COUNT));
		assertNull(RepositoryFactory.getRepoConnector(API, "", REPO_URL, QUERY_STRING, START_INDEX, TOTAL_COUNT));
	}

	/**
	 * Tests
	 * {@link RepositoryFactory#getRepoConnector(String, String, String, String)}
	 * method with invalid <code>repoUrl</code> string.
	 */
	@Test
	void testGetRepoConnector5() {
		assertNull(RepositoryFactory.getRepoConnector(API, REPO_NAME, null, QUERY_STRING, START_INDEX, TOTAL_COUNT));
		assertNull(RepositoryFactory.getRepoConnector(API, REPO_NAME, "", QUERY_STRING, START_INDEX, TOTAL_COUNT));
		assertNull(RepositoryFactory.getRepoConnector(API, REPO_NAME, INVALID_REPO_URL, QUERY_STRING, START_INDEX,
				TOTAL_COUNT));
	}

	/**
	 * Tests
	 * {@link RepositoryFactory#getRepoConnector(String, String, String, String)}
	 * method specifying <code>null</code> and empty string <code>query</code>.
	 */
	@Test
	void testGetRepoConnector6() {
		assertNotNull(RepositoryFactory.getRepoConnector(API, REPO_NAME, REPO_URL, null, START_INDEX, TOTAL_COUNT));
		assertNotNull(RepositoryFactory.getRepoConnector(API, REPO_NAME, REPO_URL, "", START_INDEX, TOTAL_COUNT));
	}

}
