package fr.inria.ilda.lodatlas.datamanager.util;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class TestUtil {

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	@Disabled
	void testLoadCsvAsMap() {
		fail("Not yet implemented");
	}

	@Test
	@Disabled
	void testLoadFromFile() {
		fail("Not yet implemented");
	}

	@Test
	@Disabled
	void testLoadProperties() {
		fail("Not yet implemented");
	}

}
